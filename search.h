// DEPRECATED -- NOT USED
#include <iostream>
#include <sstream>
#include <string> // needed for stoul()
#include <vector>
#include <queue>

#include <stdio.h>
#include <string.h>
#include <fstream>

using namespace std;

vector<string> tokenizer(string s,string delim); 
void searchFileParser(ifstream &file);

// A queue of one or more heuristic searches (or maybe other actions) that can be executed in sequence
class Search 
	{
	private:
		SearchParameters default_search_parms;
		queue <SearchParameters> search_action_list;
	//std::ifstream file("modeltest");

	public:
		Search(SearchParameters dsp):default_search_parms{dsp} // no search action file given
			{}

		Search(SearchParameters dsp, ifstream &file):default_search_parms{dsp}
			{
			if (file.is_open()) 
				searchFileParser(file);
			}
		// might want to have another execute taking an entire PQ as input...
		void execute(TSASDataPersistent dp)
			{
			TSASHeuristic th(dp);
			while (!search_action_list.empty())
				{
				SearchParameters parms = search_action_list.front();
				// if (sp.action == "simple") ...
				th.simpleHeuristic(parms);
				search_action_list.pop();
				}
			}
		inline void push(SearchParameters parms) { search_action_list.push(parms);}


	private:
	void searchFileParser(ifstream &file)
	{
	std::string line;
	while (std::getline(file, line)) 
		{
		//printf("%s\n", line.c_str());
		auto v = tokenizer(line," \t");
		//for (int i=0;i<v.size();i++)	
		//	cout << v.at(i) << endl;
		string algo = v.at(0);
		cout << algo << endl;
		SearchParameters sp = default_search_parms;
		sp.action = algo;
		if (v.size()>1) // there are key:parm pair(s); fetch them 
			{
			for (int i=1;i<v.size();i++)	
				{
				string key_val = v[i];
				auto split = tokenizer(key_val,": ");
				if (split.size() == 2)
					{
					string key = split[0];
					string val = split[1];
					cout << key << "':'" << val << endl;
					storeKeyVal(key,val,sp);
					}
				}	
			}
		search_action_list.push(sp);
		}
	file.close();
	}
	private:
	void storeKeyVal(string key, string val,SearchParameters &sp)
		{
		if (key == "gtpmp_max_iter") sp.gtpmp_max_iter = stoul(val); 
		else if ("sp_max_iter") sp.sp_max_iter = stoul(val); 
		else if ("gtp_weight") sp.gtp_weight = stod(val); 
		else if ("mp_weight") sp.mp_weight = stod(val); 
		else if ("nni_radius") sp.nni_radius = stoul(val);
		else if ("multrees") sp.multrees = stoul(val); 
		else if ("h_mulltrees") sp.h_multrees = stoul(val); 
		}
	};

// Global function
// Using the old C functionality here: better suited to multiple delimiters, etc.; less wordy too
vector<string> tokenizer(string s,string delim) // default to EOL
	{
	vector<string> v;

	char *c_s = strdup(s.c_str());
 	const char *c_delim = delim.c_str();

	char *token;
	token  = strtok(c_s,c_delim);
	while (token != NULL)
		{
		string word(token);
		v.push_back(word);
		token = strtok(NULL,c_delim);
		}
	return v;
	}

