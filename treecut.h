#ifndef TREECUT_H
#define TREECUT_H
#include "tree.h"

// Class for the two trees that result when we disconnect a subtree from an original tree by deleting an edge. Useful for rearrangements.
// Instantiate with   TreeCut(original_tree, subtree_node), where subtree_node is the root of the subtree removed.
// NB. Watch out if there is a problem with template for NAMEDLEAFNODE being unrooted, which I dealt with in tree.h:setupNodeVectors
// When iterating, note that the node lists get modified by cutting. Better make a copy of nodelist first and iterate through copy
// I have this one setting in which the tree uses existing nodes of another tree, so when deleting the tree,
// I don't want the usual destructor; I just get rid of the vectors of node pointers, but keep the nodes proper.
class ShadowTree:public GeneTreeUnrooted
	{

		~ShadowTree()
			{
				nodes.clear();
				leafnodes.clear();
			}
	};

class TreeCut
	{
	public: 
	TREE* pruned_tree;
	TREE* subtree;
	NODE* subroot; // the root of the subtree to be pruned
	NODE* restore_node; // sib of subroot, tells us how to put it back
	NODE* save_p;
	int which_sib;

		TreeCut(TREE* t, NODE* sr): pruned_tree{t},subroot{sr}  
			{
			//subtree = new TREE(); // NOT RIGHT, viz memory leak...
			subtree = new ShadowTree(); // NOT RIGHT, viz memory leak...
			restore_node = subroot->getSibling();
//msgout << endl << "Initial tree before cut " << pruned_tree->tree2newick() << endl;
			}

		void cut() 
			{ 
			if (subroot->isRoot()) EXCEPTION("Subroot == root in TreeCut");
			pruned_tree->nodes.clear();
			pruned_tree->leafnodes.clear();

			NODE* sib = subroot->getSibling();
			NODE *p   = subroot->parent();
			save_p    = p;
			subroot->parent() = nullptr;	
			subtree->root = subroot;
			if (p->isRoot())
				{
				sib->parent() = nullptr;
				pruned_tree->root = sib;
				}
			else
				{
				NODE* pp = p->parent();
				sib->parent() = pp;
				which_sib = p->whichSibling(); // save which child of pp node p is
				pp->child(which_sib) = sib; // all this to preserve left right order of sib node
				}
			// note that p is left untouched and stored as p_save for later uncut()
			pruned_tree->setupNodeVectors();
			subtree->setupNodeVectors();
//msgout << "pruned tree after cut " << pruned_tree->tree2newick() << endl;
//msgout << "subtree after cut " << subtree->tree2newick() << endl;
			}

		void uncut() // merge back to original tree
			{
			pruned_tree->nodes.clear();
			pruned_tree->leafnodes.clear();

			restore_node->parent() = save_p;
			subroot->parent() = save_p;
			if (save_p->isRoot())
				{
				pruned_tree->root = save_p;
				}
			else
				{
				NODE* pp = save_p->parent();
				pp->child(which_sib) = save_p; // all this to preserve left right order of sib node
				}

			pruned_tree->setupNodeVectors();
			delete subtree;
//msgout << "pruned tree after uncut " << pruned_tree->tree2newick() << endl;
//msgout << *pruned_tree << endl;
//msgout << "subtree after uncut " << subtree->tree2newick() << endl;
			}

	};




#endif 
