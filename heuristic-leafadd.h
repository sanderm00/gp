/*
 * Copyright (C) 2007 Andre Wehe, Mukul Bansal, Oliver Eulenstein
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef HEURISTIC_LEAFADD_H
#define HEURISTIC_LEAFADD_H

#define TreeSet LEAFADD::TreeSet
#define SpeciesNode LEAFADD::SpeciesNode
#define NamedSpeciesNode LEAFADD::NamedSpeciesNode
#define GeneNodeUnrooted LEAFADD::GeneNodeUnrooted
#define NamedGeneNodeUnrooted LEAFADD::NamedGeneNodeUnrooted
#define GeneTreeUnrooted LEAFADD::GeneTreeUnrooted
#define GeneNodeRooted LEAFADD::GeneNodeRooted
#define NamedGeneNodeRooted LEAFADD::NamedGeneNodeRooted
#define GeneTreeRooted LEAFADD::GeneTreeRooted

// ------------------------------------------------------------------------------------------------------------------
class HeuristicLeafAdd : public TreeSet {
public:
	HeuristicLeafAdd() {
		#ifdef DEBUG
		cout << "Heuristic created" << endl;
		#endif
	}

	virtual ~HeuristicLeafAdd() {
		#ifdef DEBUG
		cout << "Heuristic destroyed" << endl;
		#endif
	}

	// ------------------------------------------------------------------------------------------------------
	// compute the gene duplications
	inline void computeGeneDuplications(SpeciesNode *subtree, bool reroot = true) {
		SpeciesNode *sibling = subtree->getSibling();
//cout << "test6.0" <<endl;
		speciestree->establishOrder();
//cout << "test6.1 " << endl;
		speciestree->preprocessLCA();
//cout << "test6.2 " << endl;
		resetGeneDuplications(sibling);

		// process all rooted trees
		for(int i=0; i<genetree_rooted.size(); i++) {
			GeneTreeRooted &tree = *genetree_rooted[i];
			createPrimaryMapping(tree);
//cout << "test6.5 " << genetree_rooted.size() << endl;
			const int score = getScore(tree);
//cout << "test7" << endl;
			createSecondaryMapping(tree, subtree);
//cout << "test8" << endl;
			computeGeneDuplicationsTriple();
//cout << "test9" << endl;
			removeSecondaryMapping(tree);
//cout << "test10" << endl;
			computeGeneDuplicationsAdd(sibling, score);
		}

//cout << "inside computeGeneDuplications "<< endl;
		// process all unrooted trees
		for(int i=0; i<genetree_unrooted.size(); i++) {
			GeneTreeUnrooted &tree = *genetree_unrooted[i];
			if (reroot) { // find the best geneduplication score of all rootings (rerooting of the genetrees)
				createPrimaryMappingUnrooted(tree);
				int &score = best_score;
				score = getScore(tree);
				createSecondaryMapping(tree, subtree);
				computeGeneDuplicationsTriple();
				removeSecondaryMapping(tree);
				computeGeneDuplicationsTempReplace(sibling, score);
				GeneNodeUnrooted *u = tree.root->child(0);
				GeneNodeUnrooted *v = tree.root->child(1);
				best_node[0] = u;
				best_node[1] = v;
				#ifdef DEBUG
				position_counter_debug = 2;
				if ((u == NULL) || (v == NULL)) EXCEPTION("child = NULL in computeGeneDuplications" << endl);
				#endif
				moveRoot(tree, u, subtree, sibling);
				moveRoot(tree, v, subtree, sibling);
				#ifdef DEBUG
				if (position_counter_debug != tree.nodes.size()-1)
					WARNING("tree traversal failed in computeGeneDuplications" << position_counter_debug << " != " << tree.nodes.size()-1 << endl);
				#endif
				tree.reroot(best_node[0], best_node[1]);
				addTempGeneDuplications(sibling);
			} else { // find the best genedupication of the current rooting
				createPrimaryMapping(tree);
				const int score = getScore(tree);
				createSecondaryMapping(tree, subtree);
				computeGeneDuplicationsTriple();
				removeSecondaryMapping(tree);
				computeGeneDuplicationsAdd(sibling, score);
			}
		}
		speciestree->postprocessLCA();
		forEachCallScoreComputed(sibling);
	}

	// calculate the gene duplication for all rootings
	#ifdef DEBUG
	int position_counter_debug;
	#endif
	int best_score;
	GeneNodeUnrooted *best_node[2];
	inline void moveRoot(GeneTreeUnrooted &tree, GeneNodeUnrooted *p, SpeciesNode *subtree, SpeciesNode *sibling) {
		GeneNodeUnrooted *c[2];
		for (int i=0; i<2; i++) c[i] = p->child(i);
		for (int i=0; i<2; i++) {
			if (c[i] != NULL) {
				moveRoot(tree, c[i], subtree, sibling);
				tree.reroot(c[i], p);
				#ifdef DEBUG
				tree.checkStructure();
				position_counter_debug++;
				#endif
				const int score = getScore(tree);
				if (score < best_score) {
					best_score = score;
					best_node[1] = c[i];
					best_node[0] = p;
				}
				createSecondaryMapping(tree, subtree);
				computeGeneDuplicationsTriple();
				removeSecondaryMapping(tree);
				computeGeneDuplicationsTempMin(sibling, score);
			}
		}
	}

	// reset the gene duplication score to 0
	inline void resetGeneDuplications(SpeciesNode *&node) {
		if (node == NULL) return;
		node->genedup = 0;
		for (int i=0; i<2; i++)
			resetGeneDuplications(node->child(i));
	}

	// compute the gene duplication score and add it to the current score
	inline void computeGeneDuplicationsAdd(SpeciesNode *&node, const int &genedup) {
		if (node == NULL) return;
		node->genedup += genedup;
		for (int i=0; i<2; i++)
			computeGeneDuplicationsAdd(node->child(i), genedup + node->gain - node->lost[i]);
	}

	// compute the gene duplication score into a temporary variable
	inline void computeGeneDuplicationsTempReplace(SpeciesNode *&node, const int &genedup) {
		if (node == NULL) return;
		node->tempgenedup = genedup;
		for (int i=0; i<2; i++)
			computeGeneDuplicationsTempReplace(node->child(i), genedup + node->gain - node->lost[i]);
	}
	// compute the gene duplication score into a temporary variable (replace is smaller than current score)
	inline void computeGeneDuplicationsTempMin(SpeciesNode *&node, const int &genedup) {
		if (node == NULL) return;
		if (genedup < node->tempgenedup) node->tempgenedup = genedup;
		for (int i=0; i<2; i++)
			computeGeneDuplicationsTempMin(node->child(i), genedup + node->gain - node->lost[i]);
	}
	// add the temporary gene duplication score to the final
	inline void addTempGeneDuplications(SpeciesNode *&node) {
		if (node == NULL) return;
		node->genedup += node->tempgenedup;
		for (int i=0; i<2; i++)
			addTempGeneDuplications(node->child(i));
	}

	// travers teh tree and call the virtual function scoreComputed()
	inline void forEachCallScoreComputed(SpeciesNode *&node) {
		if (node == NULL) return;
		scoreComputed(*node);
		for (int i=0; i<2; i++)
			forEachCallScoreComputed(node->child(i));
	}
	virtual void scoreComputed(SpeciesNode &node) = 0;
	virtual void run(ostream &os, const ReRoot reroot) = 0;

	// ------------------------------------------------------------------------------------------------------
	// outputs the trees into a string stream
	friend ostream & operator << (ostream &os, HeuristicLeafAdd &t);
};

// outputs the trees into a string stream
ostream & operator << (ostream &os, HeuristicLeafAdd &t) {
	os << (TreeSet&) t;
	return os;
}

#undef TreeSet
#undef SpeciesNode
#undef NamedSpeciesNode
#undef GeneNodeUnrooted
#undef NamedGeneNodeUnrooted
#undef GeneTreeUnrooted
#undef GeneNodeRooted
#undef NamedGeneNodeRooted
#undef GeneTreeRooted

#endif
