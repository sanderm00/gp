// Code to break up MP computations across the alignment set into threads that handle subsets of the alignments.
// Generally the alignment set is partitioned into random subsets of sites. Trick is to get everything back together
// again after search.
#ifdef PTHREADS
#include <pthread.h>
#include <random>
#include <algorithm>
#include <iterator>
#include <vector>

#include "tsas.h"

std::vector<bool> * partition_one (int L,int N); 
std::vector<bool> * partition_stepwise (int L,int N); 
std::vector<bool> * partition_random (int L,int N, unsigned int seed); 

class GeneTreeHeuristic;
void* pthWorker(void* thread_arg);

struct thr_data
		{
		int thread;
		TSASData *ts; // This will be used to create indendent TSASData objects for each thread. OK to use reference
					// because the constructor builds all new member data
		enum CriteriaType opt_criteria;
		unsigned int max_iter; 
		double gtp_weight; 
		double mp_weight;
		std::vector<bool> *gene_subset_vec;
		unsigned int nni_radius;
		TSASDataPersistent *tsp;
		bool root_flag;
		};


// Distribute the genes randomly into threads and do MPGTP searches across each of these subsets 
// within threads.
// Then combine the results appropriately, which is tricky. Easy to combine the sums of sums of scores
// but also have to combine the newly found genetrees from the sets of genetrees in different threads
// into a single TSAS persistent tree...
TSASDataPersistent pthMasterGTPMP(unsigned int seed, bool root_flag, int n_threads,TSASDataPersistent dp, unsigned int max_iter,double gtp_weight, double mp_weight,unsigned int nni_radius)
		{
		// Don't try to print progress on multithreaded gtpmp computations. To do it right requires locking output os blah blah. OK if threads=1
		bool save_quiet = quiet;
		if (!quiet && n_threads>1) WARNING("Disabling GTPMP search progress output when n_threads>1");
		if (n_threads>1) quiet = true;

		pthread_t threads[n_threads];
		pthread_attr_t attr;
		pthread_attr_init(&attr);
		pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE); // Likely this is the default on all systems anyway

		std::vector<TSASData*> ts_array;
		for (int t = 0; t < n_threads; t++)
			{
			TSASData * ts = new TSASData(dp);
			ts_array.push_back(ts);
			}
		struct thr_data thr_data_array[n_threads];

		int n_genes = dp.alignment_set->nAlignments();
		TreeScore score;

		// Partition the set of genes into subsets to distribute to threads.
		// This could be done once at top level of code and reused in every sp generation
		// but here it gets redone randomly each time pthMasterGTPMP is called, in case we like to mix it up.

		//auto gene_partitions1 = partition_one(n_genes,n_threads); // FOR DEBUGGING ONLY
		//std::vector<bool> * gene_partitions = partition_stepwise(n_genes,n_threads);
		std::vector<bool> * gene_partitions = partition_random(n_genes,n_threads,seed);


// ...to check this partition...
#if 0
cout << "Thread by gene partition" << endl;
for (int i=0;i<n_threads;i++)
	{
	for (int j=0;j<n_genes;j++)
		{
		bool c = gene_partitions[i].at(j);
		cout << (c?1:0);
		}
	cout << endl;
	}
#endif
		void* status;

		for (int t = 0; t < n_threads; t++)
			{
			thr_data_array[t].thread = t;
			thr_data_array[t].ts = ts_array[t];
			thr_data_array[t].opt_criteria  = GTP_MP;
			thr_data_array[t].nni_radius  = nni_radius;
			thr_data_array[t].max_iter  = max_iter;
			thr_data_array[t].gtp_weight  = gtp_weight;
			thr_data_array[t].mp_weight  = mp_weight;
			thr_data_array[t].gene_subset_vec  = &gene_partitions[t];
			thr_data_array[t].root_flag  = root_flag;
			//printf("In main: creating thread %i\n", t);
			//int rc = pthread_create(&threads[t], NULL, &pthWorker, (void *)&thr_data_array[t]); 
//			int rc = pthread_create(&threads[t], &attr, &pthWorker, (void *)&thr_data_array[t]); 
			int rc = pthread_create(&threads[t], NULL, &pthWorker, (void *)&thr_data_array[t]); 
		// NB. No end of confusion between traditional C and C++ call of the Worker function. C++ doesn't
		// like passing name of MEMBER function around by pointer, because usually this kind of func call 
		// is bundled with the member's class object that calls it. Recommendations online suggest just
		// using a global function instead (as here) -- or maybe static member func, but that makes me nervous.
			if (rc)
				{
				printf("ERROR; return code from pthread_create() is %d\n", rc);
				exit(-1);
				}
			}

		pthread_attr_destroy(&attr);
		for(int t=0; t<n_threads; t++) 
			{
			int rc = pthread_join(threads[t], &status);
 			if (rc) 
				{
 				printf("ERROR; return code from pthread_join() is %d\n", rc);
				exit(-1);
 				}
 			//printf("Main: completed join with thread %i having a status of %ld\n",t,(long)status);
       			}


		// free the TSASData objects we passed to each thread
		for (int t = 0; t < n_threads; t++)
			delete ts_array[t];

		// Here we need to merge all the newly optimized gene trees into one persistent TSAS object to return
		TSASDataPersistent mergedDP =  *(thr_data_array[0].tsp); // start with the first subset DataPersistent
		delete thr_data_array[0].tsp; // ok, b/c we just made a copy of it
		if (n_threads > 1)
			{
			for (int t = 1; t < n_threads; t++) // and then "merge" the remaining ones with the first one.
				{
				mergedDP.replaceGeneTreeSubset( thr_data_array[t].tsp ,thr_data_array[t].gene_subset_vec );
				delete thr_data_array[t].tsp; 
				}
			}
		delete[] gene_partitions; 

		// Now merge the treescores. This is much simpler, because their computation doesn't affect other threads.
		// Irony here: it's ok to add up scores over the alignments; they are independent between threads.
		// Not ok to do the same with actual gene trees since they have been attached to different TSAS/treesets...

		score =	mergedDP.totalScore(gtp_weight,mp_weight);
		mergedDP.set(score);

		quiet = save_quiet;
		return mergedDP;
		}


// NB. Each worker thread creates its own genetree-heuristic object even though its working on a subset of genes.
void* pthWorker(void* thread_arg)
		{
		struct thr_data *my_data;
		my_data = (struct thr_data *) thread_arg;
		TSASData *ts = my_data->ts;
		enum CriteriaType opt_criteria = my_data->opt_criteria;
		unsigned int max_iter = my_data->max_iter;
		double gtp_weight = my_data->gtp_weight;
		double mp_weight = my_data->mp_weight;
		std::vector<bool> *gene_subset_vec = my_data->gene_subset_vec;
		int thread = my_data->thread;
		unsigned int nni_radius = my_data->nni_radius;
		int n_genes = gene_subset_vec->size();
		bool root_flag = my_data->root_flag;


		TreeScore score,totalScore;
		GeneTreeHeuristic gh(*ts,nni_radius);	// this sets initial leaf mapping and LCA mapping when constructed,

		gh.gene_set.createLeafMapping();	// has to happen any time the species tree has been deleted and replaced
		gh.initializeScores(gtp_weight, mp_weight);

		//TreeScore gene_tree_search_score = gh.geneTreeHeuristicAll(gtpmp_max_iter,gtp_weight,mp_weight);
		int g=0;
		for (auto gene = ts->genes().begin();gene<ts->genes().end();gene++)
		//for (int g = 0; g < n_genes; g++) //another way to iterate
			{
			msgout << "Gene " << g+1 << endl ; // g+1 is consistent with gene label in nonthreaded version of code
			if (gene_subset_vec->at(g) == true) // i.e., this gene is in the partition we're working on
				{	
				//TSASData::Gene &gg = (ts->genes())[g]; //this probably works
				switch(opt_criteria)
					{
					case (GTP): ;	// unused
					case (GTP_ROOTING):
						//score = gh.runRerootSearch(gene,gtp_weight,mp_weight); break;
						score = gh.runRerootSearch(*gene,gtp_weight,mp_weight); break;
					case (MP):
						score = gh.searchGlobalSPRNeighborhood(*gene,max_iter,opt_criteria,gtp_weight,mp_weight);break;
					case (GTP_MP):
						{
						score = gh.gtpmpSearchGlobal(*gene,max_iter,gtp_weight,mp_weight);

						TreeScore checkScore = gh.getReferenceTreeScore(*gene,gtp_weight,mp_weight);
				//checkScore.printScores("[Rechecked scores:]      ");
						if (!(score == checkScore)) 
							{
							msgout << "WARNING: Score recheck FAILED!" << endl;
							//msgout << gene.tree->tree2newick()<<endl;
							EXCEPTION("Exiting...");
							}
	


						if (root_flag)
							{
							gene->tree_score = score; 
							// have to save score before calling next to provide benchmark
							score = gh.runRerootSearch(*gene,gtp_weight,mp_weight); 
							}
						break;
						}
					}
				gene->tree_score = score; // save scores for each alignment here
				score = gene->tree_score;
				}
			++g;
			}

		// store a permanent snapshot of the genetrees for this thread, to be combined in Master
		// don't forget to delete
		my_data->tsp = new TSASDataPersistent(*ts,gene_subset_vec); // this constructor ignores stuff not in the subset mask
 

// (my_data->tsp)->print();

    	pthread_exit((void*) NULL);
		return (void*)NULL;
		}

//****************************************

/* Representation of a partition of the integers [0,L-1] into N sets using a data structure
   consisting of an array of N bool vectors of length L each.
   
   This particular function returns a simple "step" partition, as in following example where N=8, L=100.

1111111111110000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000001111111111110000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000001111111111110000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000001111111111110000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000001111111111110000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000001111111111110000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000001111111111110000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000001111111111111111

   Other use cases might require random partitions,  etc.
*/

// Allocates memory, so free later.

std::vector<bool> * partition_stepwise (int L,int N) 
	{
	std::vector<bool> *vecbool_array = new vector<bool>[N]; 
	int start=0;
	int chunk = L/N;
	int end = chunk-1;

	for (int i=0;i<N;i++)
		{
		if (i == N-1) 
			end = L-1;
		for (int j=0;j<L;j++)
			{
			if (j >= start && j <= end)
				vecbool_array[i].push_back(true);
			else
				vecbool_array[i].push_back(false);
			}
		start += chunk;
		end   += chunk;
		}
	
	return vecbool_array;
	} 



// Alternate way to partition the genes: into randomly selected subsets of the sizes
// described above.

//** WATCH OUT!! Possibly only want to call this ONCE at beginning of run 
std::vector<bool> * partition_random (int L,int N, unsigned int seed) 
	{
	std::vector<unsigned int> indexes(L);
	for (int i=0;i<L;i++) indexes[i]=i;


   std::random_device rd;
   std::mt19937 g(seed); //UGHHHH EVERY TIME THROUGH WE GET THE SAME SEQ OF RANDOM NUMS BECAUSE OF THIS.
 
   std::shuffle(indexes.begin(), indexes.end(), g);

	std::vector<bool> *vecbool_array; //pointer to first vector in array
	vecbool_array = new std::vector<bool>[N]; 


	int start=0;
	int chunk = L/N;
	int end = chunk-1;

	for (int i=0;i<N;i++)
		{
		for (int j=0;j<L;j++)
			vecbool_array[i].push_back(false);
		if (i == N-1) 
			end = L-1;
		for (int j=0;j<L;j++)
			{
			unsigned int index = indexes[j];
			if (index >= start && index <= end)
				vecbool_array[i].at(j)=true;
			}
		start += chunk;
		end   += chunk;
		}
	
	return vecbool_array;


	} 


	std::vector<bool> * partition_one (int L,int N) 
	{
	std::vector<bool> *vecbool_array = new vector<bool>[N]; 
	int start=0;

	for (int j=0;j<L;j++)
			vecbool_array[0].push_back(true);
	
	return vecbool_array;
	} 

#endif


