#ifndef TSAS_H
#define TSAS_H

#include "fletcher64.h"
#include <limits.h>

// Extends the legacy DupTree TreeSet class by adding an alignment set and associated functionality.
// Generally, we will assume the alignment set remains fixed over the duration of the run,
// but it may be useful to engineer (random or other kinds of) subsets of the alignment set here.

// Once instantiated the trees are "live" and can be rearranged, etc., so...
// Important: interface here should only take newick strings as inputs or otherwise guarantee not to change existing trees

// Can access the gene tree and alignment in an instance of this class, say 'tsas0' as follows

// for (auto gene:tsas0.genes()) {
// 		gene.alignment->points to alignment
//		gene.tree-> points to gene tree
//		}

// NB! Don't try to use this object in a priority queue without rethinking copy constructor, assignment, etc., blah, blah

class TSASDataPersistent; // forward decl

class TSASData:public TreeSet, public TreeScore // treescore will be for the whole treeset, usually the overall score
{
AlignmentSet *alignment_set; // careful not to use references here; seems to trip up priority queues to have containers of references.
bool initializedTreeScores = false;

public:
struct Gene
	{
	Alignment *alignment;
	GeneTreeUnrooted *tree;
	TreeScore tree_score; // NB. No longer will  be a pointer to alignment object. That was a bad idea from getgo.
	} ;

std::vector<Gene> genes_v; // convenience to bundle alignment and gene tree together for retrieval

public:
	inline bool hasSpeciesTree() { return speciestree != nullptr; }
	inline bool hasGeneTrees()   { return !genes_v.empty(); } 
	// Following is true iff gene trees and sp trees are present and GTP and MP scores have been computed and stored at least once
	inline bool hasInitializedTreeScores()   { return initializedTreeScores; } 
	inline void setInitializedTreeScores(bool flag)   { initializedTreeScores=flag; } 

	TSASData() { alignment_set = nullptr; }
	TSASData(AlignmentSet &aset): alignment_set{&aset} {  }

	TSASData(AlignmentSet &aset,Input *treeFileInput,enum TreeSetType treeset_type):alignment_set{&aset} 
		{
		switch (treeset_type)
			{
			case NEITHER:  		// likely case when genetrees are in alignment files and no sptree given
				break; 		
			case BOTH:		// both sptree and genetrees are given in file
				readTrees(*treeFileInput); 
				break;
			case SPECIESTREE:	// likely case when genetrees are in alignment files and sptree is given
				readSpeciesTree(*treeFileInput);
				break;
			case GENETREES: 	// likely case when genetrees in treeset file but no sptree given
				readGeneTrees(*treeFileInput);
				break;
			default: break;
			}	


		if (treeset_type==BOTH || treeset_type==GENETREES) 
		// assume a genetree is present in treeset file for each alignment, save these as 'Genes',
		// and ignore any starting trees that were found in the individual alignment files
			{
			for (int i=0;i<alignment_set->alignments.size();i++)
				{
				Alignment *alignment = alignment_set->alignments[i];
				GeneTreeUnrooted* tree = genetree_unrooted[i];
				TreeScore tscore;
				Gene gene = {alignment,tree,tscore};
				genes_v.push_back(gene);
				alignment->initializeEncodedSeqsAtNodes(tree); // used for (old) non-view-based MP calculations; consider
				}
			}
		else	// go check if they were loaded as starting trees with the alignments
		for (Alignment *alignment:alignment_set->alignments)
			{
			if (alignment->hasValidGeneTree) // meaning a starting tree PersistentTree
				{
				GeneTreeUnrooted* tree  = new GeneTreeUnrooted;
				tree -> newick2tree( alignment->starting_tree.newick() );
				tree -> weight = 1.0;
				genetree_unrooted.push_back(tree);
				TreeScore tscore;
				Gene gene = {alignment,tree,tscore};
				genes_v.push_back(gene);

				alignment->initializeEncodedSeqsAtNodes(tree); // used for (old) non-view-based MP calculations; consider
										// removing to save RAM later
				}

			}
		if (genetree_unrooted.size() == 0) 
			fatal("No gene trees were provided in either the input tree file or the alignment files");
		}




#if 0
	TSASData(AlignmentSet &aset,Input *treeFileInput,enum TreeSetType treeset_type,unsigned int gtp_weight, unsigned int mp_weight):
			TSASData(aset,treeFileInput,treeset_type)
			{
			if (hasGeneTrees())
				{
			initializeMPScores(gtp_weight,mp_weight);
				if (hasSpeciesTree())
					{
					initializeGTPScores(gtp_weight,mp_weight);
					scoreTotal(gtp_weight, mp_weight);
					}
				}
			}
#endif


	// NB! The following is a species case of the previous more general constructor. Probably can be deleted.
	// This constructor takes an alignment set with optional starting trees, and a required tree file for input.
	// The tree file MUST start with the starting species tree. Optionally it can include an entire set of unrooted gene trees.
	// The tree file takes precendence over any starting trees found while reading the alignment files. 

	// The object-wide treescore is not initialized mainingfully by the constructor

	TSASData(AlignmentSet &aset,Input &treeFileInput):alignment_set{&aset} // address of reference fives as a pointer to alignment set
		{
//		readSpeciesTree(spInput); // inherited from TreeSet
//cout << "Messy constructor TSASData" << endl; 
		readTrees(treeFileInput);
		if (genetree_unrooted.size()>0) // we did find (default unrooted) gene trees in the input file; assume one is present for each alignment
						// and ignore any starting trees that were found in the individual alignment files
			{
			for (int i=0;i<alignment_set->alignments.size();i++)
				{
				Alignment *alignment = alignment_set->alignments[i];
				GeneTreeUnrooted* tree = genetree_unrooted[i];
				TreeScore tscore;
				Gene gene = {alignment,tree,tscore};
				genes_v.push_back(gene);
				alignment->initializeEncodedSeqsAtNodes(tree); // used for (old) non-view-based MP calculations; consider
				}
			}
		else	// no  genetrees were read in the input treefile, go check if they were loaded as starting trees with the alignments
		for (Alignment *alignment:alignment_set->alignments)
			{
			if (alignment->hasValidGeneTree) // meaning a starting tree PersistentTree
				{
				GeneTreeUnrooted* tree  = new GeneTreeUnrooted;
				tree -> newick2tree( alignment->starting_tree.newick() );
				tree -> weight = 1.0;
				genetree_unrooted.push_back(tree);
				TreeScore tscore;
				Gene gene = {alignment,tree,tscore};
				genes_v.push_back(gene);

				alignment->initializeEncodedSeqsAtNodes(tree); // used for (old) non-view-based MP calculations; consider
										// removing to save RAM later
				}

			}
		if (genetree_unrooted.size() == 0) 
			fatal("No gene trees were provided in either the input tree file or the alignment files");
		}


	TSASData(TSASDataPersistent &dp); // implementations are below
	TSASData(TSASDataPersistent &dp,std::vector<bool> &mask);

	void replaceSpeciesTree(PersistentTree<SpeciesTree> new_sptree)
		{
			//if (speciestree != nullptr) 
			//	delete speciestree;
			//else
				readSpeciesTree(new_sptree.newick());
		}

	// When we change just the sptree after a species tree search (leaving gene trees the same),
	// we need to update all the gene trees' GTP scores without changing their MP scores. Do this
	// on a TSASData object, then convert to TSASDataPersistent later 
	void updateGTPScores(PersistentTree<SpeciesTree> new_sptree,double gtp_weight,double mp_weight)
		{
			replaceSpeciesTree(new_sptree);
			createLeafMapping();
			speciestree->preprocessLCA();
			TreeScore tree_score(0,0,0,gtp_weight,mp_weight);
			//for (auto gene:genes_v)
			for (int i = 0; i< genes_v.size();i++)
				{
				//TreeScore score = *(gene.tree_score);
				struct Gene& gene = genes_v.at(i); // gives a reference to this
				TREE *t0=gene.tree;
				createPrimaryMapping(*t0); 
				unsigned int gtpScore = getScore(*t0);
				TreeScore &ts = gene.tree_score;
				ts.setGTPScore(gtpScore);

				//(gene.tree_score).setGTPScore(gtpScore);	
				if (gtpScore < gene.alignment->bestever_gtp_score)
					gene.alignment->bestever_gtp_score=gtpScore;
				tree_score = tree_score + gene.tree_score;
				}	
			set(tree_score); // this is the total score
			return;
		}
	// This also initializes weights; don't need to do that in MP initializer, since has to be done here for weights to mean much
	void initializeGTPScores(double gtp_weight,double mp_weight)
		{
		if (!(hasSpeciesTree() && hasGeneTrees())) 
				EXCEPTION("Missing species or gene trees in initializer.");
		createLeafMapping();
		speciestree->preprocessLCA();
			//for (auto gene:genes_v)
		for (int i = 0; i< genes_v.size();i++)
			{
			//TreeScore score = *(gene.tree_score);
			struct Gene& gene = genes_v.at(i); // gives a reference to this
			TREE *t0=gene.tree;
			createPrimaryMapping(*t0); 
			unsigned int gtpScore = getScore(*t0);
			TreeScore &ts = gene.tree_score;
			ts.setGTPScore(gtpScore);
			ts.setGTPWeight(gtp_weight);
			ts.setMPWeight(mp_weight);
			//(gene.tree_score).setGTPScore(gtpScore);	
			gene.alignment->bestever_gtp_score=gtpScore;
			}	
		return;
		}
	typedef GeneTreeUnrooted TREE;
	void initializeMPScores(double gtp_weight, double mp_weight)
		{
		if (!hasGeneTrees()) 
				EXCEPTION("Missing gene trees in initializer.");
		for (int i = 0; i< genes_v.size();i++)
			{
			struct Gene& gene = genes_v.at(i); // gives a reference to this
			TREE *t0=gene.tree;
			unsigned int mpScore = gene.alignment->computeMPScore(t0);
			TreeScore &ts = gene.tree_score;
			ts.setMPScore(mpScore);	
			ts.setGTPWeight(gtp_weight);
			ts.setMPWeight(mp_weight);
			gene.alignment->bestever_mp_score=mpScore;
			}
		return ;
		}
	// If species trees and gene trees existin in this TSAS, here we can initialize all genes' treescores and the total one
	// This function initializes treescores COMPLETELY and that's what the function hasInitializedTreeScores() means!

	// ** Currently called from pqInitializeScores() in tsas-heuristic()

	void initializeScores(double gtp_weight, double mp_weight)
		{
		TreeScore score,totalScore;
		if (!(hasSpeciesTree() && hasGeneTrees())) 
			EXCEPTION("Missing species or gene trees in TSASData::initializeScores().");
		initializeMPScores(gtp_weight, mp_weight);
		initializeGTPScores(gtp_weight, mp_weight);
		totalScore.setScores(0,0,0,gtp_weight,mp_weight); // weights are set initially and TreeScore + operator does not change them.
		for (int i = 0; i< genes_v.size();i++)
			{
			struct Gene& gene = genes_v.at(i); // gives a reference to this
			totalScore = totalScore + gene.tree_score;
			}
		set(totalScore);
		setInitializedTreeScores(true);
		return ;	
		}

	void scoreTotal(double gtp_weight, double mp_weight)
		{
		TreeScore score,totalScore;
		if (!(hasSpeciesTree() && hasGeneTrees())) 
			EXCEPTION("Missing species or gene trees in TSASData::scoreTotal().");
		totalScore.setScores(0,0,0,gtp_weight,mp_weight); // weights are set initially and TreeScore + operator does not change them.
		for (int i = 0; i< genes_v.size();i++)
			{
			struct Gene& gene = genes_v.at(i); // gives a reference to this
			totalScore = totalScore + gene.tree_score;
			}
		set(totalScore);
		return ;	
		}

inline AlignmentSet* getAlignmentSet() {return alignment_set;}

inline std::vector<Gene>& genes()
	{
	return genes_v;
	}
friend ostream & operator << (ostream &os, TSASData &t) ;

#if 0
// At the moment I do not put transient TSAS objects into PQs, but see below for persistent ones...
// Next is used for comparison in the priority queue
// NB. In contrast to the SPRTree<SpeciesTree> class, which compares GTP scores, the TSAS class uses total score
friend bool operator<(const TSASData& t1, const TSASData& t2) // NB. Notice this requires friend and const qualifier
		{
		return t1.getTotalScore() > t2.getTotalScore(); // uses the less than operator of PersistentTree above
		}
#endif
};

// Have to spell out this overload because of multiple inheritance of this overload from treeset and treescore classes
ostream & operator << (ostream &os, TSASData &t) {
	// Right now I am copying treeset version
	// Maybe print tree scores too?
	os << t.speciestree << ";" << endl;
	for(int i = 0; i < t.genetree_rooted.size(); i++ ) os << t.genetree_rooted[i] << ";" << endl;
	for(int i = 0; i < t.genetree_unrooted.size(); i++ ) os << t.genetree_unrooted[i] << ";" << endl;
	return os;
}

//***********************************************************************************************************************
//***********************************************************************************************************************
//***********************************************************************************************************************
//***********************************************************************************************************************

class TSASDataPersistent: public TreeScore // inherit this TreeScore to have a score for the overall tree set
	{
	bool initializedTreeScores = false;

	public:
		unsigned int id;
		AlignmentSet *alignment_set;
		
		PersistentTree<SpeciesTree> sptree;	
		vector<PersistentTree<GeneTreeUnrooted> > ugtrees;	
		//vector<PersistentTree<GeneTreeRooted> > rgtrees;	
		vector<TreeScore> gene_tree_scores;

		inline unsigned int nGeneTrees () {return ugtrees.size();}

	public:
		inline bool hasInitializedTreeScores()   { return initializedTreeScores; } 
		inline void setInitializedTreeScores(bool flag)   { initializedTreeScores=flag; } 

		TSASDataPersistent(AlignmentSet &aset,Input *treeFileInput,enum TreeSetType treeset_type):alignment_set{&aset} 
		{
		std::vector<string> newick_trees;
		int size;
		if (treeset_type != NEITHER)
			{
			newick_trees = treeFileInput->readTreeSetFileAsNewick();
			for (auto nw:newick_trees) 
				cout << nw << endl; 
			size = newick_trees.size();
			}
		switch (treeset_type)
			{
			case NEITHER:  		// likely case when genetrees are in alignment files and no sptree given
				break; 		
			case BOTH:		// both sptree and genetrees are given in file
				if (size < 2) EXCEPTION("Number of trees read is fewer than 2 but expected species and gene trees");
				for (int i = 0; i < size; i++)
					{
					auto nw = newick_trees[i];
					if (i==0)
						sptree = PersistentTree<SpeciesTree>(nw);
					else
						{
						ugtrees.push_back(PersistentTree<GeneTreeUnrooted>(nw));
						TreeScore tsc;
						gene_tree_scores.push_back(tsc);
						}
					}
				break;
			case SPECIESTREE:	// likely case when genetrees are in alignment files and sptree is given
				if (size == 0) EXCEPTION("Number of trees read is 0 but expected one species tree ");
				if (size > 1) EXCEPTION("Number of trees read is greater than 1 but expected one species tree only");
				sptree = PersistentTree<SpeciesTree>(newick_trees[0]);
				break;
			case GENETREES: 	// likely case when genetrees in treeset file but no sptree given
				if (size == 0) EXCEPTION("Number of trees read is 0 but expected at least one gene tree ");
				for (auto nw:newick_trees)
						{
						ugtrees.push_back(PersistentTree<GeneTreeUnrooted>(nw));
						TreeScore tsc;
						gene_tree_scores.push_back(tsc);
						}
				break;
			default: break;
			}	


		if (treeset_type == NEITHER || treeset_type== SPECIESTREE) // Probably has gene trees only in alignments files
					// In the future we might build genetrees from alignments by leaf addition...
			for (Alignment *alignment:alignment_set->alignments)
				{
				if (alignment->hasValidGeneTree) // meaning a starting tree PersistentTree
					{
					ugtrees.push_back(alignment->starting_tree);
					TreeScore tsc;
					gene_tree_scores.push_back(tsc);
					}
				}
		}


		TSASDataPersistent(AlignmentSet *as): alignment_set{as} {};

		TSASDataPersistent(TSASData  &ts):alignment_set{ts.getAlignmentSet()}
			{
			setInitializedTreeScores(ts.hasInitializedTreeScores());	
			if (ts.speciestree != nullptr)
				sptree = PersistentTree<SpeciesTree>(ts.speciestree);
			set(ts.getTreeScore());
			for (int i=0;i<ts.genetree_unrooted.size();i++)
				ugtrees.push_back(PersistentTree<GeneTreeUnrooted>(ts.genetree_unrooted[i]));
			//for (int i=0;i<ts.genetree_rooted.size();i++)
			//	rgtrees.push_back(PersistentTree<GeneTreeRooted>(ts.genetree_rooted[i]));
			for (auto gene : ts.genes()) 
				{
				gene_tree_scores.push_back(gene.tree_score);
				}

			}

		// Construct an object, but ignore any gene trees (and scores) not present in the bool vector mask
		TSASDataPersistent(TSASData  &ts, std::vector<bool> *mask):alignment_set{ts.getAlignmentSet()}
			{
			setInitializedTreeScores(ts.hasInitializedTreeScores());	
			sptree = PersistentTree<SpeciesTree>(ts.speciestree);
			set(ts.getTreeScore());
			for (int i=0;i<ts.genetree_unrooted.size();i++)
				{
				if (mask->at(i))
					{
					ugtrees.push_back(PersistentTree<GeneTreeUnrooted>(ts.genetree_unrooted[i]));
					auto gene = ts.genes()[i];
					gene_tree_scores.push_back(gene.tree_score);
					}
				else	// keep the following, have to push back these to instantiate the whole vector, duh
					{
					ugtrees.push_back(PersistentTree<GeneTreeUnrooted>());
					TreeScore tsc;
					gene_tree_scores.push_back(tsc);
					}


				}
			}
		// Summarize scores across gene set after searches, assuming those scores have been saved as gene.treeScore
		TreeScore totalScore(double gtp_weight, double mp_weight)
			{
			TreeScore total_score(0,0,0,gtp_weight,mp_weight); 
			for (auto score : gene_tree_scores) 
				{
//cout << "Score per gene: " << score << endl;
				total_score = total_score + score;
				}
			return total_score;	
			}

		string peek()  
			{
			ostringstream os;	
			TreeScore score = getTreeScore();
			os << "Peek at TSAS Object with checksum = " << checksum() << " having scores: "<< score << endl;
			os <<  finalReport().str()<<endl;
			os << "...." << endl;
			return os.str();
			}
		string peekCheck()  
			{
			ostringstream os;	
			TreeScore score = getTreeScore();
			int gtp,mp,total;
			gtp=mp=total=0;
			sumScores(gtp,mp,total);
			os <<  checksum() << "(id="<<id<<") : "<< score ;
			if (score.getGTPScore() != gtp || score.getMPScore() != mp || score.getTotalScore() != total)
				{
				os <<  checksum() << "(id="<<id<<") : "<< score << " :: " << gtp << " " << mp << " " << total ;
				cout << os.str() << endl;
				EXCEPTION("peekCheck failed. See previous line for mismatches in scores");
				}
			return os.str();
			}
		// Merges the gene trees and their scores from a TSASDP that might have some gene trees missing ("empty")
		// Copies the gene trees and scores from 'ts' to this if the ts genetree is present.
		// Genetrees in  'this' might be overwritten, or they might just stay as is, depending on what's present
		// in ts.
		// The sp tree from 'this' object not affected (but it better be there).
		void replaceGeneTreeSubset(TSASDataPersistent *ts, std::vector<bool> *mask)
			{
			for (int i=0;i<ts->ugtrees.size();i++)
			    {
			    if (mask->at(i))
				{
				//ugtrees.at(i) = ts->ugtrees[i]; //doesn't matter
				ugtrees[i] = ts->ugtrees[i];
				gene_tree_scores.at(i) = ts->gene_tree_scores[i];
				}
			    }
			}
		bool treeScoresEqual(TSASDataPersistent &tp)
			{
			// Useful to check redundancy of TSAS objects by just checking their sets of treescores.
			// This is not exactly the same as tree equality check!!!
			// first check overall treescore
			//if (this->getTotalScore() != ts.getTotalScore()) return false; ... really only need to check two of these
			if (this->getGTPScore() != tp.getGTPScore()) return false;
			if (this->getMPScore() != tp.getMPScore()) return false; 
			// Then all treescores of gene alignments
			for (int i=0;i<gene_tree_scores.size();i++)
				{
				auto ts1 = gene_tree_scores[i];
				auto ts2 = tp.gene_tree_scores[i];
				if (ts1.getGTPScore() != ts2.getGTPScore()) return false;
				if (ts1.getMPScore() != ts2.getMPScore()) return false;
				}	
			return true;
			}

		// Another approximate check that treesets are equal. This one leverages the (new) ability to compare species
		// trees using the sorted newick string storage of PersistentTree<SpeciesTree> in TSASDataPersistent objects.
		// But then it again uses just the numerical comparisons of scores rather than checking all the gene trees formally.
		bool treesetsEqual(TSASDataPersistent &tp)
			{
			// fast, do this first
			if (this->getGTPScore() != tp.getGTPScore()) return false;
			if (this->getMPScore() != tp.getMPScore()) return false; 

			if (!(this->sptree == tp.sptree)) return false; // uses the overloaded == operator of TSASDataPers class

			// Then all treescores of gene alignments
			for (int i=0;i<gene_tree_scores.size();i++)
				{
				auto ts1 = gene_tree_scores[i];
				auto ts2 = tp.gene_tree_scores[i];
				if (ts1.getGTPScore() != ts2.getGTPScore()) return false;
				if (ts1.getMPScore() != ts2.getMPScore()) return false;
				}	
			return true;
			}
		// A Fletcher64 checksum modified from various sources
		// Based only on the gene trees' GTP and MP scores. Not on the species tree at all,
		// though this checksum will typically be affected by changes in either genetree or species tree
		uint64_t checksum()
			{
			const int32_t size = gene_tree_scores.size();
			uint32_t data[2*size];
			for (int i=0;i<size;i++)
				{
				auto ts1 = gene_tree_scores[i];
				data[2*i] = ts1.getGTPScore();
				data[2*i+1] = ts1.getMPScore();
				}
			uint64_t sum = fletcher64(data,2*size);
			return sum;
			}


		inline void setTreeScore(TreeScore ts) {set(ts);}

		inline string getSpeciesTree(){ return sptree.newick();}

		inline void setSpeciesTree(PersistentTree<SpeciesTree> pt) {sptree = pt;}

		void print()
			{
			msgout << "      Species Tree: " << sptree.newick() << endl;
			for (int i=0;i<ugtrees.size();i++)
				msgout << "Unrooted Gene Tree: " << ugtrees[i].newick() << endl;
			//for (int i=0;i<rgtrees.size();i++)
			//	msgout << "  Rooted Gene Tree: " << rgtrees[i].newick() << endl;
			for (auto score:gene_tree_scores)
				msgout << score << endl;
			printScores(" ** Persistent tree overall scores: ** ");
			}
	string rowOfScores(enum CriteriaType crit)
		{
		ostringstream os;
		for (TreeScore ts:gene_tree_scores)
			{
			unsigned int score;
			//if (crit==GTP) score = ts.getGTPScore();
			//else if (crit == MP) score = ts.getMPScore();
			//else if (crit == GTP_MP) score = ts.getTotalScore();
			//else fatal ("Invalid score in rowOfScores");	
			//os << score << "\t";
			os << ts.getMPScore()<<"/"<<ts.getGTPScore() << "\t";
			}	
		return os.str();
		}	

	void sumScores(int &gtp, int &mp, int &total) 
		{
		for (int i=0;i<alignment_set->alignments.size();i++)
			{
			auto align = alignment_set->alignments[i];
			TreeScore score = gene_tree_scores[i];
			gtp+=score.getGTPScore();
			mp+= score.getMPScore();
			total+=score.getTotalScore();
			}
		}
	void vecStats(vector<int> v, int& min, int& max, float& mean)
		{
		int size = v.size();
		float sum=0.0;
		min=INT_MAX;
		max=INT_MIN;
		for (int x:v)
			{
			sum += x;
			if (x < min) min=x;
			if (x > max) max=x;
			}
		mean = sum/size;
		return;
		}
	void vecStatsVCV(vector<int> v1, vector<int> v2, float& cov, float& corr, float& var1, float& var2)
		{
		// Use two pass method to avoid numerical issues
		if (v1.size() != v2.size()) EXCEPTION("Vectors have different sizes in vecStatsVCV");
		float sum1=0.0,sum2=0.0,sumsq1=0.0,sumsq2=0.0,sumcv=0.0;
		int n = v1.size();
		float x1,x2;
		for (int i =0; i<n;i++)
			{
			x1 = v1[i];
			x2 = v2[i];
			sum1 += x1; 
			sum2 += x2;
			}
		float mean1=sum1/n;
		float mean2=sum2/n;
		for (int i =0; i<n;i++)
			{
			x1 = v1[i];
			x2 = v2[i];
			sum1 += x1; 
			sum2 += x2;
			sumsq1 += (x1-mean1)*(x1-mean1);
			sumsq2 += (x2-mean2)*(x2-mean2);
			sumcv  += (x1-mean1)*(x2-mean2);
			}
		cov = sumcv/n;
		var1= sumsq1/n;
		var2= sumsq2/n;
		corr= sumcv/sqrt(sumsq1*sumsq2);
		return;
		}

	string vecHisto(vector<int> v)
		{
#define LINEAR_SCREEN_WIDTH 80
		ostringstream os;
		int min,max; 
		float mean;
		vecStats(v,min,max,mean);

		int num_elem = max-min+1;
		unsigned int  histo[num_elem]; // defaults to inits of 0
		for (int i=0;i<num_elem;i++) histo[i]=0;
		for (int x:v)
			{
			int bin = x-min;
			histo[bin]++;
			}
		int max_count = INT_MIN;
		unsigned int right_label;
		for (int count:histo)
			{
			if (count > max_count) max_count = count;
			}
		float linear_screen_fraction;
		if (max_count > LINEAR_SCREEN_WIDTH) 
			{
			linear_screen_fraction = LINEAR_SCREEN_WIDTH/(float)max_count;
			right_label = max_count;
			}
		else
			{
			linear_screen_fraction = 1.0;	
			right_label=LINEAR_SCREEN_WIDTH;
			}
		os << "        1                   10                  100                 1000                10000"<<endl;
		os << charX("-",LINEAR_SCREEN_WIDTH+8) << endl;
		for (int i=0;i<num_elem;i++)
			{
			int count = histo[i]*linear_screen_fraction;
			float lcount;
			if (histo[i]==0) 
				lcount = 0; // hack for arg of 0 to log
			else
				lcount = int(log10(histo[i])*LINEAR_SCREEN_WIDTH/4.0);
			int val = i+min;
			os << val << "\t" << charX("*",lcount) << endl;
			}
		os << charX("-",LINEAR_SCREEN_WIDTH+8) << endl;
		return os.str();
		}

	string charX(string c, unsigned int times) // concat the same char many times as a string
		{
		ostringstream os("");
		for (int i=0;i<times;i++) os << c;
		return os.str();
		}

	ostringstream finalReport() 
		{
		unsigned int size_histo=50;
		vector<int>histo;
		for (int i=1;i<=size_histo;i++)histo.push_back(0);
		ostringstream os;
		os << "********************************************************************************" << endl;
		os << "Results by alignment" << endl;
		//os << "Align\tFile\t\t\tnGenes\tnSpp\tGTP\tMP\tTotal" << endl;
		os << "Align\tFile\t\t\tnGenes\tnSpp\tGTP\tMP\tTotal\tBestGTP\tDelta\tBestMP\tDelta" << endl;
		//msgout << "Align\tFile\t\tnGenes\tnSpp\tGTP\tMP\tTotal" << endl;
		int sum_delta_gtp=0,sum_delta_mp=0, sum_delta,sum_gtp=0,sum_mp=0;
		int count_0_dups=0;
		int count_0_mpdelta=0;
		int count_0_gtpdelta=0;
		int n_align = alignment_set->alignments.size();
		float sum_dups_per=0;
		int total_genes = 0;
		int total_sites=0;
		vector<int> nSpps;
		vector<int> nGenes; // n leaves of gene trees
		vector<int> mpDeltas;
		vector<int> gtpDeltas;
		vector<int> alignment_lengths; //hack to use an int here
		int min,max; float mean;
		for (int i=0;i<alignment_set->alignments.size();i++)
			{
			auto align = alignment_set->alignments[i];
			string fn = alignment_set->filenames[i];
			fn.resize(20,' '); // pad names out to 20 spaces   
			TreeScore score = gene_tree_scores[i];
			int deltaGTP = score.getGTPScore()-align->bestever_gtp_score;
			int deltaMP = score.getMPScore()-align->bestever_mp_score;
			mpDeltas.push_back(deltaMP);
			gtpDeltas.push_back(deltaGTP);
			sum_delta_gtp+= deltaGTP;
			sum_delta_mp += deltaMP;

			int n_genes = align->ntax;
			int n_spp = align->nspp;
			unsigned n_char = align->nchar;
			nSpps.push_back(n_spp);
			nGenes.push_back(n_genes);
			
			alignment_lengths.push_back(n_char);
			total_sites += n_char;
			total_genes += n_genes;
			int gtp = score.getGTPScore();
			float dups_per_gene = (float)gtp/n_genes;
			sum_dups_per += dups_per_gene;
			if (gtp==0) ++count_0_dups;
			if (deltaGTP==0) ++count_0_gtpdelta;
			if (deltaMP==0) ++count_0_mpdelta;


			os << i << "\t" << fn << "\t" << align->ntax << "\t" << align->nspp << "\t" ;	
			os << score.getGTPScore() << "\t" << score.getMPScore()<<"\t"<<score.getTotalScore()<<"\t";
			os <<  align->bestever_gtp_score << "\t" << deltaGTP<< "\t" << align->bestever_mp_score << "\t"<<deltaMP << endl;
			sum_gtp+=score.getGTPScore();
			sum_mp+= score.getMPScore();
			}
		sum_delta = sum_delta_gtp+sum_delta_mp;
		os << "---------------------------------------------------------------------------------" << endl;

		os << "TSAS score string: " << printScores()<<endl;	

		os << "Summary statistics on the "<<n_align<< " alignments used" << endl;
		vecStats(nGenes,min,max,mean);
		os << "  Genes (leaves) per gene tree (min - mean - max): "<< min << " - " << mean << " - " << max << endl;
		vecStats(nSpps,min,max,mean);
		os << "  Species per gene tree (min - mean - max): "<< min << " - " << mean << " - " << max << endl;
		vecStats(alignment_lengths,min,max,mean);
		os << "  Sites per alignment (min - mean - max): "<< min << " - " << mean << " - " << max << " (Total sites=" << total_sites << ")" << endl;

		os << endl << "Results:"<<endl;
		os << "  Sum of Scores: GTP = " << sum_gtp       << "  MP = " << sum_mp      <<  " Both = " << sum_gtp+sum_mp << endl;
		os << "  Sum of Deltas: GTP = " << sum_delta_gtp << "  MP = " << sum_delta_mp << " Both = " << sum_delta << endl;
		os << "  Mean delta GTP = " << (float)sum_delta_gtp/n_align << "  Mean delta MP = " << (float)sum_delta_mp/n_align << endl;

		os << "  Number of alignments with GTP score == 0: " << count_0_dups << "(" << 100*(float)count_0_dups/n_align << "%)"<<endl;
		os << "  Number of alignments with Delta GTP score == 0: " << count_0_gtpdelta << "(" << 100*(float)count_0_gtpdelta/n_align << "%)"<<endl;
		os << "  Number of alignments with Delta MP score == 0: " << count_0_mpdelta << "(" << 100*(float)count_0_mpdelta/n_align << "%)"<<endl;
		os << "  Mean duplications per gene: "<< (float)sum_gtp/total_genes << " [Total genes = " << total_genes<<"]"<<endl; 
		os << "  Mean alignment-wise duplications per gene: "<< sum_dups_per/n_align << endl; 

		vecStats(gtpDeltas,min,max,mean);
		os << endl << "  **** GTP delta report ****" << endl;
		os << "  GTP delta per gene (min - mean - max): "<< min << " - " << mean << " - " << max << endl;
		os << "  Histogram of count of genes with particular GTP delta:" << endl;
		os << vecHisto(gtpDeltas);
		vecStats(mpDeltas,min,max,mean);
		os << endl << "  **** MP delta report ****" << endl;
		os << "  MP delta per gene (min - mean - max): "<< min << " - " << mean << " - " << max << endl;
		os << "  Histogram of count of genes with particular MP delta:" << endl;
		os  << vecHisto(mpDeltas);

		float cov,corr,var1,var2;
		vecStatsVCV(gtpDeltas, mpDeltas, cov, corr, var1,var2 );
		os << endl << "  Correlation between GTP and MP delta statistics = "<< corr << endl;
		return os;  
		}



	// Next is used for comparison in the priority queue
	// NB. In contrast to the SPRTree<SpeciesTree> class, which compares GTP scores, the TSAS class uses total score
	friend bool operator<(const TSASDataPersistent& t1, const TSASDataPersistent& t2)  //NB. Notice this requires friend and const qualifier
		{
		return t1.getTotalScore() > t2.getTotalScore(); // uses the less than operator of PersistentTree above
		}
	friend ostream & operator << (ostream &os, TSASDataPersistent &t) ;
	};

ostream & operator << (ostream &os, TSASDataPersistent &t) {
	os << t.sptree.newick() << ";" << endl;
	for(int i = 0; i < t.ugtrees.size(); i++ ) os << t.ugtrees[i].newick() << ";" << endl;
	//for(int i = 0; i < t.rgtrees.size(); i++ ) os << t.rgtrees[i].newick() << ";" << endl;
	return os;
}
//***************************************************************************************************
// Create brand new TSAS object from a persistent version
// [Maybe not thread safe across gene subsets, since we clobber all genetrees, gene alignments here...]

TSASData::TSASData(TSASDataPersistent &dp)
	{
	setInitializedTreeScores(dp.hasInitializedTreeScores());	

//cout << "TSAS constructor from persistent object" << endl;
	// instantiates EMPTY superclass TreeSet before the following
	set(dp.getTreeScore());	 // initialize the overall tree score
	alignment_set = dp.alignment_set;
	if (dp.sptree.exists())
		readSpeciesTree( dp.sptree.newick() );  // this is the newick version of this treeset member function, needed for constraints
		// Notice that we sometimes might instantiate this without a species tree (say, before building one with leaf addition)

//cout << "SPTREE SIZE = " << speciestree->setupNLeaves() << endl;
//cout << speciestree->tree2newickSorted() << endl;
	for (int i=0;i<dp.ugtrees.size();i++)
		{
		GeneTreeUnrooted* tree  = new GeneTreeUnrooted;
		tree -> newick2tree( dp.ugtrees[i].newick() );
//cout << "TREE SIZE = " << tree->setupNLeaves() << endl;
//cout << tree->tree2newickSorted() << endl;
		genetree_unrooted.push_back(tree);
		//TreeScore ugtscore = dp.gene_tree_scores[i];
		Alignment *alignment = alignment_set->alignments[i];
		//alignment->treeScore = ugtscore;

		Gene gene = {alignment,tree,dp.gene_tree_scores[i]};
		genes_v.push_back(gene);

		alignment->initializeEncodedSeqsAtNodes(tree); // used for (old) non-view-based MP calculations; consider
		}								// removing to save RAM later
	}
TSASData::TSASData(TSASDataPersistent &dp,std::vector<bool> &mask)
	{
	setInitializedTreeScores(dp.hasInitializedTreeScores());	
	set(dp.getTreeScore());	 // initialize the overall tree score
	alignment_set = dp.alignment_set;
	readSpeciesTree( dp.sptree.newick() ); 

	for (int i=0;i<dp.ugtrees.size();i++)
		{
		if (mask[i])
			{
			GeneTreeUnrooted* tree  = new GeneTreeUnrooted;
			tree -> newick2tree( dp.ugtrees[i].newick() );
			genetree_unrooted.push_back(tree);
			//TreeScore ugtscore = dp.gene_tree_scores[i];
			Alignment *alignment = alignment_set->alignments[i];
			//alignment->treeScore = ugtscore;

			Gene gene = {alignment,tree,dp.gene_tree_scores[i]};
			//Gene gene = {alignment,tree,&ugtscore}; // error to use &ugtscore? Didn't test; not using this here, but see TSASDP above
			genes_v.push_back(gene);
	
			alignment->initializeEncodedSeqsAtNodes(tree); 
			}
		else //init with null-ish stuff
			{
			genetree_unrooted.push_back(nullptr);
			TreeScore tsc;
			Gene gene = {nullptr,nullptr,tsc};
			genes_v.push_back(gene);
			// don't need to initialize encoded seqs here; will never be relied on
			}
		}								
	}





//*************************** SOME GLOBAL FUNCTIONS OPERATING ON PQS ******************************************
// TO DO: Perhaps we will extend the priority_queue class by something nice using composition technique
// Would also put the sample function here instead of in the Heuristic-PQ algorithm code...

// truncate pq to have no more than size elements; clunky but seems necessary
	void pqTruncate(std::priority_queue<TSASDataPersistent> &pq, unsigned int size)
		{
		std::priority_queue<TSASDataPersistent> new_pq;
		while (!pq.empty() && size-- > 0)
			{
			new_pq.push(pq.top());
			pq.pop();
			}
		pq = new_pq; // ok bc uses copy constructor
		} 		

// Get a vector of total scores from a priority queue of TSASDataPersistent objects; reconstitute the pq so it
// isn't borked by this operation....
	std::vector<double> pqGetScoreVector(std::priority_queue<TSASDataPersistent> &pq)
		{
		std::priority_queue<TSASDataPersistent> new_pq;
		std::vector<double> v;
		while (!pq.empty())
			{
			TSASDataPersistent dp = pq.top();
			new_pq.push(dp);
			pq.pop();
			v.push_back(dp.getTotalScore());
			}
		pq = new_pq;
		return v;
		} 		


// Get a vector of of TSASDataPersistent objects from a PQ of same; reconstitute the pq so it
// isn't borked by this operation....
	std::vector<TSASDataPersistent> pqToVector(std::priority_queue<TSASDataPersistent> &pq)
		{
		std::priority_queue<TSASDataPersistent> new_pq;
		std::vector<TSASDataPersistent> v;
		while (!pq.empty())
			{
			TSASDataPersistent dp = pq.top();
			new_pq.push(dp);
			pq.pop();
			v.push_back(dp);
			}
		pq = new_pq;
		return v;
		} 		


// Deletes 'redundant' TSAS objects from a priority queue
// 'Redundant' means two treesets are '==' according to treeScoresEqual() function.
// This function is now modified to include an exact assessment of whether the sp trees are equal,
// together with the requirement that all genetree GTP and MP scores are equal between sets.
// [This could be made more efficient likely by using checksum function!]
// This does not mean all trees are topologically identical, though it is evidence in favor of that.
		void deleteRedundantTrees(std::priority_queue<TSASDataPersistent> &pq)
			{
			std::list<TSASDataPersistent> sol_list;
			std::list<TSASDataPersistent>::iterator s1,s2;
			while (!pq.empty())
				{
				TSASDataPersistent ts = pq.top();
				sol_list.push_back(ts);
				pq.pop();
				}
			if (sol_list.size() >= 2)
				{
				s1=sol_list.begin();
				++s1;
				for (;s1 != sol_list.end(); s1++) // start with second element
					{ // decide if we delete this element by checking if any earlier ones in list are "equal"
					for (s2=sol_list.begin();s2 != s1; s2++) 
						{
						TSASDataPersistent t1 = *s1;
						TSASDataPersistent t2 = *s2;
						//if (t1.treeScoresEqual(t2))
						if (t1.treesetsEqual(t2)) // changed October 2022
							{
							sol_list.erase(s2);
							break;
							}
						}
					
					}
				}
			int soln=0;
			//msgout << "Non-redundant solutions in priority queue" << endl;
			for (s1=sol_list.begin();s1 != sol_list.end(); s1++) 
				{
				++soln;
				TSASDataPersistent ts = *s1;
				//msgout << soln << "\t" << ts.printScores() << "\t"  << ts.getSpeciesTree()<<endl;
				pq.push(*s1);
				}
			return;
			}
// Alternative way to set up a priority queue using an external comparator class. 
#if 0
class TSASCompare
	{
	public:
	bool operator()(TSASData& t1, TSASData& t2)
		{
		return t1.getTotalScore() > t2.getTotalScore(); // uses the less than operator of PersistentTree above
		}
	};
#endif
#endif
