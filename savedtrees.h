// Important classes and functions for storing trees and their associated data.
// Key distinction between persistent trees and temporary trees that have volatile
// but useful data structures attached at the moment.

#ifndef SAVEDTREES_H
#define SAVEDTREES_H

#include "moves.h"

//*******************************************************************************
class TreeScore
	{
	protected:
	unsigned int gtp_score;
	unsigned int mp_score;
	unsigned int rooting_score;
	//unsigned int total_score;

	double gtp_weight;
	double mp_weight;

	public:
		TreeScore() {};
		TreeScore(unsigned int gtp, unsigned int mp, unsigned int rs,double gtpw=1.0, double mpw=1.0): gtp_score{gtp},mp_score{mp},rooting_score{rs},gtp_weight{gtpw},mp_weight{mpw} {};

		bool isBetterThan(TreeScore ts, enum CriteriaType crit)
			{ // don't use subtraction on unsigned int!
			if (crit == MP) return this->mp_score < ts.mp_score ? true : false;
			if (crit == GTP) return this->gtp_score < ts.gtp_score ? true : false;
			if (crit == GTP_MP) return  this->getTotalScore() < ts.getTotalScore() ? true : false;
			EXCEPTION("Incorrect criteria in TreeScore.isBetterThan()");
			}

		inline void set (TreeScore ts)
			{ setScores(ts.getGTPScore(),ts.getMPScore(),ts.getRootingScore(),ts.getGTPWeight(),ts.getMPWeight());	}

		inline void setScores(unsigned int gtp, unsigned int mp,unsigned int rs,double gtpw,double mpw) {gtp_score=gtp;mp_score=mp;rooting_score=rs; gtp_weight=gtpw; mp_weight=mpw; }

		TreeScore getTreeScore() 
			{
			return TreeScore(gtp_score,mp_score,rooting_score,gtp_weight,mp_weight);
			}
		inline void setGTPScore(unsigned int m) {gtp_score = m;}
		inline void setMPScore(unsigned int m) {mp_score = m;}
		inline void setRootingScore(unsigned int m) {rooting_score = m;}
		inline void setGTPWeight(double gtpw) {gtp_weight = gtpw;}
		inline void setMPWeight(double mpw) {mp_weight = mpw;}

// SHOULD BE UNSIGNED INT...
// NB. MAKE THESE CONST MEMBER FUNCTIONS, SO THAT WHEN THEY ARE USED BY CONST REFERENCE THINGS WILL BE OK (AS IN COMPARATORS FOR SORTING).
		inline int getGTPScore() const {return gtp_score;}
		inline int getMPScore() const {return mp_score;}

		// Notice we return an unsigned int here rather than double, mostly for pretty printing and simplifying stuff...
		inline unsigned int getTotalScore() const {return gtp_weight*gtp_score + mp_weight*mp_score;}

		inline int getRootingScore() const {return rooting_score;}
		inline double getGTPWeight() const {return gtp_weight;}
		inline double getMPWeight() const {return mp_weight;}

		// Useful for comparing scores of two saved trees...
		friend TreeScore operator-(const TreeScore &c1, const TreeScore &c2)
			{
			if (c1.gtp_weight != c2.gtp_weight || c1.mp_weight != c2.mp_weight) 
				fatal ("Attempting to use operator on two treescores with different weights");
			TreeScore t;		
			t.setGTPScore(c1.gtp_score-c2.gtp_score);
			t.setMPScore (c1.mp_score-c2.mp_score);
			t.setRootingScore (c1.rooting_score-c2.rooting_score);
			//t.setTotalScore (c1.total_score-c2.total_score);
			t.setGTPWeight(c1.gtp_weight); // the same in both objects, so just copy one of them to "sum"...
			t.setMPWeight(c1.mp_weight); // the same in both objects, so just copy one of them to "sum"...
			return t;
			}
		friend TreeScore operator+(const TreeScore &c1, const TreeScore &c2)
			{
			if (c1.gtp_weight != c2.gtp_weight || c1.mp_weight != c2.mp_weight) 
				{
				c1.printScores("TS1 ");
				c2.printScores("TS2 ");
				fatal ("Attempting to use operator on two treescores with different weights");
				}
			TreeScore t;		
			t.setGTPScore(c1.gtp_score+c2.gtp_score);
			t.setMPScore (c1.mp_score+c2.mp_score);
			t.setRootingScore (c1.rooting_score+c2.rooting_score);
			t.setGTPWeight(c1.gtp_weight); // the same in both objects, so just copy one of them to "sum"...
			t.setMPWeight(c1.mp_weight); // the same in both objects, so just copy one of them to "sum"...
			//t.setTotalScore (c1.total_score+c2.total_score);
			return t;
			}
		friend bool operator==(const TreeScore &c1, const TreeScore &c2)
			{
			if (c1.gtp_weight != c2.gtp_weight || c1.mp_weight != c2.mp_weight) 
				fatal ("Attempting to use operator on two treescores with different weights");
			if (c1.gtp_score!=c2.gtp_score) return false;
			if (c1.mp_score!=c2.mp_score) return false;
			if (c1.rooting_score!=c2.rooting_score) return false;
			//if (c1.total_score!=c2.total_score) return false;
			return true;
			}
		void printScores(string title) const
			{
			msgout << title << "\t";
			msgout << gtp_score << "\t" << mp_score << "\t" << getTotalScore() << "\t" << gtp_weight << "\t" << mp_weight;
			msgout << endl;
			}
		string printScores() const
			{
			ostringstream os;
			os << gtp_score << "\t" << mp_score << "\t" << getTotalScore() << "\t" << gtp_weight << "\t" << mp_weight;
			return os.str();
			}

	friend ostream & operator << (ostream &os, TreeScore &ts); // declaration
	};



ostream & operator << (ostream &os, TreeScore &ts) // inserter to print the data
	{
	//os << ts.gtp_score << "\t" << ts.mp_score << "\t" << ts.rooting_score << "\t" << ts.getTotalScore() << "\t";
	os << ts.gtp_score << "\t" << ts.mp_score << "\t" << ts.getTotalScore() << "\t";
	return os;
	}







/*
"Compressed" tree object useful for some purposes in the context of an NNI sequence object.
Stores data for the post tree corresponding to some element in the nni array in a local neighborhood.
This tree is thus 'relative' to the reference tree of the neighborhood.
It is 'mutable' in the sense that the reference tree might change during running. No permanent version of the 
reference tree is maintained.

IMPORTANT: The 'pos' position is offset by 1 from the array index in the NNI_Sequence array.
This lets us use the pos=0 to mean the reference tree, and then pos = 1..n to be the n elements of the
NNI_Seq array. We keep this funny business HERE only, and try to hide it with member functions.
Thus, assuming there are 2k elements in the nni_seq array
........................................................................................................................
nni_seq index			0       	1       	2       	3	...	2k-1
tree		t_0=reference		t_1		t_2		t_3		t_{2k-1}	t0=reference_tree
tree_pos		0		1		2		3		2k
........................................................................................................................

So we tour from reference tree, t_0, through 2k other trees, the last of which is back to the reference tree.
Some of these trees are repeated as they represent the nodes of a traversal of a tree itself.

nni_seq[i] describes a move from t_i to t_{i+1}, where t_i is the "pre-tree" and t_{i+1} is the "post-tree".

Note there is a bit of a disconnect between the relative-tree 'pos' index and the nni_seq array index. This is managed
with member functions that distinguish nni indexes by name.
		


Takes O(1) time to store tree info and trivial memory, but O(n) to retrieve the tree
because we rebuild the whole nni array in the local spr neighborhood and then
partly traverse that array until we get to a particular position.

TO DO: Could store a reference to a local SPR neighborhood, but right now we don't keep these several neighborhoods
for different source nodes lying around. If these local neighborhoods were all kept in memory, retrieval would not
require rebuilding the nni array for each neighborhood, BUT note that it still traverses the entire nni array to fetch
the tree (incl. traversing to its end to reset to reference tree).

*/


class NNISeqTree:public TreeScore
	{
	private:
	TREE* reference_tree;
	NODE* source_node; // optional parameter describing the source subtree in an SPR move
	int tree_pos;

	public:
		// DON'T WRITE A CONSTRUCTOR THAT OVERRIDES THE BASE CLASS CONSTRUCTOR : i.e., avoid storing a newick tree
		NNISeqTree() {};
		NNISeqTree(TREE* rt, NODE* src=nullptr, int tp=0):reference_tree{rt},source_node{src},tree_pos{tp} {};
		NNISeqTree(TREE* t, TreeScore ts, NODE* src=nullptr, int tp=0):reference_tree(t),source_node{src},tree_pos{tp} { set(ts); }

// NB: The following USED to include total score parameter...
// NB: And note that the set() function doesn't touch score weights, which is ok if they were initialized, but dangerous and inconsistent
		inline void set(unsigned int gtp, unsigned int mp, unsigned int rs, int tp, NODE* src=nullptr) {gtp_score=gtp;mp_score=mp; rooting_score=rs; tree_pos=tp;source_node=src;};
		inline void set (TreeScore ts)
			{ setScores(ts.getGTPScore(),ts.getMPScore(),ts.getRootingScore(),ts.getGTPWeight(),ts.getMPWeight());	}
		inline void setNNIPosition(int p) {tree_pos = p+1;}
		inline void setTreePosition(int p) {tree_pos = p;}
		inline void setPositionToReferenceTree() {tree_pos=0;}
		inline bool isReferenceTree() {return (tree_pos==0);}

		inline void setSource(NODE* src) {source_node=src;}
		inline void setReferenceTree(TREE* rt) {reference_tree=rt;}
		inline int getTreePosition() {return tree_pos;}
		inline int getNNIPosition() 
			{
			if (isReferenceTree()) EXCEPTION("Tried to fetch NNI position of reference tree");
			return tree_pos-1;
			}
		inline NODE*& getSource() {return source_node;} // return a reference: this return type is 
					// required for other funcs like getNodeName()...careful
		//TreeScore getTreeScore()
		//	{
		//	return TreeScore(gtp_score,mp_score,rooting_score,total_score);
		//	}
	};

class SPRSeqTree:public TreeScore
	{
	private:
	TREE* reference_tree;
	SPR spr;

	public:
		// DON'T WRITE A CONSTRUCTOR THAT OVERRIDES THE BASE CLASS CONSTRUCTOR : i.e., avoid storing a newick tree
		SPRSeqTree() { };
		SPRSeqTree(TREE* t): reference_tree(t),spr(t) { };
		SPRSeqTree(TREE* t, SPR s):reference_tree(t), spr{s} {};

		// init with SPR set to ref tree and given tree score
		SPRSeqTree(TREE* t, TreeScore ts):reference_tree(t) { set(ts); }

		// construct as a reference tree using that SPR constructor
		SPRSeqTree(TREE* t, NODE* subroot):reference_tree(t),spr(t,subroot) { };
		
		//inline void setNNIPosition(int p) {tree_pos = p+1;}
		//inline void setTreePosition(int p) {tree_pos = p;}
		//inline void setPositionToReferenceTree() {tree_pos=0;}
		inline bool isReferenceTree() {return spr.isReferenceTree();}

		//inline void setSource(NODE* src) {source_node=src;}
		inline void setReferenceTree(TREE* rt) {reference_tree=rt;}
		inline void setSPR(SPR s) {spr=s;}
		//inline int getTreePosition() {return tree_pos;}
		//inline int getNNIPosition() 
		//	{
		//	if (isReferenceTree()) EXCEPTION("Tried to fetch NNI position of reference tree");
		//	return tree_pos-1;
		//	}
		//inline NODE*& getSource() {return source_node;} // return a reference: this return type is 
		//			// required for other funcs like getNodeName()...careful
		SPR getSPR() { return spr; }
		//TreeScore getTreeScore()
		//	{
		//	return TreeScore(gtp_score,mp_score,rooting_score,total_score);
		//	}
	};
//***************************************************************

// A compact version of a tree defined relative to a reference tree via an SPR move. The reference tree and nodes are stored here.
// New stab at this kind of class, using legacy code philosophy of templates...
template<class>
class PersistentTree;

template<class TREE>
class SPRTree : public TreeScore 
	{ 
	typedef typename TREE::Node NODE; //... typename tells us that TREE::Node is a type, and thus NODE will be a new type name, voila
	public:
		TREE *tree;
		NODE *source,*dest; // source = crown node of subtree that will move to become sister to dest node
		//int counter=0;

	public:
		SPRTree(TREE* t = nullptr, NODE* s=nullptr, NODE* d=nullptr):tree{t}, source{s},dest{d} {}; // this defaults to "no move" 

	inline void rearrange() 
		{
		if (source != nullptr) 	// meaning,  not "no move"
			tree->moveSubtree(source,dest);
		}

	SPRTree reversed() // MUST SAVE THIS *BEFORE* ANY REARRANGEMENT OF THE TREE...
		{
		if (source==nullptr) EXCEPTION("reversed() function attempted on null pointer");
		return SPRTree(tree,source,source->getSibling()); // turns out to be correct source and dest
		}

	PersistentTree<TREE> toPersistentTree() // make this into a persistent tree but be sure to move back to ref tree on exiting
		{
		if (source != nullptr) // be careful about "reference tree" with null source
			{
			SPRTree rev = this->reversed();
			this->rearrange();
			PersistentTree<TREE> pt(this->tree);
			rev.rearrange();
			pt.setScores(this->getGTPScore(),this->getMPScore(),this->getRootingScore(),this->getGTPWeight(),this->getMPWeight());
			return pt;	
			}
		else
			{
			PersistentTree<TREE> pt(this->tree);
			pt.setScores(this->getGTPScore(),this->getMPScore(),this->getRootingScore(),this->getGTPWeight(),this->getMPWeight());
			return pt;	
			}
		}	


	// NB! The following comparator is template-specialized ONLY TO SPECIES TREES AND USES ONLY THE GTP SCORE
	// It would be more transparent to have an explicit comparator here.
#if 1
	friend bool operator<(const SPRTree<SpeciesTree>& t1, const SPRTree<SpeciesTree>& t2)  // NB. Notice this requires friend qualifier
		{
		//if (t1.gtp_score > t2.gtp_score) 
		//return (t1.getGTPScore() > t2.getGTPScore()) ; ... this works instead also!
		if (t1.getGTPScore() > t2.getGTPScore()) 
			return true;
		else
			return false;
		}
	};
#else // below attempts to implement stable sort, but didn't seem to matter for bug
	friend bool operator<(const SPRTree<SpeciesTree>& t1, const SPRTree<SpeciesTree>& t2)  // NB. Notice this requires friend qualifier
		{
		if (t1.getGTPScore() != t2.getGTPScore())
			return (t1.getGTPScore() > t2.getGTPScore());
		else
			return (t1.counter > t2.counter);
		}
	};

#endif

#if 0
template<class TREE>
class SPRTreeComparator
	{
	public:
	bool operator()(SPRTree<TREE>& t1, SPRTree<TREE>& t2)  // NB. Notice this requires friend qualifier
		{
		//if (t1.gtp_score > t2.gtp_score) 
		if (t1.getGTPScore() > t2.getGTPScore()) 
			return true;
		else
			return false;
		}
	};
#endif

// Used to save tree as a string so we can get rid of or change underlying volatile tree structure
// **Note newick strings are not saved with semicolons at end.**

// **Note there are explicitly specialized constructors for SpeciesTrees later in file to allow storing them in special sorted
// fashion, which takes time. Didn't want to do this in general for the gene trees, since there may be many orders of magnitude
// more gene trees than species trees.
template<class TREE>
class PersistentTree: public TreeScore
	{
	private:
		string newick_string="";
		bool present = false;
	public:
		PersistentTree() {};
		PersistentTree(string  newick): newick_string{newick} { present = true; };
		PersistentTree(TREE* t) 
			{ 
			newick_string = t->tree2newick(); 
			present=true;
			}
		inline string newick() { return newick_string; };

		inline bool exists() { return present; }

// FOLLOWING ONLY WORKS CORRECTLY FOR SPECIES TREES!!
// SHOULD TRY TO TEMPLATE SPECIALIZE THIS TOO.
		friend bool operator==(const PersistentTree &c1, const PersistentTree &c2)
			{
			return c1.newick_string == c2.newick_string;
			}

	};



// NOTE THIS EXPLICIT TEMPLATE SPECIALIZATION ON SPECIES TREES TO STORE NEWICK STRINGS IN SPECIAL SORTED FASHION FOR LATER EQUALITY TEST

template <>
//PersistentTree<SpeciesTree>::PersistentTree(string  newick): newick_string{sortedNewick<TREE>(newick)} { present = true; }; 
PersistentTree<SpeciesTree>::PersistentTree(string  newick) 
	{
	//cout << "BEFORE " << newick << endl;
	newick_string=sortedNewick<SpeciesTree>(newick); 
	//cout << "AFTER " << newick_string << endl;
	present = true; 
	}; 

template <>
PersistentTree<SpeciesTree>::PersistentTree(SpeciesTree* t) 
	{ 
	newick_string = t->tree2newickSorted();  
	present=true;
	//cout << "** " << newick_string << endl;
	}


#endif

