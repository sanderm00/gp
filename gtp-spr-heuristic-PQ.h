/*
 * Copyright (C) 2007 Andre Wehe, Mukul Bansal, Oliver Eulenstein
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

// MJS Modified below in various ways to do just a single round of species tree SPR search and put all resulting species
// trees in a priority queue based on duplication score. 
// NB. The priority queue HERE stores SPRTree objects, which have an overloaded '<' operator based on the GTP score only.
// This contrasts with the TSASDataPersistent objects used elsewhere in priority queues, which overload based on total score.

// Currently rerooting of gene trees is hard-coded OFF for this algorithm.

#ifndef HEURISTICPQ
#define HEURISTICPQ
#include "savedtrees.h"
#include <algorithm>



class HeuristicPQ :  public Heuristic {

protected:
//	int counter=0;
	SPRTree<SpeciesTree> sprmv;
        double Best_score;
	bool update;

	std::priority_queue<SPRTree<SpeciesTree>> pqueue;
	std::vector<unsigned int>gtp_score_v;

	int j, index;
	Format format;
	unsigned int countTotal;

public:
// Create brand new TSAS object from a persistent version
void readTSASTrees(TSASDataPersistent &dp)
	{
	// instantiates EMPTY superclass TreeSet before the following

	readSpeciesTree(dp.sptree.newick());

//	SpeciesTree* tree  = new SpeciesTree;
//	tree -> newick2tree( dp.sptree.newick() );
//	speciestree = tree;

	for (int i=0;i<dp.ugtrees.size();i++)
		{
		GeneTreeUnrooted* tree  = new GeneTreeUnrooted;
		tree -> newick2tree( dp.ugtrees[i].newick() );
		tree -> weight = 1.0; // apparently needed by GTP parts of legacy code.
		genetree_unrooted.push_back(tree);
		}								// removing to save RAM later
	}

        inline void scoreComputed(SpeciesNode &node)
	{
                const double &genedup = node.score;
		//MJS added for priority queue; this will use the SPRTree '<' operator that is based only on GTP score
		sprmv.source = speciestree->nodes[j];
                sprmv.dest = &node;
		sprmv.setScores((unsigned int)genedup,0,0,1.0,1.0); //correct initializers for search that only uses GTP scores
                //sprmv.setGTPScore(genedup);
		sprmv.tree = speciestree;
//	sprmv.counter = counter++; //trying stable sort code
		pqueue.push(sprmv);
		gtp_score_v.push_back((unsigned int)genedup);
        }

	void dumpScores()
		{
		for (auto gtp:gtp_score_v)
			msgout << gtp << " ";
		msgout << endl;
		}

        //HeuristicPQ(std::priority_queue<SPRTree<SpeciesTree>> &pq) : pqueue{pq}

// NB. I could still just make this whole class a subclass of TSAS; tried it once and a few complications, but ultimately doable.
// That would make the constructor already done basically...
        HeuristicPQ(TSASDataPersistent &dp) 
        {
		readTSASTrees(dp);
		// Always set up a "reference tree" and put it in the pq. This is the species tree from the dp object and its treescore!
		SPRTree<SpeciesTree> refTree; 
		refTree.tree = speciestree; // source fields will default to nullptr, iterpreted as "no move"
		refTree.set(dp.getTreeScore()); // notice, the ref tree has a complete TreeScore in the SPRTree object
		pqueue.push(refTree); 

		index=0;
		//srand(time(0));
		Best_score = UINT_MAX;
		update = true;
		countTotal = 0;
	}
	inline unsigned int pq_size() {return pqueue.size();}


	// CALLED WITH 'WITH_TIES' option (currently default)
	// Return a sample from the PQ consisting of just the best *UNIQUE* 'first_k' trees, where uniqueness is based
	// on topology of trees.
	// Of those kept, some might have tied scores, hence the flag WITH_TIES is what calls this
	// The trees are removed from the PQ in the process.
	std::vector< PersistentTree<SpeciesTree> > samplePQ(int first_k)
		{
		std::vector< PersistentTree<SpeciesTree> > sptrees;
		while (!pqueue.empty() && first_k > 0)
			{
			SPRTree<SpeciesTree> sprt = pqueue.top();
			PersistentTree<SpeciesTree> pt=sprt.toPersistentTree();
			// Check for uniqueness (many won't be) using class PersistentTree == operator implicitly
			// For class SpeciesTree this operator relies on the sortedNewick() functionality found in 
			// that kind of PersistentTree
			if ( std::find(sptrees.begin(),sptrees.end(),pt) == sptrees.end() ) 
				{
				sptrees.push_back(pt); // in other words, if its not present in array already, push
				--first_k;
				}
			pqueue.pop();
			//msgout << "Persistent SPR tree = " << pt.newick() << ":" << pt.getGTPScore() << endl;
			}
		return sptrees;
		}


	// CALLED WITH 'WITHOUT_TIES' option
	// Return the top trees from the PQ having distinct scores; i.e., no ties are present.
	// We can assume these trees will be distinct, so this saves time by not checking topologies,
	// but this will overlook some distinct trees that have tied scores (see samplePQ() just above).
	std::vector< PersistentTree<SpeciesTree> > samplePQByScores(int first_k_distinct)
		{
		std::vector< PersistentTree<SpeciesTree> > sptrees;
		bool first = true;	
		unsigned int best_gtp_score ;
		while (!pqueue.empty() && first_k_distinct > 0)
			{
			SPRTree<SpeciesTree> sprt = pqueue.top();
			if (first) 
				{
				best_gtp_score = sprt.getGTPScore();
				PersistentTree<SpeciesTree> pt=sprt.toPersistentTree();
				sptrees.push_back(pt);
				pqueue.pop();
				first = false;
				--first_k_distinct;
				}	
			else
				{
				if (sprt.getGTPScore() > best_gtp_score)
					{
					best_gtp_score = sprt.getGTPScore();
					PersistentTree<SpeciesTree> pt=sprt.toPersistentTree();
					sptrees.push_back(pt);
					--first_k_distinct;
					}
				pqueue.pop();
				}
			//msgout << "Persistent SPR tree = " << pt.newick() << ":" << pt.getGTPScore() << endl;
			}
		return sptrees;
		}

	void run(ostream &os, const ReRoot reroot) {}
	
	// Do rSPR search on species tree but stop after scanning the neighborhood. 
	// Do not continue to try to improve after first round

	void run()
	{
		//bool rerooting = (reroot == ALL); // 'rerooting' means whether gene trees will be rerooted at every step of iteration below...
		bool rerooting = false ; //  override duptree legacy rooting setup
                createLeafMapping();

                int num_nodes, Left_Right;
		int num_spr_searches=0;
                num_nodes = speciestree->nodes.size();
                for ( j=0; j< num_nodes;j++) // notice j is a class var
                        {
                                SpeciesNode *prnt, *sblng;

                                if (speciestree->nodes[j] == speciestree->root)
                                        continue;

                                prnt = speciestree->nodes[j]->parent();
                                if (prnt->child(0) == speciestree->nodes[j])
                                        Left_Right = 0;
                                else Left_Right =1;
                                sblng = prnt->child(1-Left_Right);
                                speciestree->moveSubtree(speciestree->nodes[j], speciestree->root);

                                computeGeneDuplications(speciestree->nodes[j], rerooting);	
					//MJS: the score for this subtree when it's made sib to root
					// rerooting controls whether gene trees will be rerooted at every iteration...
//if ( speciestree->root->child(Left_Right) != speciestree->nodes[j]) EXCEPTION ("BOOBOO");
                                speciestree->moveSubtree(speciestree->root->child(Left_Right), sblng); //MJS: Move back to original location
// Note, this seems unnecessary: can't we just move node[j] to sbling? moveSubtree should take care of left right stuff...and indeed, previous line seems to validate this...
                        }

	} // end runOneRound


	double getCurrentScore() {
		return Best_score;
	}
};



#endif
