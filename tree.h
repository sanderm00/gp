/*
 * Copyright (C) 2007 Andre Wehe, Mukul Bansal, Oliver Eulenstein
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef TREE_H
#define TREE_H

#if 0 // toying with nested class for moves.
class TTree
{
public:
TTree() {};
//virtual ~TTree() {};
};

class myNNI 
	{
	TTree* tree;
	TNode* source_node;
	bool isForwardDirection = true;
	bool hasBeenRearranged = false;
	public:
	myNNI() {};
	myNNI(TTree *t,TNode* sn=nullptr):tree{t},source_node{sn} {  msgout << "Testing myNNI" << endl; }

	myNNI reversed() // make a new nni that is the reverse of an existing one
		{
		auto nni_rev = *this;
		nni_rev.source_node = this->source_node->getAunt();
		nni_rev.isForwardDirection = !(this->isForwardDirection);
		return nni_rev;
		}
////// FIX null ptr for source node condition.....
	inline void rearrange() // rearrange tree using source_node, but only if currently unrearranged
		{
		if (hasBeenRearranged==false) 
			{
			tree->moveSubtree(source_node,source_node->grandparent());
			hasBeenRearranged=true;
			}
		}

	};

#endif



template<class NODE, class LEAFNODE>
class Tree {
//class Tree: public TTree {



// Consider having a new type here, struct or class, for nni and spr moves.

public:
	typedef NODE Node;
	typedef LEAFNODE LeafNode;

	NODE *root;
	vector<NODE*> nodes;	// All nodes, including
	vector<LEAFNODE*> leafnodes; // just the leaf nodes

	Tree() {
		root = NULL;
	}

	Tree(const Tree &r) {
		// copying gives you an empty tree rigth now!!!
		if (r.root != NULL) cerr << "WARNING: Tree is not going to be copied!" << endl;
	}

	virtual ~Tree() {
		#ifdef DEBUG
		checkStructure();
		#endif
		for (typename vector<NODE*>::iterator itr = nodes.begin(); itr != nodes.end(); itr++) delete *itr;
		nodes.clear();
	}

	//MJS:


#if 0
	uint16_t setupNLeaves()
		{
		return setupNLeavesHelper(this->root);
		}
	uint16_t setupNLeavesHelper(NODE* node)
		{
		uint16_t num;
		if (node->isLeaf()) 
			num = 1;
		else
			num = setupNLeavesHelper(node->child(0)) + setupNLeavesHelper(node->child(1));
		node->n_leaves = num;
		return num;
		}
#endif

	void setupNodeVectors()
		{
		if (nodes.empty() == false || leafnodes.empty() == false) EXCEPTION("Node vectors are NOT empty in setupNodeVectors");
		pushNodes(root);
		}
	inline void pushNodes(NODE* n)
		{
		nodes.push_back(n);
		if (n->isLeaf()) 
			{
			leafnodes.push_back((LEAFNODE*)n);
			return;
			}
		NODE *children[2];
		n->getChildren(children);
		for (int i = 0; i < 2; i++) 
			pushNodes(children[i]);
		return;
		}
//	NNI move

/*MJS
                /
               /
              *  [bp]
             / \
         e  /   \
           /     \
          /       \
         * [ap]    *  [b]
        / \                      SWAP location of subtrees at nodes a and b around edge e 
       /   \        		 The other NNI move is obtained by calling this function with node's sibling
      /     *   
     / 
    * [a] 'node' 

*/

	// performs a NNI operation on the edge above 'node' and it's parent's sibling (i.e., its aunt).
	// MJS: Important: Preserves the left right ordering of nodes during swap.
	inline void oprNNI(NODE *node) {
		NODE *a = node;
		NODE *ap = a->parent();
		int ai = ap->child(0) == a ? 0 : 1; // MJS: which child of ap is a?
		NODE *b = a->parent()->getSibling();
		NODE *bp = b->parent();
		int bi = bp->child(0) == b ? 0 : 1; // Which child of bp is b?
		a->parent() = bp;
		bp->child(bi) = a;
		b->parent() = ap;
		ap->child(ai) = b;
	}
#if 0
	NODE* nniRearrange(NODE* source) //nni rotate to exchange source node with its aunt; returns aunt, which can be used to undo this move
			{
			NODE* aunt = source->getAunt(); //do this before following operation
			oprNNI(source);	// tree now is in rearranged state relative to original tree
			return aunt;
			}
#endif

//                        rSPR move
/*MJS
                 /                                                          /
                /                                                          /
               *                                                          *
              / \                                                        / \
             /   \                                                      /   \
            /     \                                                    /     \
           /       \                                                  /       \
          /         \                                                /         *
         *           \                                              /         / \
        / \           \                                            /         /   \
       /   * b         *  [c] 'targetnode'              --->      * b     a *     * c  'targetnode'
      /                                                                    /_\
     / 
    * [a] 'subroot' 
   /_\


*/	

	// moves a subtree to a new location in the tree
	// subroot = root node of the tree to be pruned
	// targetnode and subroot are going to be sibling in the new tree
	// mjs: parent of subroot is temporarily disconnected and then reused in its new
	// location.
	inline void moveSubtree(NODE *subroot, NODE *targetnode) {
		// prune out subtree
		#ifdef DEBUG
		if (subroot == NULL) EXCEPTION("moveSubtree: subtree root node is NULL");
		if (subroot->parent() == NULL) EXCEPTION("moveSubtree: no parent node");
		for (NODE *n = targetnode; n != NULL; n = n->parent()) {
			if (n == subroot) EXCEPTION("moveSubtree: cannot move into subtree");
		}
		#endif
		NODE *p = subroot->parent();
		if (targetnode == p) return;
		NODE *pp = p->parent();
		const int pi = p->child(0) != subroot ? 0 : 1;
		NODE *pc = p->child(pi);
		pc->parent() = pp;
		if (pp != NULL) {
			const int ppi = pp->child(0) == p ? 0 : 1;
			pp->child(ppi) = pc;
		} else {
			root = p->child(pi);
			p->child(pi) = NULL;
		}

		// insert subtree
		#ifdef DEBUG
		if (targetnode == NULL) EXCEPTION("moveSubtree: target node is NULL");
		if (targetnode == subroot) EXCEPTION("moveSubtree: invalid target node");
		#endif
		NODE *q = targetnode->parent();
		if (q != NULL) {
			const int qi = q->child(0) == targetnode ? 0 : 1;
			q->child(qi) = p;
		} else {
			root = p;
			p->parent() = NULL;
		}
		p->parent() = q;
		targetnode->parent() = p;
		p->child(pi) = targetnode;
	}

	// delete a leaf node
	void deleteLeafNode(LEAFNODE *node) {
		// prune out subtree
		#ifdef DEBUG
		if (node == NULL) EXCEPTION("removeLeafNode: subtree root node is NULL");
		if (node->parent() == NULL) EXCEPTION("removeLeafNode: no parent node");
		#endif
		NODE *p = node->parent();
		NODE *pp = p->parent();
		const int pi = p->child(0) != node ? 0 : 1;
		NODE *pc = p->child(pi);
		pc->parent() = pp;
		if (pp != NULL) {
			const int ppi = pp->child(0) == p ? 0 : 1;
			pp->child(ppi) = pc;
		} else {
			root = p->child(pi);
			p->child(pi) = NULL;
		}

		// delete leaf node and it's parent node
		// from leaf set
		for (typename vector<LEAFNODE*>::iterator itr = leafnodes.begin(); itr != leafnodes.end(); itr++) {
			if (*itr == node) {
				leafnodes.erase(itr);
				break;
			}
		}
		// from node set
		for (typename vector<NODE*>::iterator itr = nodes.begin(); itr != nodes.end(); itr++) {
			if (*itr == node) {
				nodes.erase(itr);
				break;
			}
		}
		for (typename vector<NODE*>::iterator itr = nodes.begin(); itr != nodes.end(); itr++) {
			if (*itr == p) {
				nodes.erase(itr);
				break;
			}
		}
		// free the memory
		delete p;
		delete node;
		#ifdef DEBUG
		checkStructure();
		#endif
	}

	// take out root node but  keep root node lying around
	inline void disconnectRoot() {
		NODE *u = root->child(0);
		NODE *v = root->child(1);
		u->parent() = v; //MJS: these used to point to the root, now the must point to each other, for want of anything else to do
		v->parent() = u;
	}

	// insert root node
	inline void connectRoot(NODE *u, NODE *v) {
		root->child(0) = u;
		root->child(1) = v;
		u->getDirection(v) = root; //MJS: this edge used to point from u->v; now force it to point to root
		v->getDirection(u) = root; //MJS: Can't just undo the ->parent() syntax from disconnectRoot, because we can't be sure u and v
						// point to  each other like they do after disconnectRoot is called
			// MJS: Note this correctly updates the relative[] array but apparently not the parent links
			// so this seems to require running 'reroot()' in gtp-spr-treeset for an unrooted tree to leave the tree in its
			// proper usable state...
	}

	#ifdef DEBUG
	void checkStructure() {
		// DFS starting from root node
		int size = 0;
		checkStructureDFS(root, NULL, size);
		// check amount of registered nodes
		if (size != nodes.size())
			EXCEPTION("tree structure broken - " << size <<" nodes in the tree, but " << nodes.size() << " registered");
		// are all nodes of known datatype
		for (typename vector<NODE*>::iterator itr = nodes.begin(); itr != nodes.end(); itr++)
			EXCEPTIONDATATYPE2("checkStructure", **itr, NODE, LEAFNODE);
		// are all leaf nodes of datatype LEAFNODE
		for (typename vector<LEAFNODE*>::iterator itr = leafnodes.begin(); itr != leafnodes.end(); itr++)
			EXCEPTIONDATATYPE("checkStructure", **itr, LEAFNODE);
	}
	void checkStructureDFS(NODE *&node, NODE *parent, int &size) {
		if (node == NULL) return;
		size++;
		if (node->parent() != parent) EXCEPTION("tree structure broken - node " << node << " has wrong parent");
		NODE *children[2];
		node->getChildren(children);
		for (int i = 0; i < 2; i++) {
			checkStructureDFS(children[i], node, size);
		}
	}
	#endif

	// outputs a subtree into a string stream
	static void outputSubtree(ostream &os, NODE *&node, int lvl) {
		// indent according to the depth of the tree
		for (int i=0; i<lvl; i++) os << "  ";
		// output the node
		EXCEPTIONDATATYPE2("outputSubtree", *node, LEAFNODE, NODE);
		if (typeid(*node)==typeid(LEAFNODE)) os << *((LEAFNODE *)node);
		else if (typeid(*node)==typeid(NODE)) os << *((NODE *)node);
		else EXCEPTION("treenode of unknown type");

		if ((node->child(0)!=NULL) || (node->child(1)!=NULL)) os << " (";
		os << endl;
		// proceed with the children nodes
		if (node->child(0)!=NULL) outputSubtree(os, node->child(0), lvl+1);
		if (node->child(1)!=NULL) outputSubtree(os, node->child(1), lvl+1);
		// indent according to the depth of the tree
		if ((node->child(0)!=NULL) || (node->child(1)!=NULL)) {
			for (int i=0; i<lvl; i++) os << "  ";
			os << ')' << endl;
		}
	}

	// outputs the tree into a string stream
	template<class N1, class N2> friend ostream & operator << (ostream &os, Tree<N1,N2> &t);
};

// outputs the tree into a string stream
template<class NODE, class LEAFNODE>
ostream & operator << (ostream &os, Tree<NODE, LEAFNODE> &t) {
	t.outputSubtree(os, t.root, 0);
	//t.tree2newick(os);
	return os;
}

// ------------------------------------------------------------------------------------------------------------------
// extends a binary tree by input/output functions
template<class TREE>
class TreeIO : public TREE {  //MJS: i.e., this template parameter tells us what class TreeIO should inherit FROM...
public:
	using TREE::root;
	using TREE::nodes;
	using TREE::leafnodes;

	typedef typename TREE::Node NODE;
	typedef typename TREE::LeafNode NAMEDNODE;

	string setupLowestLabel()
		{
		return setupLowestLabelHelper(this->root);
		}
	string setupLowestLabelHelper(NODE* node)
		{
		string lowest;
		if (node->isLeaf()) 
			lowest = ((NAMEDNODE*)node)->getName();  // have to case this, but note that -> has higher precedence than (cast)
		else
			{
			string s0 = setupLowestLabelHelper(node->child(0)); 
			string s1 = setupLowestLabelHelper(node->child(1)); 
			lowest = s0<s1 ? s0 : s1;
//cout << "s0="<<s0<<" s1="<<s1<<" lowest="<<lowest <<endl;
			}
		node->lowest_label = lowest;
		return lowest;
		}
	// MJS added: (to make it easier to instantiate tree object from strings rather than streams)
	// Note that the newick string can optionally be terminated by ';'
	public: void newick2tree(string TD)
		{
		istringstream istreamTD;
		if (TD.back() == ';')
			istreamTD.str(TD);
		else
			istreamTD.str(TD+';');
		Input inTD(&istreamTD);
		this->stream2tree(inTD);
		}


	// read the tree from a string stream (newick formatted e.g. ((name1,name2),name3);)
	public: bool stream2tree(Input &is) {
		char c;
		input = &is;
		if (!is.nextAnyChar(c)) return false;
		if (c != '(') EXCEPTION("problem in the tree expression " << input->getPos());
		stream2tree(c, is);
		return true;
	}

	// read the tree from a string stream; given the first character of the stream
	public: void stream2tree(char &c, Input &is) {
		int lvl = 1;
		input = &is;

		// start reading the tree ... begin with root node
		commentcallback.comments.clear();
		input->setCommentCallback(&commentcallback);
		stream2nextNode(c, -1);
		input->setCommentCallback(NULL);

		// ';' terminator found?
		if (c != ';') EXCEPTION("missing ';' in tree expression " << input->getPos());
		buildTree();
		nodeorder.clear();
	}

	// called when a comment was explored
	class CommentCallBack : public InputCommentCallbackBase {
		void commentFound(string &str) {
			comments.push_back(str);
		}
		public: vector<string> comments;
	} commentcallback;

	// outputs the tree in newick format
	public: void tree2newick(ostream &os) {
		subtree2newickDFS(os, root);
		os << ';';
	}
	// MJS: outputs the tree as stream in newick format using whole names
	public: void tree2newickWholeNames(ostream &os) {
		subtree2newickDFSWholeNames(os, root);
		os << ';';
	}


	// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	// MJS: outputs the tree in newick format using whole names as a string
	// This is how I usually use this and/or as part of the << inserter below
	public: string tree2newick() {
		stringstream ss;
		subtree2newickDFSWholeNames(ss, root);
		return ss.str();
	}
	// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


	// MJS: outputs the tree in newick format using just the species names
	public: string tree2newickSpNames() {
		stringstream ss;
		subtree2newickDFS(ss, root);
		return ss.str();
	}
protected: void subtree2newickDFS(ostream &os, NODE *&node) {  // original code
		if (node == NULL) return;

		// if leaf node then output the node
		if (!node->isLeaf()) {
			EXCEPTIONDATATYPE2("tree2newickDFS", *node, NAMEDNODE, NODE);
			if ((node->child(0)!=NULL) || (node->child(1)!=NULL)) os << '(';
			// proceed with the children nodes
			subtree2newickDFS(os, node->child(0));
			if (node->child(1)!=NULL) os << ',';
			subtree2newickDFS(os, node->child(1));
			// close the subtree
			if ((node->child(0)!=NULL) || (node->child(1)!=NULL)) os << ')';
		}

		// output node name and properties
		if (typeid(*node)==typeid(NAMEDNODE)) {
			namednode2newick(os, *((NAMEDNODE *)node));
		} else {
			node2newick(os, *node);
		}
	}
	//MJS: my version using whole names
	protected: void subtree2newickDFSWholeNames(ostream &os, NODE *&node) {  
		if (node == NULL) return;

		// if leaf node then output the node
		if (!node->isLeaf()) {
			EXCEPTIONDATATYPE2("tree2newickDFS", *node, NAMEDNODE, NODE);
			if ((node->child(0)!=NULL) || (node->child(1)!=NULL)) os << '(';
			// proceed with the children nodes
			subtree2newickDFSWholeNames(os, node->child(0));
			if (node->child(1)!=NULL) os << ',';
			subtree2newickDFSWholeNames(os, node->child(1));
			// close the subtree
			if ((node->child(0)!=NULL) || (node->child(1)!=NULL)) os << ')';
		}

		// output node name and properties
		if (typeid(*node)==typeid(NAMEDNODE)) {
			namednode2newickWholeNames(os, *((NAMEDNODE *)node));
		} else {
			node2newick(os, *node);
		}
	}

	//MJS: Version of previous when we want to sort clades by size in newick string
	public: string tree2newickSorted() {
		stringstream ss;
		this->setupLowestLabel();
		subtree2newickDFSWholeNamesSorted(ss, root);
		return ss.str();
		}
	protected: void subtree2newickDFSWholeNamesSorted(ostream &os, NODE *&node) {  
		if (node == NULL) return;

		// if leaf node then output the node
		if (!node->isLeaf()) {
			EXCEPTIONDATATYPE2("tree2newickDFS", *node, NAMEDNODE, NODE);
			if ((node->child(0)!=NULL) || (node->child(1)!=NULL)) os << '(';

			// proceed with the children nodes

//cout << "==" << node->child(0)->lowest_label << " :: " << node->child(1)->lowest_label << endl; 

			if (node->child(0)->lowest_label < node->child(1)->lowest_label) // sort by descendant leaf set size
				{
				subtree2newickDFSWholeNamesSorted(os, node->child(0));
				if (node->child(1)!=NULL) os << ',';
				subtree2newickDFSWholeNamesSorted(os, node->child(1));
				}
			else
				{
				subtree2newickDFSWholeNamesSorted(os, node->child(1));
				if (node->child(0)!=NULL) os << ',';
				subtree2newickDFSWholeNamesSorted(os, node->child(0));
				}

			// close the subtree
			if ((node->child(0)!=NULL) || (node->child(1)!=NULL)) os << ')';
		}

		// output node name and properties
		if (typeid(*node)==typeid(NAMEDNODE)) {
			namednode2newickWholeNames(os, *((NAMEDNODE *)node));
		} else {
			node2newick(os, *node);
		}
	}
// MJS: added these utility functions to grapple with types of nodes, and this program's assumptions about unnamed nodes, etc.
// MJS: added these utility functions to grapple with types of nodes, and this program's assumptions about unnamed nodes, etc.
// Part of issue is that node types seem to be defined in tree.h depending on trees, etc.
// If a named node, return name or w_name, else return empty string; 
	public: string getNodeName(NODE *&node)
		{
		if (typeid(*node)==typeid(NAMEDNODE)) 
			{
			NAMEDNODE* nn;
			nn = (NAMEDNODE*)node;
			return nn->getName();
			}
		else
			return "";	
		}
	public: string getNodeWName(NODE *&node) // get the w_name instead...
		{
		if (typeid(*node)==typeid(NAMEDNODE)) 
			{
			NAMEDNODE* nn;
			nn = (NAMEDNODE*)node;
			return nn->getWholeName();
			}
		else
			return "";	
		}

	// output a named tree node in newick format
	public: virtual void namednode2newick(ostream &os, NAMEDNODE &node) {
		os << name2newick(node.getName()); 
	}
	// output a named tree node in newick format
	public: virtual void namednode2newickWholeNames(ostream &os, NAMEDNODE &node) {
		os << name2newick(node.getWholeName()); 
	}

	// output a unnamed tree node in newick format
	public: virtual void node2newick(ostream &os, NODE &node) {
	}

	protected: Input *input;

	// temporary all-purpose node
	protected: class TempNode {
	public:
		TempNode() {
			parent = -1;
			name = "";
			weight = 0;
			node = NULL;
		}
		TempNode(int parent) : parent(parent) {
			name = "";
			weight = 0;
			node = NULL;
		}
		TempNode(int parent, string name) : parent(parent), name(name) {
			weight = 0;
			node = NULL;
		}
		TempNode(const TempNode& p) {
			parent = p.parent;
			name = p.name;
			node = p.node;
			weight = p.weight;
			comments = p.comments;
		}

		int parent;
		string name;
		double weight;
		vector<string> comments;
		NODE* node;
	};
	vector<TempNode> nodeorder; //MJS: a vector of all the nodes in the tree as found while parsing

	// build the tree from all-purpose nodes
	// MJS: i.e., use nodeorder[] and put the nodes in a more useful data structure...
	protected: void buildTree() {
		// resolve multifurcations by making them arbitrarily binary
		bool warn_root_furcation=false;
		bool warn_nonroot_furcation=false;
		bool warn_many_furcation=false;
		vector<vector<int> > children(nodeorder.size()); //MJS: initialize nodeorder.size() vectors of children, one vector for each tempnode in nodeorder...?
		for (int i=0; i<nodeorder.size(); i++) {
			TempNode *n = &nodeorder[i];
			if (n->parent != -1) children[n->parent].push_back(i);
		} // ok, that setup is done, now deal with any multifurcs
		for (int i=0; i<children.size(); i++) { //children.size() is the number of nodes
			// MJS added warnings about multifurcations...
			if (children[i].size() > 2)
				{
				if (children[i].size() == 3)
					{
					TempNode *n = &nodeorder[i];
					if (n->parent == -1) 
						warn_root_furcation=true;
					else	
						warn_nonroot_furcation=true;
					}
				else
					warn_many_furcation=true;
				}
			while (children[i].size() > 2) { // but children[i] is the vector of children of those nodes...
				// create a common parent
				int newid = nodeorder.size();
				nodeorder.push_back(TempNode(i));

				// pick 1st node randomly
				int l = (int) (children[i].size() * (rand() / (RAND_MAX + 1.0)));
				nodeorder[children[i][l]].parent = newid;
				children[i].erase(children[i].begin()+l);

				// pick 2nd node randomly
				int r = (int) (children[i].size() * (rand() / (RAND_MAX + 1.0)));
				nodeorder[children[i][r]].parent = newid;
				children[i].erase(children[i].begin()+r);

				// add new node as child
				children[i].push_back(newid);
			}
	}

		// create all nodes
		for (int i=0, last=nodeorder.size(); i<last; i++) {
			TempNode &n = nodeorder[i];
			createNode(n);
			nodes.push_back(n.node);
		}

		// connect all nodes
		for (int i=0; i<nodeorder.size(); i++) {
			TempNode &n = nodeorder.at(i);
			if (n.parent == -1) {
				root = n.node;
				n.node->parent() = NULL;
			} else {
				TempNode &p = nodeorder.at(n.parent);
				n.node->parent() = p.node;
				if (p.node->child(0) == NULL) {
					p.node->child(0) = n.node;
				} else
				if (p.node->child(1) == NULL) {
					p.node->child(1) = n.node;
				} else {
					cout<<"  node:"<<i<<" parent:"<<n.parent<<endl;
					EXCEPTION("buildTree: cannot connect more than 2 children to one parent");
				}
			}
		}

		// build leaf set
		for (int i=0; i<nodeorder.size(); i++) {
			TempNode &n = nodeorder.at(i);
			if (n.node->isLeaf()) leafnodes.push_back((NAMEDNODE*)n.node);
		}
		if (warn_root_furcation)
							{WARNING("Randomly resolving root trifurcation (tree likely is UNROOTED)");} // wrap in brackets because its a #define
		if (warn_nonroot_furcation)
							{WARNING("Randomly resolving non-root trifurcation");} // wrap in brackets because its a #define
		if (warn_many_furcation)
							{WARNING("Randomly resolving >3-furcation");} // wrap in brackets because its a #define
	}

	// creates a tree node from a temporary node
	public: virtual void createNode(TempNode &n) {
		if (n.name.empty()) {
			n.node = new NODE(NULL);
		} else {
			n.node = new NAMEDNODE(n.name, NULL);
		}
	}

	// process an internal or leaf node
	protected: void stream2nextNode(char &c, const int &parentid) {
		// create a temporary node
		const int nodeid = nodeorder.size();
		nodeorder.push_back(TempNode(parentid));
		#define tempnode nodeorder[nodeid]

		// add comments prior to the node expression
		tempnode.comments = commentcallback.comments;
		commentcallback.comments.clear();

		if (c == '(') { // internal node
			int childCount = 0;
			do {
				// check for multifurcations
				childCount++;
				//if (childCount > 2) WARNING("multifurcation (children=" << childCount<<") detected at " << input->getLastPos())

				// node/subtree
				if (!input->nextAnyChar(c)) EXCEPTION("unexpected end in the tree expression " << input->getPos());
				stream2nextNode(c, nodeid);
			} while (c == ',');
			if (c != ')') EXCEPTION("missing ')' in input tree expression " << input->getLastPos());

			// read name
			if (!input->nextAnyChar(c)) EXCEPTION("unexpected end in the tree expression " << input->getPos());
			if ((c != ')') && (c != ',') && (c != ':')) {
				input->pushBack(c);
				tempnode.name = input->getName();

				if (!input->nextAnyChar(c)) EXCEPTION("unexpected end in the tree expression " << input->getPos());
			}
		} else { // leaf node
			input->pushBack(c);
			tempnode.name = input->getName();

			if (!input->nextAnyChar(c)) EXCEPTION("unexpected end in the tree expression " << input->getPos());
		}
		// read weight
		if (c == ':') {
			double weight = input->readNumber(); // read weight + get next character
			tempnode.weight = weight;

			if (!input->nextAnyChar(c)) EXCEPTION("unexpected end in the tree expression " << input->getPos());
		}
		// add comments post to the node expression
		tempnode.comments.insert(tempnode.comments.end(), commentcallback.comments.begin(), commentcallback.comments.end());
		commentcallback.comments.clear();
		#undef tempnode
	}

	void callbackComment(string &str) {
		cout << endl << str << endl;
	}
	// outputs the tree into a string stream
	template<class T> friend ostream & operator << (ostream &os, TreeIO<T> *t);

};
// MJS. Added this inserter for printing trees in newick painlessly; does not include semicolon...
template<class T>
ostream & operator << (ostream &os, TreeIO<T> *t) {
	os << t->tree2newick();
	return os ;
}
#if 1
template <class TREE>
string	sortedNewick(string newick)
	{
	TREE* tree  = new TREE;
	tree -> newick2tree( newick );
	//cout <<"IN sortedNewick BEFORE: "<< tree->tree2newick()<<endl;
	string nwk = tree->tree2newickSorted();
	//cout <<"IN sortedNewick AFTER: "<< nwk <<endl;
	delete tree;
	return nwk;
	}
#endif

#endif
