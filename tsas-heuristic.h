// Classes and routines to manage the high level search to optimize TSAS objects (i.e., species/gene trees together)
#ifndef TSASHEURISTIC
#define TSASHEURISTIC

#include <iostream>
#include <sstream>
#include <string> // needed for stoul()
#include <vector>
#include <stdio.h>
#include <string.h>
#include <fstream>
#include <queue>

struct SearchParameters
	{
	string action;
	ofstream *outscores;
	unsigned int randomseed;
	bool root_flag;
	bool queue_flag;
	unsigned int n_threads;
	unsigned int gtpmp_max_iter;
	unsigned int sp_max_iter;
	double gtp_weight;
	double mp_weight;
	unsigned int nni_radius;
	enum HSearchType hs_type; 
	unsigned int multrees;
	unsigned int h_multrees;
	istream * constraints;

	string str()
		{
		stringstream s;
		s << "Action:        " << action <<endl;
		s << "randomseed:    " << randomseed<<endl;
		s << "n_threads:     " << n_threads<<endl;
		s << "gtpmp_max_iter:" << gtpmp_max_iter<<endl;
		s << "nni_radius:    " << nni_radius<<endl;
		s << "root_flag:     " << root_flag<<endl;
		s << "sp_max_iter:   " << sp_max_iter<<endl;
		s << "multrees:      " << multrees<<endl;
		s << "h_multrees:    " << h_multrees<<endl;
		s << "gtp_weight:    " << gtp_weight<<endl;
		s << "mp_weight:     " << mp_weight<<endl;
		return s.str();
		}
	};

vector<string> tokenizer(string s,string delim); 
void searchFileParser(ifstream &file);

class TSASHeuristic
{

typedef std::priority_queue<TSASDataPersistent> PQ_TSAS; 

private:
	SearchParameters default_search_parms;
	queue <SearchParameters> search_action_queue;
	PQ_TSAS pq; // the "current" priority queue
	std::vector<double> pq_scores_previous,pq_scores_current;
	
public:
	TSASHeuristic() {}
	TSASHeuristic(SearchParameters dsp):default_search_parms{dsp} {}

	// Constructors for a single TSAS object (a pq of size 1)
	TSASHeuristic(TSASDataPersistent tsas0, SearchParameters dsp):default_search_parms{dsp} 
		{ pq.push(tsas0); }
	TSASHeuristic(TSASDataPersistent tsas0) 
		{ pq.push(tsas0); }
	TSASHeuristic(TSASDataPersistent tsas0, SearchParameters dsp, ifstream &file):default_search_parms{dsp}
		{
		pq.push(tsas0);
		if (file.is_open()) 
			searchFileParser(file);
		}
	// Constructor for an initial priority queue of arbitrary size
	TSASHeuristic(PQ_TSAS pq_in, SearchParameters tp):default_search_parms{tp},pq{pq_in} {} 

	inline PQ_TSAS getPQ() { return pq; }
	inline void push_search_action(SearchParameters parms) { search_action_queue.push(parms);}

	void execute_queue()
		{
		int elnum=0;
		while (!search_action_queue.empty())
			{
			++elnum;
			SearchParameters parms = search_action_queue.front();
			*parms.outscores << "****************************************************"<<endl;
			*parms.outscores << "          Executing algorithm element number " << elnum << "(" << parms.action<<")"<<endl;
			*parms.outscores << "          With search parameters: "<<"["<<endl;
			*parms.outscores << parms.str() ;
			*parms.outscores << "]"<<endl;
			*parms.outscores << "****************************************************"<<endl;
			if (parms.action == "simple") 
				simpleHeuristic(parms);
			else if (parms.action == "leafadd") 
				speciesTreeSearchByLeafAddition(parms);
			else if (parms.action == "report") 
				pq_report();
			search_action_queue.pop();
			*parms.outscores << "****************************************************"<<endl;
			*parms.outscores << "          END algorithm element number " << elnum << endl;
			*parms.outscores << "****************************************************"<<endl;
			}
		}

	// stuff for queue of searches...
	private:
	void storeKeyVal(string key, string val,SearchParameters &sp)
		{
		if (key == "gtpmp_max_iter") sp.gtpmp_max_iter = stoul(val); 
		else if (key == "sp_max_iter") sp.sp_max_iter = stoul(val); 
		else if (key == "gtp_weight") sp.gtp_weight = stod(val); 
		else if (key == "mp_weight") sp.mp_weight = stod(val); 
		else if (key == "nni_radius") sp.nni_radius = stoul(val);
		else if (key == "multrees") sp.multrees = stoul(val); 
		else if (key == "h_multrees") sp.h_multrees = stoul(val); 
		else if (key == "root") 
			{
			if (stoul(val)==1)
				sp.root_flag = true;
			else
				sp.root_flag = false;
			}
		}


	void searchFileParser(ifstream &file)
	{
	std::string line;
	while (std::getline(file, line)) 
		{
		//printf("%s\n", line.c_str());
		auto v = tokenizer(line," \t");
		//for (int i=0;i<v.size();i++)	
		//	cout << v.at(i) << endl;
		string algo = v.at(0);
		//cout << algo << endl;
		SearchParameters sp = default_search_parms;
		sp.action = algo;
		if (v.size()>1) // there are key:parm pair(s); fetch them 
			{
			for (int i=1;i<v.size();i++)	
				{
				string key_val = v[i];
				auto split = tokenizer(key_val,": ");
				if (split.size() == 2)
					{
					string key = split[0];
					string val = split[1];
					//cout << key << "':'" << val << endl;
					storeKeyVal(key,val,sp);
					}
				}	
			}
//msgout << sp.str() << endl;
		search_action_queue.push(sp);
		}
	file.close();
	}

	public:

	void pq_report()
		{
		if (pq.size() != 1) EXCEPTION("pq_report called with number of TSAS objects != 1");
		TSASDataPersistent dp0 = pq.top();
		dp0.finalReport();
		}

//*************************************************
//***  NOW THE ALGORITHMS PROPER ***
//*************************************************

// Mid level TSAS heuristic search functions

// At the moment these include just the 'leafadd' heuristic and the 'simple' heuristic search

//*************************************************
//
// This is the 'leafadd' Mid-level module
//
// Input is the collection of starting rooting gene trees
// Output is a TSAS object with a species tree, optimally rerooted gene trees, and all scores on all trees.
// This is added to the TSASHeuristic's PQ.
//
// Algorithm: a species tree is built from rooted gene trees by DupTree leaf addition; 
// then the gene trees are optimally rerooted rerooted relative to the sp tree using OPT option in DupTree code;
// and finally the MP scores of the gene trees and other scores are computed.

// Note: rerooting the gene trees after leaf addition is currently hard coded here and cannot be changed (easy to do though).

void speciesTreeSearchByLeafAddition(SearchParameters parms)
	{
	if (pq.size() != 1) EXCEPTION("Leaf addition heuristic called with number of TSAS objects != 1");
	TSASDataPersistent dp0 = pq.top();
	pq.pop(); // pq now empty

	ostringstream ostreamBuildtree;
	buildtree::HeuristicLeafAdd *heuristic = NULL;
	msgout << "Using randomized leaf adding heuristic [Duptree option 1] to build starting species tree..." << endl;
	msgout << "(Note, gene trees are rerooted optimally at end of leaf addition)" << endl;
	*parms.outscores << "Begin species tree heuristic via leaf addition [Duptree option 1]..." << endl;
	heuristic = new buildtree::HeuristicSecond();
	heuristic->readTrees(dp0);

	// read input constraints from a constraints file; these impose monophyly constraints on species tree
	if (parms.constraints!=NULL) 
		{
		Input constraints_input(parms.constraints);
		heuristic->readConstraints(constraints_input);
		}
	ReRoot reroot=OPT; // This option forces rerooting of gene trees at end of leaf addition, to improve scores
	heuristic->run(ostreamBuildtree, reroot);

	msgout << "Species tree estimated :" << endl;
	msgout <<ostreamBuildtree.str(); 
	PersistentTree<SpeciesTree> estimated_spt(ostreamBuildtree.str());

//....
	TSASDataPersistent soln_p(dp0.alignment_set);
	
	if (heuristic->speciestree == nullptr)
		fatal("Leaf addition heuristic FAILED to return species tree. Exiting now...");
	soln_p.sptree = PersistentTree<SpeciesTree>(heuristic->speciestree->tree2newick());
	for (int i=0;i<heuristic->genetree_rooted.size();i++)
		{
		soln_p.ugtrees.push_back(PersistentTree<GeneTreeUnrooted>(heuristic->genetree_rooted[i]->tree2newick()));
		// notice "crossing streams" from rooted to unrooted; leafadd uses former, we use latter
		//...AH THIS IS PROBLEMATIC; WE BORK ANY PREVIOUSLY PRESENT SCORES, SUCH AS MP SCORES, and ALSO WEIGHTS
		TreeScore tsc;
		soln_p.gene_tree_scores.push_back(tsc);
		}
		// notice we are NOT storing genetree treescores here and in fact ar borking any that existed
	delete heuristic;
	pq.push(soln_p);

	// OK, NOW initialize the treescores, since the sptree is now built and all gene trees and alignments are in place
	pqInitializeScores(parms);
	*parms.outscores << pqPrint("\nPQ of (1) species tree built by leaf addition:");
	*parms.outscores << "End species tree heuristic via leaf addition..." << endl;
	return;
	}

//*************************************************
	// 'Simple' heuristic to improve an input priority queue of tsas objects using a two step algorithm on each element.
	// On each iteration of a loop, do the following
	// 	(1) Keep gene trees fixed and use rSPR moves on species tree to improve it; keep one or more of the resulting sp trees
	//	(2) For each of the kept species trees, now keep sptree fixed and rearrange and reroot all gene trees to optimize
	//		the (possibly weighted) GTP+MP score.
	// Terminate if there is no improvement in the elements of the PQ, or if last iteration is finished.
	// Input state: the TSASHeuristic() object's  priority queue should be initialized to have one or more TSAS objects that have
	//		a complete tree set and alignments.
	// Output state: the PQ will be modified as more optimal TSAS objects with better GTP+MP scores are found.
	// Some nuances here involve truncating and/or merging some of these priority queues as we go to manage the search space.
	void simpleHeuristic(SearchParameters parms)
		{
		// Just in case treescores have not been initialized, check and do so. Note, these are initialized at the end of
		// leaf addition heuristic, which was a problem in previous versions.
		pqInitializeScores(parms);

		msgout << "Begin simpleHeuristic..." << endl;
		*parms.outscores << "Begin simpleHeuristic..." << endl;
		for (unsigned int i = 0;i<parms.sp_max_iter;i++)
			{
			msgout << "\nIteration "<< i << endl;
			*parms.outscores << "\nIteration "<< i << endl;
				//*parms.outscores << "gh\t" << gene_tree_search_score.printScores() << "\t" << data.speciestree << endl;	
			// Phase (1)
			optOverSpeciesTrees(parms); 
			msgout << pqPrint("\nPQ after sptree opt");
			*parms.outscores << pqPrint("\nPQ after sptree opt (may contain duplicates)");

			// Manage the search space in various ways
			deleteRedundant(); // Get rid of redundant TSAS objects (i.e., identical trees)
			truncate(parms.multrees);	// Makes a big difference to truncate here vs. after genetree opt! 

			msgout << pqPrint("\nPQ after deletion of duplicate solutions and truncation");
			*parms.outscores << pqPrint("\nPQ after deletion of duplicate solutions and truncation");
			// Phase (2)
			optOverGeneTrees(parms);	
			msgout << pqPrint("\nPQ after genetree opt");
			*parms.outscores << pqPrint("\nPQ after genetree opt");

			// Check if need to terminate
			if (pqUnchanged())
				{
				msgout << "\nPQ solution is unchanged; terminating early" << endl;
				*parms.outscores << "\nPQ solution is unchanged; terminating early" << endl;
				break;
				}
			}
		msgout << pqPrint("\nFinal PQ");
		*parms.outscores << pqPrint("\nFinal PQ");
		msgout << "\nEnd simpleHeuristic..." << endl;
		*parms.outscores << "\nEnd simpleHeuristic..." << endl;
		}

//*************************************************
//*************************************************


//*************************************************
// Low level TSAS heuristic search functions and management of PQs
//*************************************************
// Truncate the current priority queue to size
	void truncate(unsigned int size)
		{
		std::priority_queue<TSASDataPersistent> new_pq;
		while (!pq.empty() && size-- > 0)
			{
			new_pq.push(pq.top());
			pq.pop();
			}
		pq = new_pq; // ok bc uses copy constructor
		} 		


	// Take the TSAS's in current PQ and get the neighborhood of best species trees for each in its own PQ.
	// Then merge all these PQs into one. Note that the gene tree sets are correctly scored for each TSAS, but
	// they should be further optimized via GTPMP.
	void optOverSpeciesTrees(SearchParameters parms)
		{
		PQVector pqv;
		while (!pq.empty())
			{
			TSASDataPersistent dp = pq.top();
			pq.pop();
			PQ_TSAS pqNeighbors = speciesTreeSearchByRearrangements(dp, parms);
			pqv.push_back(pqNeighbors);
			}
		pq = pqv.mergePriorityQueues();
		}

	// Take the TSAS's in current PQ and optimize each via GTPMP; 
	void optOverGeneTrees(SearchParameters parms)
		{
		PQ_TSAS optimized_dps;
		while (!pq.empty())
			{
			TSASDataPersistent dp = pq.top();
			pq.pop();
			optimized_dps.push(optimizeGTPMP(dp,parms));
			}
		pq = optimized_dps;
		}



// Deletes 'redundant' TSAS objects from a priority queue
// 'Redundant' means two treesets are '==' according to treesetsEqual() function.
// This function is now modified to include an exact assessment of whether the sp trees are equal,
// together with the requirement that all genetree GTP and MP scores are equal between sets.
// [This could be made more efficient likely by using checksum function!]
// This does not mean all trees are topologically identical, though it is evidence in favor of that.
		void deleteRedundant()
			{
			std::list<TSASDataPersistent> sol_list;
			std::list<TSASDataPersistent>::iterator s1,s2;
			while (!pq.empty())
				{
				TSASDataPersistent ts = pq.top();
				sol_list.push_back(ts);
				pq.pop();
				}
			if (sol_list.size() >= 2)
				{
				s1=sol_list.begin();
				++s1;
				for (;s1 != sol_list.end(); s1++) // start with second element
					{ // decide if we delete this element by checking if any earlier ones in list are "equal"
					for (s2=sol_list.begin();s2 != s1; s2++) 
						{
						TSASDataPersistent t1 = *s1;
						TSASDataPersistent t2 = *s2;
						//if (t1.treeScoresEqual(t2))
						if (t1.treesetsEqual(t2)) // changed October 2022
							{
							sol_list.erase(s2);
							break;
							}
						}
					
					}
				}
			int soln=0;
			//msgout << "Non-redundant solutions in priority queue" << endl;
			for (s1=sol_list.begin();s1 != sol_list.end(); s1++) 
				{
				++soln;
				TSASDataPersistent ts = *s1;
				//msgout << soln << "\t" << ts.printScores() << "\t"  << ts.getSpeciesTree()<<endl;
				pq.push(*s1);
				}
			return;
			}

	TSASDataPersistent optimizeGTPMP (TSASDataPersistent dp, SearchParameters parms) 
		{
				// Phase 1: Optimize over the gene trees for fixed sp tree
				// In the Pthreads version, we also reroot gene trees (optionally) within the threaded code
				// In the non-Pthreads version, the rerooting is done below.
#ifdef PTHREADS
				TSASDataPersistent pthDP = pthMasterGTPMP(parms.randomseed,parms.root_flag,parms.n_threads,dp, \
					parms.gtpmp_max_iter,parms.gtp_weight, parms.mp_weight,parms.nni_radius);
				TreeScore gene_tree_search_score = pthDP.getTreeScore();
				TSASData data(pthDP);
				// At this point data has fully updated scores for all alignments and its total!
#else
				TSASData data(dp); // this has the current scores updated in the pthMaster func
				GeneTreeHeuristic gh_sp(data,parms.nni_radius);	
				TreeScore gene_tree_search_score = gh_sp.geneTreeHeuristicAll(parms.gtpmp_max_iter,parms.gtp_weight,parms.mp_weight);
#endif
				//*parms.outscores << "gh\t" << gene_tree_search_score.printScores() << "\t" << data.speciestree << endl;	
				TreeScore current_score = gene_tree_search_score;

#ifndef PTHREADS		// In pthreads we do the rooting within the threads
				if (parms.root_flag) 
					{
					TreeScore reroot_score = gh_sp.optimizeRooting(parms.gtp_weight, parms.mp_weight);
					//*outscores << "reroot" << "\t" << reroot_score << "\t-" <<  endl; // log the initial scores
					current_score = reroot_score;
					}

				data.set(current_score); // Unlike in pthreads path where it was already done, 
							// we need to update data's total score for use later
#endif

				TSASDataPersistent data_p(data); // need this again because of different paths through PTHREADS or not
				return data_p;
		}
//..................


//**************************************************************

// Search for a set of improved species trees from a starting species tree via SPR rearrangements, 
// keeping the gene tree sets constant.

// Input: a TSASDataPersistent object, having a species tree and gene trees
// Output: a priority queue of such objects ranked by total score, where the gene trees are all kept constant.

// This uses legacy duptree code (modified) to optimize over species trees, given fixed gene trees. Total score
// therefore only depends on the GTP score since the sum of genetree MP scores is not affected by twiddling the species tree.
// Species trees in the SPR neighborhood of the starting tree are examined, and a subset of them is returned by sampling in 
// various ways. 
// Once the species trees are computed, the individual GTP scores for each genetree are updated
// (it was not kept during the species tree heuristic).

	PQ_TSAS speciesTreeSearchByRearrangements(TSASDataPersistent data_p, SearchParameters parms)
		{
		// Setup the heuristic search of one SPR neighborhood of species trees
		std::priority_queue<TSASDataPersistent> pq;
		gtpspr::HeuristicPQ *heuristic = NULL; 
		//I've specialized this to HeuristicPQ, which avoids some bugs with using base class 
		//when we had several different heuristics--bit of a puzzle 
		heuristic = new gtpspr::HeuristicPQ(data_p);  
		heuristic->run(); // this makes SPRTrees with GTP scores only
		//msgout << "Species tree SPR Neighborhood PQ_SIZE = " << heuristic->pq_size()<< endl;

		//msgout << "  GTP scores: "; heuristic->dumpScores();

		// Get a sample of *UNIQUE/NONREDUNDANT* trees from the rSPR neighborhood search as PersistentTrees
		std::vector<PersistentTree<SpeciesTree>> sampled_trees;

		if (parms.hs_type == WITH_TIES) // this is the current default
			sampled_trees = heuristic->samplePQ(parms.h_multrees);
		else if (parms.hs_type == WITHOUT_TIES)
			sampled_trees = heuristic->samplePQByScores(parms.h_multrees);
		else 
			EXCEPTION("Error in HSearchType in func speciesTreeSearchByRearrangement");
		//...and add them to the 'this_iter' priority queue as TSAS Persistent objects
		int pix=0;
				// printing these queues is useful in debugging
		//if (parms.queue_flag)
		//	cout << "Species tree neighborhood: "<< endl;
		TSASData data(data_p); // need a live version of the data to do the updates next

		for (auto ptree : sampled_trees)
			{
			data.updateGTPScores(ptree,parms.gtp_weight,parms.mp_weight); 
			// Need to update all scores since all we got from heuristic was the species trees w/o scores
			// Note the species tree is free to change but not the gene trees.
			TSASDataPersistent data_p(data);  
			pq.push(data_p);
			}

		delete heuristic; // careful here with impact on postprocessLCA() on the heuristic.speciestree
		return pq;
		}

// ****************************************************************
// Utility functions
// ****************************************************************

// Check every TSAS in PQ and initialize scores if need to
	void pqInitializeScores(SearchParameters parms)
		{
		std::priority_queue<TSASDataPersistent> new_pq;
		while (!pq.empty() )
			{
			TSASDataPersistent dp = pq.top();
			if (dp.hasInitializedTreeScores()) 
				new_pq.push(dp);
			else
				{
				TSASData d(dp);
				d.initializeScores(parms.gtp_weight,parms.mp_weight);
				TSASDataPersistent dp1(d); 
				new_pq.push(dp1);
				}
			pq.pop();
			}
		pq = new_pq; // ok bc uses copy constructor
		} 		



	// Early termination criterion for search.
	// Compare the total scores of the tsas data objects in current PQ vs previous iterations. 
	// If whole vector of these is the same, signal ready to terminate early
	// Note that this is based only on the scores
	bool pqUnchanged()
		{
		pq_scores_current = pqGetScoreVector(pq);
		if (pq_scores_current == pq_scores_previous) 
				return true;
		else
				{
				pq_scores_previous = pq_scores_current;
				return false;
				}
		}
	string pqPrint(string title="")
		{
		ostringstream os;
		PQ_TSAS pq_new;
		if (title != "")
			os << title << endl;
		os << "D\tMP\twD+vMP\tw\tv\tGn treeset checksum\tSpecies Tree" << endl;
		os << ".................................................................................................."<<endl;
		while (!pq.empty())
				{
				TSASDataPersistent dp = pq.top();
				uint64_t check = dp.checksum();

				//*outpq << dp.printScores()<< "\t" << dp.getSpeciesTree() << ";" << endl;
				os << dp.printScores()<< "\t" << check << "\t" << dp.getSpeciesTree() << ";" << endl;
				pq_new.push(dp);
				pq.pop();
				}
		pq = pq_new;
		return os.str();
		}
	inline TSASDataPersistent best() { return pq.top();}
};



// Global function
// Using the old C functionality here: better suited to multiple delimiters, etc.; less wordy too
vector<string> tokenizer(string s,string delim) // default to EOL
	{
	vector<string> v;

	char *c_s = strdup(s.c_str());
 	const char *c_delim = delim.c_str();

	char *token;
	token  = strtok(c_s,c_delim);
	while (token != NULL)
		{
		string word(token);
		v.push_back(word);
		token = strtok(NULL,c_delim);
		}
	return v;
	}

#endif
