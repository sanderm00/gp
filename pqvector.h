#ifndef PQVEC
#define PQVEC

// Special class for a set of priority queues of TSAS persistent objects.
// Useful for merging them into one, truncating them, etc.
// By default, assume every interesting function borks the original priority queues, given their volatility

template <class T>
void mergePQs(std::priority_queue<T> &target, std::priority_queue<T> &source);

void pqTruncate(std::priority_queue<TSASDataPersistent> &pq, unsigned int size);

class PQVector
{
typedef std::priority_queue<TSASDataPersistent> PQType;
private:
	std::vector<PQType> pqv;

public:

	PQVector() { }

	void push_back(PQType pq)
		{ pqv.push_back(pq); }

	inline unsigned int size() { return pqv.size(); }

	inline PQType& at (size_t index) { return pqv.at(index); }

	// Merge all the PQs into one and return it
	PQType mergePriorityQueues()
		{
		if (pqv.size() == 0) EXCEPTION("Trying to merge empty PQVector");
		PQType m = pqv.at(0);
		if (pqv.size() == 1) 
			return m;
		for (size_t ix = 1;ix < pqv.size();ix++)
			mergePQs(m, pqv.at(ix));
		return m;
		}

	// Merge all the PQs into one and return it, but truncate it to max
	PQType mergePriorityQueues(unsigned int max)
		{
		PQType pq = mergePriorityQueues();
		pqTruncate(pq, max);
		return pq;
		}
};

#endif
