// Classes that describe individual tree rearrangements like NNI and SPR moves,
// as well as sequences or sets of those moves. 

#ifndef MOVES_H
#define MOVES_H

#if ROOTED
typedef GeneTreeRooted TREE;
typedef GeneNodeRooted NODE;
#else
typedef GeneTreeUnrooted TREE;
typedef GeneNodeUnrooted NODE;
#endif

/* These snippets might help with turning this into template  
template<class TREE>
class TreeIO : public TREE {  //MJS: i.e., this template parameter tells us what class TreeIO should inherit FROM...
public:
	using TREE::root;
	using TREE::nodes;
	using TREE::leafnodes;

	typedef typename TREE::Node NODE; ... typename tells us that TREE::Node is a type, and thus NODE will be a new type name, voila
						this gets us our needed node type from the tree type
	typedef typename TREE::LeafNode NAMEDNODE;
*/



// *****************************************
class NNI 	// Encapsulates a single NNI move 
		// Instantiating the object "stages" the nni before actually doing the move
		// Note. Rearranges original tree. Does NOT make a copy and work on that.

		// The nni is in one of two 'states'; either before the rearrangement or after, 
		// indicated by the 'hasBeenRearranged' flag. The 'getTree() func gets the tree
		// in whatever state it is (dangerous). The 'getPostTree()' checks the state and
		// does the rearrangement if necessary so as to always return the tree AFTER this
		// nni move.

		/* USAGE in setting up the nni array:
			NNI nni_forward(t,source);
			NNI nni_backward= nni_forward.reversed();

		*/
	{
	private:
// WORKS: TNode* source_node2;
	NODE* source_node;// will swap this node with its aunt
	TREE* tree; // the tree upon which this NNI acts
		// it's both a feature and a shortcoming that this can point to changing trees during lifespan of the object! Watch out.
	bool isForwardDirection = true;
	bool hasBeenRearranged = false;

	public:
	//NNI(TREE *t):tree{t} {};
	NNI(TREE *t,NODE* sn=nullptr):tree{t},source_node{sn} { }; // null ptr signals a non-move -- stays put at the tree
	// WORKS:  NNI(TREE *t,NODE* sn=nullptr):tree{t},source_node{sn}, source_node2{sn} { }; // formal cast works

	NNI reversed() // make a new nni that is the reverse of an existing one
		{
		auto nni_rev = *this;
		nni_rev.source_node = this->source_node->getAunt();
		nni_rev.isForwardDirection = !(this->isForwardDirection);
		return nni_rev;
		}
////// FIX null ptr for source node condition.....
	inline void rearrange() // rearrange tree using source_node, but only if currently unrearranged
		{
		if (hasBeenRearranged==false) 
			{
			//tree->oprNNI(source_node);
			tree->moveSubtree(source_node,source_node->grandparent());
			hasBeenRearranged=true;
			}
		}
	inline void print() { msgout << "NNI: " << tree << "; " << source_node << endl; }
	inline auto getTree() { return tree; }	// This doesn't respect the current rearrangement status of the nni
	inline auto getPostTree()	// i.e., the tree AFTER this nni has been completed 
		{ 
		rearrange();	// make sure this nni has been rearranged, else do so now
		return tree;	// Thus, this tree is the "post" tree for this nni 
		}
	inline auto getSource() { return source_node; }
	inline bool hasRearranged() {  return hasBeenRearranged; }
	inline bool isForward () {  return isForwardDirection; }
	inline bool isPostTreeNovel () {  return isForwardDirection; }
	};

/*
 Encapsulates the infrastructure to maintain a sequence of NNI moves that can be used elsewhere (e.g. neighborhoods)
 These are kept as a vector of NNI objects, the order of which is important.
 Thus, assuming there are 2k elements in the nni_seq array
........................................................................................................................
nni_seq index			0       	1       	2       	3	...	2k-1
tree		t_0=reference_tree	t_1		t_2		t_3		t_{2k-1}	t0=reference_tree
tree_pos		0		1		2		3		2k
........................................................................................................................

So we tour from reference tree, t_0, through 2k other trees, the last of which is back to the reference tree.
Some of these trees are repeated as they represent the nodes of a traversal of a tree itself.

nni_seq[i] describes a move from t_i to t_{i+1}, where t_i is the "pre-tree" and t_{i+1} is the "post-tree".
*/


class NNI_Sequence
	{
	//protected: 
	public: 
		vector<NNI> nnis;
	public:
		NNI_Sequence() { } ;
		std::vector<NNI>::iterator begin() {return nnis.begin();};	
		std::vector<NNI>::iterator end() {return nnis.end();};	

		inline void clear() {nnis.clear();}

		inline void push_back(NNI nni) {nnis.push_back(nni);}


		inline int position(std::vector<NNI>::iterator itr) // the 'array index' (0-off) position of this iterator in the underlying array
			{
			return std::distance(nnis.begin(),itr);
			}
		std::vector<NNI>::iterator moveToNNIat(int pos) // returns iterator to NNI at pos, and leaves tree in post state
			//buggy: fixed 3/30/22
			{
			auto itr = begin();
			(*itr).rearrange();
			for (int i=0;i<pos;i++)
				{
				(*(++itr)).rearrange();
				}
			return itr;	
			}

		// To unwind a moveToPosition, just run out to the end of the nni sequence
		void moveBackToReference(std::vector<NNI>::iterator itr) 
			{
			//while (itr!=nnis.end()) 
int c=0;
			while (++itr!=nnis.end()) 
				{
				(*itr).rearrange();
(*itr).print();
cout << "===Before=== " << ++c <<  "  " << (*itr).getTree()->tree2newick() << endl;
				//++itr; 
				}
			}
		void moveReferenceTreeToHere(int tree_pos)
			{
			if (tree_pos == 0)
				return; 
			else
				{
				auto itr = NNI_Sequence::moveToNNIat(tree_pos-1);
				return;			
				}
			}
		string toNewickString(int tree_pos)
			// Takes O(n) to find tree and then go to end of nni array to move back to reference tree.
			// BE CAREFUL IF MODIFYING THE REFERENCE TREE DURING SEARCHES...INVALIDATES THIS.
			{
			if (tree_pos == 0)
				{
				auto itr = begin();
				return (*itr).getTree()->tree2newick(); // get the pre-tree
				}
			else
				{
				auto itr = moveToNNIat(tree_pos-1);
				string s = (*itr).getPostTree()->tree2newick();
				moveBackToReference(itr);
				return s;			
				}
			}


	};



class SPR // rooted SPR move
	{
	public:
	TREE* tree;
	NODE* subroot; // the root of the subtree to be pruned
	NODE* target_node; // subroot subtree will become sib of this node
//	bool isRefTree=false; // check the move when initialized to see if it moves to itself--i.e., stays the reference tree

	public: 
		SPR() {};
		// NB. If sr and tn are missing, move is interpreted as staying put at the starting tree
		SPR(TREE* t, NODE* sr=nullptr, NODE* tn=nullptr): tree{t},subroot{sr},target_node{tn} 
			{
			}
	bool isReferenceTree() 
		{
		if (tree != nullptr && subroot == nullptr) return true;
		else return false;
		}

	inline void rearrange() 
		{
		if (subroot != nullptr) 
			tree->moveSubtree(subroot,target_node);
		}
	inline auto getTree() { return tree; }	// This doesn't respect the current rearrangement status of the nni

	inline void print()
		{
		msgout << "SPR tree,subroot,target: " << tree << " " <<  subroot << " " << target_node << endl;
		}
	};

class SPR_Sequence
	{
	protected: 
		vector<SPR> sprs;
	public:
		SPR_Sequence() { } ;

		inline void clear() {sprs.clear();}

		inline void push_back(SPR spr) {sprs.push_back(spr);}
		std::vector<SPR>::iterator begin() {return sprs.begin();};	
		std::vector<SPR>::iterator end() {return sprs.end();};	
		inline int position(std::vector<SPR>::iterator itr) // the 'array index' (0-off) position of this iterator in the underlying array
			{
			return std::distance(sprs.begin(),itr);
			}

// Following are useful in future? when might need a sequence of dependent SPR moves...
		std::vector<SPR>::iterator moveToSPRat(int pos) // returns iterator to NNI at pos, and leaves tree in post state
			//buggy: fixed 3/30/22
			{
			EXCEPTION("NOT BUILT YET");
			auto itr = begin();
			// .....
			return itr;	
			}
		void moveReferenceTreeToHere(int tree_pos)
			{
			if (tree_pos == 0)
				return; 
			else
				{
				SPR spr = sprs[tree_pos-1];
				spr.rearrange();
				return;			
				}
			}

	};


#endif



