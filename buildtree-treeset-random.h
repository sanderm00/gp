/*
 * Copyright (C) 2007 Andre Wehe, Mukul Bansal, Oliver Eulenstein
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef BUILDTREE_TREESET_RANDOM_H
#define BUILDTREE_TREESET_RANDOM_H

// ------------------------------------------------------------------------------------------------------------------
class SepciesNode;
class NamedSepciesNode;
class GeneNode;
class NamedGeneNode;

// ------------------------------------------------------------------------------------------------------------------
// species node
class SpeciesNode : public TreeNodeRooted<SpeciesNode> {
public:
	SpeciesNode(SpeciesNode  *parent = NULL) : TreeNodeRooted<SpeciesNode>(parent) {}
	virtual ~SpeciesNode() {}

	friend ostream & operator << (ostream & os, SpeciesNode & m);
};

// output a node into a string stream
ostream & operator << (ostream & os, SpeciesNode & m) {
	os << (TreeNodeRooted<SpeciesNode> &)m;
	return os;
}

// ------------------------------------------------------------------------------------------------------------------
// species node with name
class NamedSpeciesNode : public SpeciesNode, public NodeID {
public:
	NamedSpeciesNode(const string &id, SpeciesNode *parent = NULL) : SpeciesNode(parent), NodeID(id) {}
	virtual ~NamedSpeciesNode() {}

	friend ostream & operator << (ostream & os, NamedSpeciesNode & m);
};

// output a node into a string stream
ostream & operator << (ostream & os, NamedSpeciesNode & m) {
	os << (NodeID &)m << ", "
	   << (SpeciesNode &)m;
	return os;
}

// ------------------------------------------------------------------------------------------------------------------
// gene node
class GeneNode : public TreeNodeRooted<GeneNode> {
public:
	GeneNode(GeneNode *parent = NULL) : TreeNodeRooted<GeneNode>(parent) {}
	virtual ~GeneNode() {}

	friend ostream & operator << (ostream & os, GeneNode & m);
};

// output the node into a string stream
ostream & operator << (ostream & os, GeneNode & m) {
	os << (TreeNodeRooted<GeneNode> &)m;
	return os;
}

// ------------------------------------------------------------------------------------------------------------------
// gene node with name
class NamedGeneNode : public GeneNode, public NodeID {
public:
	NamedGeneNode(const string &id, GeneNode *parent = NULL) : GeneNode(parent), NodeID(id) {}
	virtual ~NamedGeneNode() {}

	friend ostream & operator << (ostream & os, NamedGeneNode & m);
};

// output a node into a string stream
ostream & operator << (ostream & os, NamedGeneNode & m) {
	os << (NodeID &)m << ", " << (GeneNode &)m;
	return os;
}

// ==================================================================================================================
// ==================================================================================================================
// ==================================================================================================================

// ------------------------------------------------------------------------------------------------------------------
class SpeciesTree : public TreeIO<Tree<SpeciesNode, NamedSpeciesNode> > {
public:
	SpeciesTree() : TreeIO<Tree<SpeciesNode, NamedSpeciesNode> >() {}
	virtual ~SpeciesTree() {}
};

// ------------------------------------------------------------------------------------------------------------------
class GeneTree : public TreeIO<Tree<GeneNode, NamedGeneNode> > {
public:
	bool rooted;
	GeneTree() : TreeIO<Tree<GeneNode, NamedGeneNode> >() {
		rooted = true;
	}
	virtual ~GeneTree() {}
};

// ==================================================================================================================
// ==================================================================================================================
// ==================================================================================================================

// -----------------------------------------------------------------------------------------------------------------
class BuildTreeSet {
public:
	// data container for the gene trees
	vector<GeneTree*> genetree;

	// data container for the species tree
	SpeciesTree speciestree;

	BuildTreeSet() {
	}

	~BuildTreeSet() {
		for(vector<GeneTree*>::iterator itr=genetree.begin(); itr!=genetree.end(); itr++) delete *itr;
	}

	// ------------------------------------------------------------------------------------------------------
	// read all trees from the input
	void readTrees(Input &input) {
			char c;
			// read all gene trees
			for (;;) {
				string str;
				bool rooted = true;
				while (!(str=input.getComment()).empty()) {
					if (str == "[&U]") rooted = false;
					if (str == "[&R]") rooted = true;
				}
				GeneTree *tree = new GeneTree;
				if (tree->stream2tree(input)) {
					genetree.push_back(tree);
					tree->rooted = rooted;
				} else {
					delete tree;
					break;
				}
			}
			if (genetree.size() < 1) EXCEPTION("no input for the gene trees"); // ERROR no gene trees
	}

	// ------------------------------------------------------------------------------------------------------
	// constructs a random species tree
	void createRandomTree() {
		// build list of unique leaf nodes
		set<string> leafs;
		for (vector<GeneTree*>::iterator itr = genetree.begin(); itr != genetree.end(); itr++) {
			GeneTree &genetree = **itr;
			for (vector<NamedGeneNode*>::iterator itr = genetree.leafnodes.begin(); itr != genetree.leafnodes.end(); itr++) {
				NamedGeneNode &node = **itr;
				leafs.insert(node.getName());
			}
		}

		// create species tree leaf nodes
		vector<SpeciesNode*> nodes;
		for (set<string>::iterator itr=leafs.begin();itr!=leafs.end();itr++) {
			NamedSpeciesNode *node = new NamedSpeciesNode(*itr);
			nodes.push_back(node);
			speciestree.nodes.push_back(node);
			speciestree.leafnodes.push_back(node);
		}

		// build a random tree from the leaf nodes
		while (nodes.size() >= 2) {
			// pick 2 random nodes and create a common parent
			SpeciesNode *node = new SpeciesNode();
			for (int i=0; i<2; i++) {
				const int r = (int) (nodes.size() * (rand() / (RAND_MAX + 1.0)));
				node->child(i) = nodes[r];
				node->child(i)->parent() = node;
				nodes.erase(nodes.begin()+r);
			}
			// insert the new parent node
			nodes.push_back(node);
			speciestree.nodes.push_back(node);
		}

		// connect root node
		speciestree.root = nodes[0];
	}

	// ------------------------------------------------------------------------------------------------------
	// outputs the trees into a string stream
	friend ostream & operator << (ostream &os, BuildTreeSet &t);
};

// outputs the trees into a string stream
ostream & operator << (ostream &os, BuildTreeSet &t) {
	for(int i = 0; i < t.genetree.size(); i++ ) os << "gene tree" << i << ':' << endl << *t.genetree[i] << endl;
	os << "species tree:" << endl << t.speciestree << endl;
	return os;
}

#endif
