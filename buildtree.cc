/*
 * Copyright (C) 2007 Andre Wehe, Mukul Bansal, Oliver Eulenstein
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <signal.h>
#include "common.h"
#include "argument.h"
#include "input.h"
#include "node.h"
#include "tree.h"
#include "buildtree-treeset-random.h"
#include "buildtree-treeset-leafadd.h"
#include "heuristic-leafadd.h"
#include "buildtree-heuristicsecondfast.h"
#include "buildtree-heuristicsecond.h"

int main(int argc, char *argsv[]) {
	initialization();
	signal(SIGINT, interruptFunction);
	Argument::add(argc, argsv);

	// random seed given
	unsigned int randomseed = unsigned(time(NULL)) % 0xFFFF;
	{
		const Argument *arg = Argument::find("--seed");
		if (arg != NULL) arg->convert(randomseed);
		srand(randomseed);
	}

	// choose tree generator
	int generator;
	{
		const Argument *arg1 = Argument::find("-e");
		const Argument *arg2 = Argument::find("--generator");
		const Argument *arg = arg1 != NULL ? arg1 : arg2;
		if (arg == NULL) generator = 1;
		else arg->convert(generator);
	}

	// output input trees
	bool pipeFlag = false;
	{
		const Argument *arg1 = Argument::find("-g");
		const Argument *arg2 = Argument::find("--genetrees");
		const Argument *arg = arg1 != NULL ? arg1 : arg2;
		if (arg != NULL) {
			pipeFlag = true;
			if (arg->hasValue()) EXCEPTION("--genetrees doesn't need a value");
		}
	}

	// puts the leaf add into fast mode
	bool fastFlag = false;
	{
		const Argument *arg1 = Argument::find("-f");
		const Argument *arg2 = Argument::find("--fast");
		const Argument *arg = arg1 != NULL ? arg1 : arg2;
		if (arg != NULL) {
			fastFlag = true;
			if (arg->hasValue()) EXCEPTION("--fast doesn't need a value");
		}
	}
	if (fastFlag) {
		if ((generator != 1) && (generator != 2)) WARNING("--fast does not apply to generator " << generator);
	}

	// constraints input
	istream *constraints_in;
	ifstream constraints_infile;
	{
		const Argument *arg = Argument::find("--constraints");
		if (arg == NULL) constraints_in = NULL;
		else {
			string filename;
			arg->convert(filename);
			msgout << "Constraints file: " << filename << endl;
			constraints_infile.open(filename.c_str());
			if (!constraints_infile) EXCEPTION("constraints file " << filename << " not found");
			constraints_in = &constraints_infile;
			if ((generator != 1) && (generator != 2)) WARNING("--constraints does not apply to generator " << generator);
		}
	}


	// get a pointer to the input file
	ifstream in_file;
	{
		const Argument *arg1 = Argument::find("--input");
		const Argument *arg2 = Argument::find("-i");
		const Argument *arg = arg1 != NULL ? arg1 : arg2;
		if (arg != NULL)
		{
			string filename;
			arg->convert(filename);
			in_file.open(filename.c_str());
			if (!in_file) EXCEPTION("Input file " << filename << " not found");
		}
	}



	Input constraints_input(constraints_in);

	#include "commonarguments.cc"

	// output help message
	if (helpFlag) {
		cout << "Usage: " << argsv[0] << " [OPTION]" << endl;
		cout << endl;
		cout << "  -i, --input                   Input file name" << endl;
		cout << "  -o, --output                  Output file name" << endl;
		cout << "  -g, --genetrees               Output the input gene trees after the speciestree" << endl;
		cout << "  -e, --generator               The algorithm used to build the speciestree." << endl;
		cout << "                                1 - leaf adding heuristic [default]" << endl;
		cout << "                                2 - random tree" << endl;
		cout << "  -f, --fast                    fast mode; only for generator 1" << endl;
		cout << "      --constraints <file>      A file containing groupings of species for the leaf adding heuristic." << endl;
		cout << "  -q, --quiet                   No processing output." << endl;
		cout << "      --seed <integer number>   Set a user defined random number generator seed." << endl;
		cout << "  -v, --version                 Output the version number." << endl;
		cout << "  -h, --help                    Give a brief help message about the arguments and example." << endl;
		cout << endl;
		cout << "Example:" << endl;
		cout << "  " << argsv[0] << " -e 1 -i genetrees.newick -o speciestree.newick" << endl;
		cout << "  " << argsv[0] << " -e 2 -f -i genetrees.newick -o speciestree.newick" << endl;
		return 0;
	}

	// random seed
	msgout << "Random seed: " << randomseed << endl;

	// run tree generator
	switch (generator) {
		case 1: {
			HeuristicLeafAdd *heuristic = NULL;
			if(fastFlag)
			{
				msgout << "Using fast randomized leaf adding heuristic" << endl;
				heuristic = new HeuristicSecondFast();

				// read input trees
				heuristic->readTrees(input);

				// read constraints
				if (constraints_in!=NULL) heuristic->readConstraints(constraints_input);
			}

			else
			{
				msgout << "Using randomized leaf adding heuristic [default]" << endl;
				heuristic = new HeuristicSecond();

				// read input trees
				heuristic->readTrees(input);

				// read constraints
				if (constraints_in!=NULL) heuristic->readConstraints(constraints_input);
			}

			ReRoot reroot=OPT;
			heuristic->run(*out, reroot);
			if (pipeFlag)
			{
				std::string line;
     				while(getline(in_file, line))
				{
          				*out<<line<<'\n';
     				}
			}
			delete heuristic;
		} break;
		case 2: {
			msgout << "Building random tree" << endl;

			// read in the gene trees
			BuildTreeSet trees;
			trees.readTrees(input);

			// create a random species tree from the gene trees
			trees.createRandomTree();

			// output speciestree
			trees.speciestree.tree2newick(*out); *out << endl;

			// output genetrees
			if (pipeFlag) {
				for (int i = 0; i < trees.genetree.size(); i++) {
					if (trees.genetree.at(i)->rooted == false) *out << "[&U] ";
					trees.genetree.at(i)->tree2newick(*out); *out << endl;
				}
			}
		} break;

		default: {
			EXCEPTION("Unknown heuristic " << generator);
		} break;
	}

	#ifdef DEBUG
	for (map<string,int>::iterator itr=NodeID::namelist.begin();itr!=NodeID::namelist.end();itr++)
		cerr << "species namelist entry:" << itr->first << ' ' << itr->second << endl;
	#endif
	return 0;
}
