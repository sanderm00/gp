#ifndef ALIGNMENT_H
#define ALIGNMENT_H


// Class for a single sequence alignment and possibly a starting tree stored as a PersistentTree only. 
// Also contains functions to compute MP scores on alignments, given a gene tree.
// We don't store tree data structures here.  They belong in TreeSet-like classes.
// Labels assumed to match between the alignment and tree.

// Various rules may be in place to provide a mapping from this trees leaf labels to a species tree's leaf labels...

#include "gtp-spr-treeset.h"
#include "view.h"
#include "savedtrees.h"
#include <typeinfo>
#include <queue>
#include <random>

#define DNAIUPAC "-aAcCgGtTuUrRyYsSwWkKmMbBdDhHvVnN?" // valid symbols for DNA plus dash and question mark for missing
#define VALIDAA "-?*abcdefghiklmnpqrstuvwxyzABCDEFGHIKLMNPQRSTUVWXYZ"	// valid symbols for AA plus dash and question for missing and * for stop 


// Making this a global function here. Should probably be part of a name class or the like (repeated in node.h class NodeID)
string parseGetSpName(const string &id) // parse the node name and return the part of it corresponding to species name
		{
		string delim = "__"; // double underscore!
		std::size_t found = id.find(delim);
		if (found!=std::string::npos)
			return id.substr(0,found);
		else
			return string(id);		// no parsing was done due to lack of delimiter; just return the whole string unparsed
		}


class Alignment {

#if ROOTED
typedef GeneTreeRooted TREE;
typedef GeneNodeRooted NODE;
#else
typedef GeneTreeUnrooted TREE;
typedef GeneNodeUnrooted NODE;
#endif
 
private:
	unsigned int* boot_wts;
	bool has_boot_wts=false; // internal flag set when weights are initialized below, and then checked during MP scoring funcs
public:
	PersistentTree<TREE> starting_tree;

	//PersistentTree<TREE> best_mptree_ever;
	unsigned int bestever_mp_score; // keep best ever score here and next
	unsigned int bestever_gtp_score;

	unsigned int ntax=0; 	// let's assume no ultralarge phylogenies
	unsigned int nspp=0;	// ntax = genes; nspp = species...
	unsigned long nchar=0;  // but the alignment could be large  // looks like a assume this is int everywhere else...
	unsigned int gtpScore;	// between the gene's 'tree' and some species tree somewhere...
	unsigned long mpScore;	// relative to the gene's 'tree'
	bool hasValidAlignment=false;
	bool hasValidGeneTree=false; // meaning, has read a starting tree

	// the following vectors all have the same index, 'ix'
	vector<string> gene_ids;	// the gene taxon labels in order of the alignment
	vector<string> raw_seqs; // the raw sequences in order of the alignment (whitespace removed)
	vector<SITEINT*> encoded_seqs; // the sequences encoded one base or AA per unsigned int
					// the pointers point to arrays of ints for each sequence
	map<string,unsigned int> gene_id_to_ix_h; // hash to look up vector index for given gene taxon label key
	map<string,unsigned int> sp_name_h; // hash that counts multiplicity of species names in gene alignment and presumably tree

	inline unsigned long getMPScore() {return mpScore;}
	inline unsigned int getGTPScore() {return gtpScore;}
	inline void setMPScore(unsigned long mp) {mpScore=mp;}
	inline void setGTPScore(unsigned int gtp) {gtpScore=gtp;}

	Alignment () { }
	Alignment (Input &input) 
		{
		readRelaxedPhylip(input); 
		//msgout << "Parsing and validation of alignment file complete..." << endl;
		if (hasValidAlignment)
			{
			convertRawToEncodedSequences();
			for (unsigned int i=0;i<ntax;i++)
				gene_id_to_ix_h[gene_ids[i]]=i;	// setup hash to find the index in the sequence vector for the taxon label...
			//for(vector<NODE*>::iterator itr=g.begin(); itr!=g.end(); itr++) 
			for(auto gene_id:gene_ids)
				{
				string sp_name = parseGetSpName(gene_id);
				sp_name_h[sp_name]++;
				}
			nspp = sp_name_h.size();
			//msgout << "\tAlignment has "<<ntax<<" sequences in "<<nspp<< " species and "<<nchar<<" sites" <<endl;
			}
		//if (hasValidGeneTree) 
		//	{ msgout << "Starting gene tree saved" << endl; }

		boot_wts = new unsigned int[nchar];	// array might be 'long' (have many elements), but each is an uint
		for (unsigned long i=0;i<nchar;i++) boot_wts[i]=0; 

		}
	~Alignment()
		{
			for (SITEINT* es:encoded_seqs) delete[] es;
			delete[] boot_wts;
		}
	void initializeBootWeights(std::mt19937 generator)
		{

//   		std::mt19937 generator(seed);  put this in init of program
		std::uniform_int_distribution<unsigned int> random_site(0,nchar-1);

		for (unsigned long i=0;i<nchar;i++) 
			{
			unsigned long site = random_site(generator);
			boot_wts[site]++;
			}
		//msgout << "Bootstrap weights initialized to:" << endl;	
		//for (unsigned long i=0;i<nchar;i++) 
		//	msgout << boot_wts[i] << " ";
		//msgout << endl;
		has_boot_wts=true;
		}


	bool satisfies(Filters filters)
		{
			if (ntax < filters.min_leaves) return false;
			if (nspp < filters.min_spp) return false;
			if (nchar< filters.min_chars) return false;
			if (ntax > filters.max_leaves) return false;
			if (nspp > filters.max_spp) return false;
			if (nchar> filters.max_chars) return false;
			return true;
		}


public: void printPhylip()
	{
	msgout << ntax << "\t" << nchar << endl;
	for (int i=0;i<ntax;i++)
		{
		msgout << gene_ids[i] << "\t" << raw_seqs[i] << endl;
		}
	msgout << starting_tree.newick() << endl;
	}


// An older formulation to do mp scoring; used now as a check
// Note! This allocates memory for chars at internal nodes of tree; these should be deleted when tree is,
// BUT, there are also encoded seqs at leaf nodes; these should remain throughout life of program; see node.h destructor
public: void initializeEncodedSeqsAtNodes(TREE* tree)
	{
		vector<NODE*> &g = tree->nodes;
		for(vector<NODE*>::iterator itr=g.begin(); itr!=g.end(); itr++) 
			{
			NODE *&n = *itr;
			if (n->isLeaf()) // for a leaf, store a pointer to the encoded seq at the leaf node
				{
				string gene_id = tree->getNodeWName(n); 
				std::map<string,unsigned int>::iterator it;
				//msgout << "sp id="<<tree->getNodeName(n)<< " gene id="<<gene_id<<endl;
				it = gene_id_to_ix_h.find(gene_id);
				if (it == gene_id_to_ix_h.end())
					EXCEPTION("A taxon name in the gene tree was not found in the alignment");
				unsigned int ix = it->second;
				n->setEncodedSequence(encoded_seqs[ix]);
				}
			else	// for internal node, store a pointer to a zero-ed encoded seq at that node
				{
				if (n->getEncodedSequence() == nullptr)
					{
					SITEINT* pp = new SITEINT[nchar];
			    		for (int i=0;i<nchar;i++)
						pp[i]=0;
					n->setEncodedSequence(pp);
					}
				}

			//msgout << "Node name => " << tree->getNodeName(n) << endl;
			}
	}

//******************************************************************************************************************

	// Compute the mp score for each site and summed across sites for the class's alignment and gene tree.
	// Calls a postorder traversal (leaves toward root), which sets up state sets at internal nodes and,
	// in the process, finds the minimum number of state changes. State sets are initialized at leaves using
	// a bit encoding in an unsigned int. State change 'assumptions' are that all
	// sites are unordered (Fitch parsimony), which allows very simple bitwise operations to determine the
	// state set of a parent node from each of it's children's state sets (se below). Importantly, for any
	// node in which the intersection of the two children's state sets is empty, there must be one 
	// state change along one of the two parent-child edges. A follow-up traversal from root to leaves is
	// needed to actually infer the ancestral states at every internal node, but this is not necessary to
	// merely compute the mp score.

	public:  long computeMPScore(TREE *t)	
		{
		//int *scores = new int [nchar]; // there was no reason for this to by dynamic (and undeleted!)
		int scores[nchar];
		for (int i=0;i<nchar;i++) scores[i]=0;
		traverseMP(t->root,scores);
		int score = 0;
#if BOOT
		for (int i=0;i<nchar;i++) score += boot_wts[i]*scores[i];
#else
		for (int i=0;i<nchar;i++) score += scores[i];
#endif
		return score;
		}



	private: void traverseMP(NODE *&node, int *score) 
	// [arg does not  have to be *& ] 
	// score[] is an array holding the current mp score at each site; must be initialized to all 0s before calling
		{
		SITEINT iintersection, iunion;
		if (node == NULL) return;
		if (node->isLeaf()) return;	// only computes on internal nodes
		//msgout << "Traversing: " << tree->getNodeName(node)  << endl;
		for (int i=0; i<2; i++)
			traverseMP(node->child(i),score);
		SITEINT* seq  = node->getEncodedSequence();
		SITEINT* seq0 = node->child(0)->getEncodedSequence();
		SITEINT* seq1 = node->child(1)->getEncodedSequence();
		for (int j=0;j<nchar;j++)
			{
			iintersection = seq0[j] & seq1[j];
			if (iintersection == 0) 	// intersection of two childrens' state sets is empty
				{ 
				iunion = seq0[j] | seq1[j]; // so we use the union
				seq[j]=iunion;	// save the new state set at this node
				score[j]++;	// this is the case where there is a state change
				}
			else				// intersection has states
				seq[j]=iintersection;	// so save that state set

			}
		}
	// Following is used throughout project and is public therefore
	public: unsigned int computeStateSet(SITEINT* ss_child_0, SITEINT* ss_child_1, SITEINT* ss_parent)
		{
		SITEINT iintersection, iunion; 
		unsigned int mpScoreIncrement=0;
		for (int j=0;j<nchar;j++)
			{
			iintersection = ss_child_0[j] & ss_child_1[j];
			if (iintersection == 0) 	// intersection of two childrens' state sets is empty
				{ 
				iunion = ss_child_0[j] | ss_child_1[j]; // so we use the union
				ss_parent[j]=iunion;	// save the new state set at this node
// This tiny if statement and the addition of boot_wts, doubles the execution time.
// When I write a separate version of computeStateSet for each case so there is no if inside here,
// it still seems to take about twice the time, even when NOT doing bootstrap...doesn't add up!
// SO AT THE MOMENT I'VE SLOWED THE PROGRAM DOWN 2x EVEN WHEN NOT BOOTSTRAPPING.
				//if (has_boot_wts) {;} else {;} this toy example does NOT add runtime
				// so seems like the following does because it can't linearize the code anymore maybe?? 
#if BOOT
				mpScoreIncrement += boot_wts[j];
#else
				++mpScoreIncrement;
#endif				
//				mpScoreIncrement += (temp_flag & boot_wts[j]) + (temp_flag1 & 1);
				}
			else				// intersection has states
				ss_parent[j]=iintersection;	// so save that state set
			}
		return mpScoreIncrement;
		}

	// Following is only used in setupStateViews() function below and is therefore private
	// Assumes stateSet vectors have been instantiated.
	private: void computeStateSetAt(View view)
		{
		NODE* children[2];
		auto node = view.view_node;
		auto view_from_node = view.view_from;
		node->getChildrenDirected(view_from_node, children); // children "opposite" view_from node
		//unsigned int* ss_node    = node->getStateSet(view_from_node);
		SITEINT* ss_node    = view.getStateSet();

		// checking for non-trinary nodes
		SITEINT *ss_child_0 = children[0]==nullptr ? nullptr : children[0]->getStateSet(node) ;
		SITEINT *ss_child_1 = children[1]==nullptr ? nullptr : children[1]->getStateSet(node) ;

		unsigned int mpc0 = children[0]==nullptr ? 0 : children[0]->getMPScore(node) ;
		unsigned int mpc1 = children[1]==nullptr ? 0 : children[1]->getMPScore(node) ;

		// handle degree 2 nodes and error condition of degree 1 node, which should not be called on leaves
		if (ss_child_0 == nullptr && ss_child_1==nullptr) EXCEPTION ("Two children were NULL in computeStateSetAt");
		if (ss_child_0 == nullptr) 
			{
			for (int j=0;j<nchar;j++) ss_node[j]=ss_child_1[j];
			node->setMPScore(view_from_node,mpc1);
			return;
			}
		if (ss_child_1 == nullptr) 
			{
			for (int j=0;j<nchar;j++) ss_node[j]=ss_child_0[j];
			node->setMPScore(view_from_node,mpc0);
			return;
			}

#if 0  // Was unnecessary to repeat this code...
		// normal  interesting case of a trinary node
		unsigned int iintersection, iunion, mpScoreIncrement=0;
		for (int j=0;j<nchar;j++)
			{
			iintersection = ss_child_0[j] & ss_child_1[j];
			if (iintersection == 0) 	// intersection of two childrens' state sets is empty
				{ 
				iunion = ss_child_0[j] | ss_child_1[j]; // so we use the union
				ss_node[j]=iunion;	// save the new state set at this node
				++mpScoreIncrement;
				}
			else				// intersection has states
				ss_node[j]=iintersection;	// so save that state set
			}
		unsigned int mpScore = mpScoreIncrement + mpc0 + mpc1;
#else
		unsigned int mpScore = computeStateSet(ss_child_0,ss_child_1,ss_node) + mpc0 + mpc1;
		
#endif
		//  Finally, assign the mp score for this view 
		node->setMPScore(view_from_node,mpScore);
		return; 

		}
//******************************************************************************************************************

/*
Read an alignment and optional tree in relaxed phylip format. Here is an example.
Note that it is relaxed about whitespace (but respects end of lines),
and about taxon labels (including that it allows quotes)

Note also that it probably borks if there is a '(' character in the alignment

3 20
a	acgt

'b xvxcv'   accc
c aggg

(a,('b xvxcv',c));

*/
public: void readRelaxedPhylip(Input &input)
	{
	char c;

	if (!input.nextAnyChar(c)) 
		EXCEPTION("File contains no readable text\n");
	if (c != '(') 
		{ // this allows us to read just a tree description
		input.pushBack(c); 	
		// Note the following does not pinpoint location of errors on this line exactly
		if (!input.readNumber(ntax)) EXCEPTION("Error reading ntax\n");
		if (!input.readNumber(nchar)) EXCEPTION("Error reading nchar\n");
		string gene_id;
		string raw;
		string td;
		if (ntax>0) 
			{
			for (int i=0;i<ntax;i++)
				{
				gene_id=input.getNameMJS();
				if (gene_id != "")
					gene_ids.push_back(gene_id);					
				raw = input.restOfLineNoWhiteSpace();
				if (raw != "")
					raw_seqs.push_back(raw);
				}
			}
		else
			EXCEPTION("ntax cannot be set to zero in alignment file\n");			
		validateAlignment();
		} // end processing possible alignment
	else
		input.pushBack(c);
	if (input.nextAnyChar(c))
		{
		if (c=='(')
			{
			input.pushBack(c); // used up the left paren; have to push it back so following line gets the while newick string
			string st = input.restOfLine(';');
			starting_tree = PersistentTree<TREE>(st);
			hasValidGeneTree=true;
			}
		}
	}
private:
	void validateAlignment() // really just validates the alignment
		{
		//msgout << "Validating gene file..." << endl;
		bool error=false;
		if (gene_ids.size() != ntax || raw_seqs.size() != ntax) 
			EXCEPTION("Mismatch between specified number of taxa and alignment read\n");
		for (int i=0;i<ntax;i++)
			{
			unsigned long len=raw_seqs[i].length();
			if (len != nchar) 
				EXCEPTION("Sequence " << i+1 << " length (" << len << ") did not match that expected from dimensions (" << nchar << ")\n");
			std::size_t found;
			if (seqDataType == NT)
	  			found = raw_seqs[i].find_first_not_of(DNAIUPAC);
			else if (seqDataType == AA)
	  			found = raw_seqs[i].find_first_not_of(VALIDAA);
			else
				EXCEPTION("Problem with seqDataType in validateAlignment");
  			if (found!=std::string::npos) // std search function in prev line returns this if nothing found
  				{
				msgout << "Invalid [NOW VERY STRICT!] character in sequence "<< i+1 << " at position "<<found << endl;
				msgout << raw_seqs[i] << endl;
				for (int j=0;j<found;j++) msgout << " ";
				msgout << "^" << endl;
				error=true;
				}
			}
		if (error) 
			EXCEPTION ("Invalid characters found. Exiting...\n");
		if (ntax>0 && nchar>0)
			{
			hasValidAlignment=true;
			//msgout<<"Valid alignment parsed" << endl;



			}
		}

	// This func needed for both deprecated and stateset approaches to state sets
	void convertRawToEncodedSequences() // sets up vector of encoded seqs for leaves of tree; NB. Don't free these vecs
		{
		#include "ascii_map.inc"
		for (int k=0;k<ntax;k++)
		    {
		    SITEINT* pp = new SITEINT[nchar]; // I don't currently delete this as alignments don't go away
		    for (int l=0;l<nchar;l++)
			{
			SITEINT c =raw_seqs[k][l];
			if (seqDataType == NT)
				pp[l] = pll_map_nt[c];
			else if (seqDataType == AA)
				pp[l] = pll_map_aa[c];
			else EXCEPTION("Problem with sequence data type in convertRawToEncodedSequences()");
			}
		    encoded_seqs.push_back(pp);
		    }
				
		}

	void printEncodedAlignment()
		{
		msgout << "Encoded alignment:"<<endl;
		for (int k=0;k<ntax;k++)
		    {
		    SITEINT *p = encoded_seqs[k];
		    for (int l=0;l<nchar;l++)
			msgout << int(p[l]) << " ";
		    msgout << endl;
		    }
		}

//*************************************************************************************************
// Sets up the stateSet 3-vectors at each node efficiently in O(n) time.

// First, sets up a sorted vector having ALL the views possible on an unrooted tree, sorted by the size of 
// the 'viewed' subtree. Thus if the view is (u,v), namely subtree u viewed from v, the size is the
// size of the subtree rooted at u. This allows efficient construction of the 3 state
// sets at each node.

// Note we use number of vertices for 'size' unlike the bucket sort algorithm in the literature, which uses
// number of leaves. This allows us to do this on a rooted tree with a binary root node (and possibly other
// degree two nodes).

// Second, goes through this size sorted list in order of increasing size to set up the 3 views of stateSets at
// each node. This uses the usual intersection/union rules for MP.

// Note about use of views that span the root node: the root node is never included in a view; it is suppressed
// such that the two children of the root form two opposite views, each with the other child as its view_from node.
//*************************************************************************************************
	public: void setupStateSetViews(TREE* tree) // this SHOULD work for any subtree of gene.tree, but careful of the arrays and maps there
		{

		// First, bucket sort all the views

		unsigned int tree_size = tree->nodes.size(); // Here tree size is the number of (all) nodes
		// Following is an array of "buckets" where each subtree size has its own bucket, which is a queue, and thus the array
		// itself represents a size-sort.
		queue<View> tree_size_array[tree_size+1]; // (sub)tree sizes are between 1 and tree_size and we need an extra slot at end


		// Start recursion and, btw, store the view for the root node w.r.t entire tree
		View view(tree->root,nullptr);		
		unsigned int n = subtreeSizeSetup(view,tree_size,tree_size_array);	
		view.size = n;
		tree_size_array[n].push(view);

		// Done setting up size sorted views; now put them in a nicer linear list.
		std::vector<View> size_ordered_views;
		for (int sz = 1; sz <=  tree_size; sz++)
			{
			auto q=tree_size_array[sz];
			if (!q.empty())
				{
				//msgout << "(sub)tree size: " << sz << " : Count = " << q.size() << endl;
				while (!q.empty())
					{
					size_ordered_views.push_back(q.front());
					q.pop();
					}
				}
			}

		//msgout << "Size of ordered vector: " << size_ordered_views.size() << endl;


		// Second, use the views to set up the stateSets at all nodes, including root
		// NB!! leaves will only have a single defined stateSet, the one viewed from its parent.
		// Also note that nodes have their stateset and mpscore vectors initialized to null or 0 when nodes initialized

		for (auto view:size_ordered_views) // reusing 'view' in this scope
			{
			auto node = view.view_node;
			auto node_view_from=view.view_from;

			if (node->isLeaf()) 	// for leaves, initialize a stateSet based on already encoded sequence data. 
				{
				string gene_id = tree->getNodeWName(node); 
				std::map<string,unsigned int>::iterator it;
				it = gene_id_to_ix_h.find(gene_id);
				if (it == gene_id_to_ix_h.end())
					EXCEPTION("A taxon name in the gene tree was not found in the alignment");
				unsigned int ix = it->second;
				view.setStateSet(encoded_seqs[ix]);
				}
			else		// for any view toward an internal node, compute stateSet based on its neighbors
				{
				SITEINT* ss = view.getStateSet();
				if (ss == nullptr) // first time it's seen, may have to instantiate it, but reuse from then
					ss  = new SITEINT[nchar];
				view.setStateSet(ss);
				computeStateSetAt(view);
				}
			}
		}


	private:
	unsigned int subtreeSizeSetup(View v, unsigned int tree_size, queue<View> clade_size_ar[])
		{
		auto node = v.view_node;
		if (node==nullptr) return 0;
		if (node->isLeaf()) return 1;
		unsigned int n_child,n=1;
		NODE* children[2];
		node->getChildrenDirected(v.view_from, children); // children[] are the two relatives of node that are not v.view_from
		for (auto child:children)
			{
			if (child == nullptr) continue;
			View vc(child,node); // interpreted as rooted clade size of child viewed from node
			n_child = subtreeSizeSetup(vc,tree_size,clade_size_ar);
			vc.size = n_child;
			n+=n_child;
			clade_size_ar[n_child].push(vc);
			vc.swap(); // interpreted as rooted clade size of node viewed from child
			vc.size = tree_size-n_child;
			clade_size_ar[tree_size-n_child].push(vc);
			}
		return n;
		}



};

#endif
