/*
 * Copyright (C) 2007 Andre Wehe, Mukul Bansal, Oliver Eulenstein
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef FASTGENEDUP_BIOHEURISTIC_CPLAN_H
#define FASTGENEDUP_BIOHEURISTIC_CPLAN_H

class ConstructionPlan {
public:
                struct data2
                {
                        SpeciesNode *parentptr;
                        SpeciesNode *leftptr;
                        SpeciesNode *rightptr;
                } tempcplan;

		data2 *pnodes;
		int tree_size;

		ConstructionPlan()
		{
		}

                ConstructionPlan(SpeciesTree &speciestree)
                {
                        tree_size = speciestree.nodes.size();
			pnodes = new data2[tree_size];
			construct_plan(speciestree);
                }

		~ConstructionPlan() {
			delete [] pnodes;
		}

                void construct_plan(SpeciesTree &speciestree)
                {
                        for (int k =0 ; k< tree_size ;k++)
                        {
				data2 &d = pnodes[k];
				d.parentptr = speciestree.nodes[k]->parent();
                                d.leftptr = speciestree.nodes[k]->child(0);
                                d.rightptr = speciestree.nodes[k]->child(1);
                        }
                }

                void construct_tree(SpeciesTree &speciestree) const
                {
                        for(int k=0 ; k< tree_size; k++)
                        {
				data2 &d = pnodes[k];
                                speciestree.nodes[k]->parent() = d.parentptr;
                                speciestree.nodes[k]->child(0) = d.leftptr;
                                speciestree.nodes[k]->child(1) = d.rightptr;
				if (d.parentptr == NULL)
					speciestree.root = speciestree.nodes[k];
                        }
                }

		void printplan()

		{
			cout<< "beginning cplan output \n";
			for (int k= 0; k< tree_size;k++)
			{
				cout << pnodes[k].parentptr << " " << pnodes[k].leftptr << " " << pnodes[k].rightptr << endl;
			}
		}
};

class ConstructionKey
{
	public:
		ConstructionPlan *cplan;

		ConstructionKey()
		{
		}

		ConstructionKey(ConstructionPlan *cplan)
		{
			this->cplan = cplan;
		}

/*                bool operator==(const ConstructionKey &key) const
//		{
//		cout << "=operator!"<< endl;
//		return false;
//		}
                {
                        for(int k=0 ; k< cplan.size(); k++)
                        {
                                if(cplan[k].parentptr != plan.cplan[k].parentptr)
                                        return false;
                        }
                        return true;
                } */


                bool operator<(const ConstructionKey &key) const
                {
			#ifdef DEBUG
			if (cplan->tree_size != key.cplan->tree_size) EXCEPTION("ConstructionKey operator< : pnodes of different sizes")
			#endif

//cout <<"In operator! cplan print \n";
//cplan->printplan();
//cout << "key.cplan print";
//key.cplan->printplan();
                        for(int k=0 ; k< cplan->tree_size; k++)
                        {

//				cout << "in operator, cplan prnt = "<<cplan->pnodes[k].parentptr << "key prnt  ="<< key.cplan->pnodes[k].parentptr<< endl;
				if (cplan->pnodes[k].parentptr < key.cplan->pnodes[k].parentptr)
					return true;
                                if (cplan->pnodes[k].parentptr > key.cplan->pnodes[k].parentptr)
				{
//					cout <<"returning false first in < \n";
                                        return false;
				}

 //                               if(cplan->pnodes[k].parentptr == key.cplan->pnodes[k].parentptr)
  //                                      flg++;
                        }
                        return false;
                }
};

#endif
