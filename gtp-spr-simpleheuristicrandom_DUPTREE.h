/*
 * Copyright (C) 2007 Andre Wehe, Mukul Bansal, Oliver Eulenstein
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef FASTGENEDUP_SIMPLEHEURISTICRANDOM_H
#define FASTGENEDUP_SIMPLEHEURISTICRANDOM_H

class SimpleHeuristicRandom :  public Heuristic {
protected:
        double Best_score;
	bool update;
	bool score_flag;
	struct data
	{
		SpeciesNode *BestSubtreeRoot;
		SpeciesNode *BestNewLocation;
	} temp, old;
	vector<data> queue;
	int j, index;
	Format format;
	unsigned int countTotal;

public:
        inline void scoreComputed(SpeciesNode &node)
	{
                const double &genedup = node.score;
                if(genedup < Best_score)
                {
			update = true;
			queue.clear();
			temp.BestSubtreeRoot = speciestree->nodes[j];
                        temp.BestNewLocation = &node;
			queue.push_back(temp);
                        Best_score = genedup;

                        msgout<< "\rCurrent best score: " <<Best_score<<"      ";
			flush(cout);
                }
		else if ((update== true) && (genedup == Best_score))
		{
			temp.BestSubtreeRoot = speciestree->nodes[j];
                        temp.BestNewLocation = &node;
                        queue.push_back(temp);
		}

        }

        SimpleHeuristicRandom(Format format, bool score_flag) : format(format), score_flag(score_flag)
        {
		index=0;
		//srand(time(0));
		Best_score = UINT_MAX;
		update = true;
		temp.BestSubtreeRoot = NULL;
		temp.BestNewLocation = NULL;
		countTotal = 0;
		queue.push_back(temp);
	}

	void run(ostream &os, const ReRoot reroot)
	{
		bool rerooting = (reroot == ALL);
                createLeafMapping();

                int num_nodes, Left_Right;
                num_nodes = speciestree->nodes.size();

//		list<data>:: iterator iter;

		msgout << "Computing..." << endl;
		do {
                        for ( j=0; j< num_nodes;j++)
                        {

                                SpeciesNode *prnt, *sblng;

                                if (speciestree->nodes[j] == speciestree->root)
                                        continue;

                                prnt = speciestree->nodes[j]->parent();
                                if (prnt->child(0) == speciestree->nodes[j])
                                        Left_Right = 0;
                                else Left_Right =1;

                                sblng = prnt->child(1-Left_Right);

// checkConstraintsStructure(speciestree->root,0);
                                speciestree->moveSubtree(speciestree->nodes[j], speciestree->root);

                                computeGeneDuplications(speciestree->nodes[j], rerooting);
                                speciestree->moveSubtree(speciestree->root->child(Left_Right), sblng);
// checkConstraintsStructure(speciestree->root,0);

                        }
			if (update == false) {
				computeBestRooting();
				if (rerooting) break;
				rerooting = true;
				queue.clear();
			} else {
				rerooting = (reroot == ALL);
			}

			update = false;

			if (!queue.empty()) {
				countTotal++;
				index = rand()%queue.size();
				old.BestSubtreeRoot = queue[index].BestSubtreeRoot;
				old.BestNewLocation = queue[index].BestNewLocation;
// cout << "----------------------" << endl;
// speciestree->tree2newick(cout); cout << endl;
				speciestree->moveSubtree(old.BestSubtreeRoot, old.BestNewLocation);
// speciestree->tree2newick(cout); cout << endl;
// cout << endl;
// checkConstraintsStructure(speciestree->root,0);
// cout << "======================" << endl;
			}
		} while(!interruptFlag);

		msgout << endl;
		msgout << "# of tree edit operations: " << countTotal << "rSPR" << endl;
// 		msgout << endl;

		// output score
		msgout << "Gene duplications: " << getCurrentScore() << endl;

		speciestree->tree2newick(os); os << endl;

// 		if (format == NEWICK) {
// 			speciestree->tree2newick(os); os << endl;
// 			if (score_flag) os << "Gene duplications: " << getCurrentScore() << endl;
// 		}
// 		if (format == NEXUS) {
// 			os << "#nexus" << endl;
// 			os << "begin trees;" << endl;
// 			os << "[Gene duplications: " << getCurrentScore() << "]" << endl;
// 			os << "tree speciestree = "; speciestree->tree2newick(os); os << endl;
// 			os << "end;" << endl;
// 		}

	}

// 	int currentScore() {
// 		return Best_score;
// 	}

	double getCurrentScore() {
		return Best_score;
	}
};

#endif
