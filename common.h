/*
 * Copyright (C) 2007 Andre Wehe, Mukul Bansal, Oliver Eulenstein
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

//#define DEBUG

#include <cstdlib>
#include <ctime>
#include <cctype>

#include <iostream>
#include <istream>
#include <ostream>
#include <fstream>
#include <string>
#include <sstream>
#include <map>
#include <set>
#include <vector>
#include <typeinfo>
#include <list>
#include <ctime>
#include <limits.h>

using namespace std;

// checks if object is of a certain data types
#ifdef DEBUG
	#define EXCEPTIONDATATYPE(function,src,dest) \
		if (typeid(src)!=typeid(dest)) { \
			cerr << "ERROR: internal error in function " << function << " - unexpected data type (" \
			<< typeid(src).name() << " != " << typeid(dest).name() << ')' << endl; \
			exit(1); \
		}

	// checks if object is of one of 2 data types
	#define EXCEPTIONDATATYPE2(function,src,dest1,dest2) \
		if ( (typeid(src)!=typeid(dest1)) && (typeid(src)!=typeid(dest2)) ) { \
			cerr << "ERROR: internal error in function " << function << " - unexpected data type (" \
			<< typeid(src).name() << " != " << typeid(dest1).name() \
			<< " and " \
			<< typeid(src).name() << " != " << typeid(dest2).name() << ')' << endl; \
			exit(1); \
		}
#else
	#define EXCEPTIONDATATYPE(function,src,dest)
	#define EXCEPTIONDATATYPE2(function,src,dest1,dest2)
#endif

// report an error
#define EXCEPTION(msg) {\
		cerr << "ERROR: " << msg << endl; \
		exit(1); \
	}

// report a warning
//#define WARNING(msg) {\
//		cerr << "WARNING: " << msg << endl; \
//	}

// report a warning
bool no_warn=false;
#define WARNING(msg) {\
		if (!no_warn) cerr << "WARNING: " << msg << endl; \
	}

// output messages
bool quiet = true;

// CAREFUL! YUCK! Don't  put any code, like i++, after the msgout "command", because with quiet option, it won't execute
#define msgout if (!quiet) cout

// version number
string version = "1.48";

// legal characters --  MJS added . character
//string legalChars4Name = "abcdefghijklmnopqrtsuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_";
string legalChars4Name = ".abcdefghijklmnopqrtsuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_";
#define legalChar4Name(c) (\
		((c>='a') && (c<='z')) || \
		((c>='A') && (c<='Z')) || \
		((c>='0') && (c<='9')) | \
		(c=='_') | \
		(c=='.') \
	)

//MJS: WARNING!!! THIS WILL NOT RECOGNIZE A BRANCH LENGTH WITH EXPONENTIAL NOTATION
#define legalChar4Number(c) (\
		((c>='0') && (c<='9')) || \
		(c=='.') || \
		(c=='-') || \
		(c=='+') \
	)

// encapsulate in '' if nessesary
#define name2newick(name) ((name.find_first_not_of(legalChars4Name) == string::npos) ? name : string('\''+name+'\''))

// interrupt flag
bool interruptFlag = false;
// called in case of an interrupt signel
void interruptFunction(int signal){
	if (interruptFlag) {
		msgout << endl << "User interrupt... stop immediately, bye!" << endl;
		exit(1);
	}
	msgout << endl << "User interrupt... stop after next cycle" << endl;
	interruptFlag = true;
}

// common initializations
void initialization(){
}

enum ReRoot {ALL=1, OPT=0};
enum Format {NEWICK, NEXUS};
enum SeqDataType {NT,AA};
SeqDataType seqDataType; // useful as global
enum CriteriaType {GTP,MP,GTP_ROOTING,GTP_MP};
enum TreeSetType {GENETREES,SPECIESTREE,BOTH,NEITHER};
enum HSearchType {WITH_TIES,WITHOUT_TIES};
void fatal(string msg)
	{
	cout << endl << "Fatal error: " << msg << endl;
	exit(1);
	}

struct Filters
		{
			unsigned int min_leaves{0};
			unsigned int max_leaves{UINT_MAX};
			unsigned int min_spp{0};
			unsigned int max_spp{UINT_MAX};
			unsigned int min_chars{0};
			unsigned int max_chars{UINT_MAX};
			unsigned int max_input_files{UINT_MAX};
		};
	
