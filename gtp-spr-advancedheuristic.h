/*
 * Copyright (C) 2007 Andre Wehe, Mukul Bansal, Oliver Eulenstein
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef FASTGENEDUP_ADVANCEDHEURISTIC_H
#define FASTGENEDUP_ADVANCEDHEURISTIC_H

class AdvancedHeuristic :  public Heuristic {
protected:
	double Best_score;
	bool update;
	bool secondtime;
	bool score_flag;
        SpeciesNode *oldsibling;
	struct data
	{
		SpeciesNode *BestSubtreeRoot;
		SpeciesNode *BestNewLocation;
	} temp, old;
	list<data> queue;
	int j;
	Format format;
	unsigned int countTotal;

public:
        inline void scoreComputed(SpeciesNode &node)
	{
                const double &genedup = node.score;
                if(genedup < Best_score)
                {
			update = true;
			queue.clear(); // Makes clear that as soon as we find better score we clear the queue and start over
			temp.BestSubtreeRoot = speciestree->nodes[j];
                        temp.BestNewLocation = &node;
			queue.push_back(temp);
                        Best_score = genedup;
			secondtime = false;

                        msgout<< "\rCurrent best score: " <<Best_score<<"      ";
			flush(cout);
                }
		else if ((update== true) && (genedup == Best_score))
		{
			temp.BestSubtreeRoot = speciestree->nodes[j];
                        temp.BestNewLocation = &node;
                        queue.push_back(temp);
		}

        }

        AdvancedHeuristic(Format format, bool score_flag) : format(format), score_flag(score_flag)
        {
		Best_score = UINT_MAX;
		update = true;
		secondtime = false;
		temp.BestSubtreeRoot = NULL;
		temp.BestNewLocation = NULL;
		countTotal = 0;
		queue.push_back(temp);
	}

	void run(ostream &os, const ReRoot reroot)
	{

		bool rerooting = (reroot == ALL);
//		if (reroot != ALL) WARNING("this heuristic does only work with --reroot=all");
                createLeafMapping();

                int num_nodes, Left_Right;
                num_nodes = speciestree->nodes.size();

		list<data>:: iterator iter;

		msgout << "Computing...\n";
                while(!interruptFlag)
                {
                        for ( j=0; j< num_nodes;j++)
                        {

                                SpeciesNode *prnt, *sblng;

                                if (speciestree->nodes[j] == speciestree->root)
                                        continue;

                                prnt = speciestree->nodes[j]->parent();
                                if (prnt->child(0) == speciestree->nodes[j])
                                        Left_Right = 0;
                                else Left_Right =1;

                                sblng = prnt->child(1-Left_Right);

                                speciestree->moveSubtree(speciestree->nodes[j], speciestree->root);

                                computeGeneDuplications(speciestree->nodes[j], rerooting);
                                speciestree->moveSubtree(speciestree->root->child(Left_Right), sblng);

                        }
//			cout<<"Test1\n";

//			if(queue.size() == 0)
//				break;

			if (queue.size() == 0) {
				computeBestRooting();
				if (rerooting) break;
				rerooting = true;
				secondtime = true;
				continue;
			} else {
				if(secondtime) break;
				rerooting = (reroot == ALL);
			}


			if(update == false)
			{
//				cout<<"Test 2 \n";
				speciestree->moveSubtree(old.BestSubtreeRoot, oldsibling);
			}
			update = false;


//			cout << endl <<queue.size() << endl;
			iter = queue.begin();
			old.BestSubtreeRoot = iter->BestSubtreeRoot;
			old.BestNewLocation = iter->BestNewLocation;
			queue.erase(iter);
//			queue.pop_back(old);
//			cout <<"HERE!!\n";
			oldsibling = (old.BestSubtreeRoot)->getSibling();

			countTotal++;
                        speciestree->moveSubtree(old.BestSubtreeRoot, old.BestNewLocation);
//			cout<<"Here too!!\n";
//			oldsibling = (old.BestNewLocation)->getSibling();
                }

		msgout << endl;
		msgout << "# of tree edit operations: " << countTotal << "rSPR" << endl;
// 		msgout << endl;

		speciestree->tree2newick(os); os << endl;

// 		// output score
// 		msgout << "Gene duplications: " << getCurrentScore() << endl;
//
// 		if (format == NEWICK) {
// 			speciestree->tree2newick(os); os << endl;
// 			if (score_flag) os << "Gene duplications: " << getCurrentScore() << endl;
// 		}
// 		if (format == NEXUS) {
// 			os << "#nexus" << endl;
// 			os << "begin trees;" << endl;
// 			os << "[Gene duplications: " << getCurrentScore() << "]" << endl;
// 			os << "tree speciestree = "; speciestree->tree2newick(os); os << endl;
// 			os << "end;" << endl;
// 		}

	}

	double getCurrentScore() {
		return Best_score;
	}
};

#endif
