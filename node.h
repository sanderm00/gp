/*
 * Copyright (C) 2007 Andre Wehe, Mukul Bansal, Oliver Eulenstein
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef NODE_H
#define NODE_H
#include <set>
// ------------------------------------------------------------------------------------------------------------------
// this is a general node of a binary tree
// it contains links to the parent and childen nodes

#if 0 // experimental to sidestep curiously recursive template pattern of legacy code...
class TNode 
	{
	public: 
		TNode() {};
	virtual	~TNode() {};
	//virtual TNode *&getAunt() =0; // doesn't work
	};
#endif

template<class T>
//class TreeNode: public TNode {
class TreeNode {


protected:
SITEINT* encoded_sequence=nullptr; // Here we keep a pointer to the encoded seq which is either at a leaf or inferred via algorithm

public:
//uint16_t n_leaves; // num leaves descended from this node in "rooted" versions of trees; used for trees <65k to print unique newick
string lowest_label; // "lowest" leaf label among all descendants in the sense of a lexicographic sort
#if 1
	//MJS added these for encoded sequencesi; note, now used only as a backup algorithm -- replaced by state sets and views
	inline void setEncodedSequence(SITEINT* ptr) // for GeneNodeRooted
		{
		encoded_sequence = ptr;
		}
	inline SITEINT* getEncodedSequence()
		{
		return encoded_sequence;
		}
#endif

	// access parent
	virtual T* &parent() = 0;

	// access child
	virtual T* &child(const int i) = 0;

	// access child
	inline void swapChildren() {
		T *n = child(0);
		child(0) = child(1);
		child(1) = n;
	}

	// return true if this node is a leaf (has no children)
	inline bool isLeaf() {
		return ((child(0) == NULL) && (child(1) == NULL));
	}

	// return true if this node is the root node (has no parent)
	inline bool isRoot() {
		return (parent() == NULL);
	}

	//MJS Reconsider following additions in view of rooted or unrooted trees/nodes.
	//MJS added:
	inline bool isRootOrRootChild()  // used in NNI moves, for example
		{
		if (isRoot()) return true;
		if (parent()->isRoot()) return true;
		return false;
		}

	inline bool grandParentIsRoot() 
		{
		if (isRootOrRootChild()) return false;
		if (parent()->parent()->isRoot()) return true;
		return false;
		}
	// returns the child nodes
	inline void getChildren(T *child[]) {
		child[0] = this->child(0);
		child[1] = this->child(1);
	}

	// returns the sibling node
	// if the node is the root and/or has no parent this BOMBs (mjs)
	inline T *&getSibling() {
		if (parent() == NULL) fatal ("getSibling: node has no sibling");
		return parent()->child(0) == this ? parent()->child(1) : parent()->child(0);
	}

	//MJS added:
	inline T *&getAunt()
		{
		if (isRootOrRootChild()) EXCEPTION("getAunt(): node has no aunt");
		return parent()->getSibling();
		}
	//MJS added:
	inline T *&grandparent()
		{
		if (isRootOrRootChild()) EXCEPTION("grandparent(): node has no grandparent");
		return parent()->parent();
		}
	//MJS: returns the integer, 0 or 1, for which child[] this node is of its parent :: ONLY FOR BINARY TREES!
	inline int whichSibling()
		{
		return parent()->child(0)==this ? 0 : 1;
		}
	//MJS: returns the integer, 0 or 1, for which child[] this node is of its parent :: ONLY FOR BINARY TREES!
	inline bool isLeftChild()
		{
		return parent()->child(0)==this;
		}


	// connect to a node as a child node
// 	inline void connectAsChild(T *&parent) {
// 		this->parent = parent;
// 		if (parent == NULL) return;
// 		for (int i=0; i<2; i++) {
// 			if (parent->child[i] == NULL) {
// 				parent->child[i] = this;
// 				return;
// 			}
// 		}
// 		EXCEPTION("connectAsChild: node " << this << " already has 2 child nodes connected");
// 	}
//
// 	// connect to a node as a certain child node
// 	inline void connectAsChild(T *parent, const int child) {
// 		this->parent() = parent;
// 		parent->child(child) = this;
// 	}

	template<class T2>
	friend ostream & operator << (ostream & os, TreeNode<T2> & m);
};

// output a node into a string stream
template<class T>
ostream & operator << (ostream & os, TreeNode<T> & m) {
	os << "addr:" << &m
	   << ", parent:" << m.parent()
	   << ", children:{" << m.child(0) << ", " << m.child(1) << '}';
	return os;
}

// ------------------------------------------------------------------------------------------------------------------
// node of a rooted binary tree
// MJS: derived from this are subclasses for GeneNodeRooted and NamedGeneNodeRooted
template<class T>
class TreeNodeRooted : public TreeNode<T> {

//unsigned int *encoded_sequence; // Here we keep a pointer to the encoded seq which is either at a leaf or inferred via algorithm
//unsigned int *encoded_sequence; // Here we keep a pointer to the encoded seq which is either at a leaf or inferred via algorithm


protected:
	T* parentlink;
	T* childlink[2];

public:
	// create a node (with a given parent)
	TreeNodeRooted(T *parent = NULL) : parentlink(parent) {
		for (int i = 0; i < 2; i++) childlink[i] = NULL;
	}

	// destroy a node
	virtual ~TreeNodeRooted() {}

	// access parent
	inline T* &parent() {
		return parentlink;
	}

	// access child
	inline T* &child(const int i) {
		return childlink[i];
	}

	template<class T2>
	friend ostream & operator << (ostream & os, TreeNodeRooted<T2> & m);
};

// output a node into a string stream
template<class T>
ostream & operator << (ostream & os, TreeNodeRooted<T> & m) {
	os << "addr:" << &m
	   << ", parent:" << m.parent()
	   << ", children:{" << m.child(0) << ", " << m.child(1) << '}';
	return os;
}

// ------------------------------------------------------------------------------------------------------------------
// node of a --UN--rooted binary tree [MJS correction]
template<class T>
class TreeNodeUnrooted : public TreeNode<T> {
protected:
	T *relative[3];

	SITEINT* stateSet[3]; // state sets and mpScore at this node viewed from one of each of its 3 neighbors
	unsigned int  mpScore[3];

public:
	inline void setStateSet(int ix, SITEINT* ptr) 
		{
		stateSet[ix] = ptr;
		}
	inline void setStateSet(T *view_from, SITEINT* ptr) 
		{
		int ix = getDirectionID(view_from); //which neighbor of node (0,1,2) is the view_from node?
		stateSet[ix] = ptr;
		}
	inline SITEINT* getStateSet(T *view_from) 
		{
		int ix = getDirectionID(view_from); //which neighbor of node (0,1,2) is the view_from node?
		return stateSet[ix];
		}
	inline void setMPScore(int ix, unsigned int score) 
		{
		mpScore[ix] = score;
		}
	inline void setMPScore(T *view_from, unsigned int score) 
		{
		int ix = getDirectionID(view_from); //which neighbor of node (0,1,2) is the view_from node?
		mpScore[ix] = score;
		}
	inline unsigned int getMPScore(T *view_from) 
		{
		int ix = getDirectionID(view_from); //which neighbor of node (0,1,2) is the view_from node?
		return mpScore[ix];
		}

	unsigned short parentno; //MJS: which one of the relatives is the parent?
	#define DEFAULT_ROOT_ID 0 //MJS: see comments immediately below...

	// create a node (with a given parent)
	// a node instantiated by TreeNodeUnrooted() will have its parentno==0 and relative[0]=null; i.e., its ready to be the
	// "root" node until it gets used somewhere else in the tree...
	TreeNodeUnrooted(T *parent = NULL) 
		{
		for (int i = 0; i < 3; i++) 
			{
			relative[i] = NULL;
			stateSet[i] = nullptr; //mjs: added these two
			mpScore[i]  = 0;
			}
		parentno = DEFAULT_ROOT_ID;
		relative[parentno] = parent;
		}

	// destroy a node
	//virtual ~TreeNodeUnrooted() {}
	virtual ~TreeNodeUnrooted() 
		{
		// Both of these targets for deletion below cause core dumps; line 1 => double free; line 2 => ???
		if (this->encoded_sequence != nullptr) 
			if (!this->isLeaf())
				delete[] this->encoded_sequence; 
		// CAREFUL! Leaf dynamic memory points to alignment seqs and should persist till end of program,
		// but internal node memory should go away when tree is deleted.
		// BTW, 'this' makes compiler happy re:parent class protected member 'encoded_sequence'

		for (int i = 0; i < 3; i++) 
			if (stateSet[i] != nullptr) 
				if (!this->isLeaf())
					delete[] stateSet[i]; // this pointer points to an array
		}

	// access parent
	inline T* &parent() {	// return refernce to parent; see you can have it both ways on unrooted trees
		return relative[parentno];	// "root" node will have parentno==0 and relative[0] == null and hence isRoot() will work here
	}

	// assign parent node: MJS i.e., given the parent node, set the parentno index to the corresponding value
	inline void assignParent(T *&parent) {
		for (int i=0; i < (sizeof(relative)/sizeof(T*)); i++) {
			if (relative[i] == parent) {
				parentno = i;
				return;
			}
		}
		EXCEPTION("parent not found in assignParent" << endl);
	}

	// access child: either of the two relatives not the parent
	inline T* &child(const int i) { // i.e., return reference to child[i]
		return i < parentno ? relative[i] : relative[1+i];
	}

	// access an edge
	inline T* &direction(const int i) { // i.e., return reference to adjacent node corresponding to relative[i]
		return relative[i];
	}

	// return the id of an edge (unrooted): i.e., an int in [0,1,2]  for the index of relative[] where *node is found
	inline int getDirectionID(const T *node) {
		for (int i=0; i < (sizeof(relative)/sizeof(T*)); i++) {
			if (relative[i] == node) return i;
		}
		EXCEPTION("node not found in getDirectionID" << endl);
	}

	// return the direction of an edge (unrooted) ; MJS: returns reference to relative[i] for this node
	inline T *&getDirection(const T *node) {
		for (int i=0; i < (sizeof(relative)/sizeof(T*)); i++) {
			if (relative[i] == node) return relative[i];
		}
		EXCEPTION("node not found in getDirection" << endl);
	}

	// returns the child nodes (all nodes that are not parent)
	inline void getChildrenDirected(T *&parent, T *child[]) {
		T **r = &relative[0];
		for (int i=0; i < (sizeof(relative)/sizeof(T*)-1); i++) {
			if (relative[i] == parent) r++;
			child[i] = *r++;
		}
	}

	template<class T2>
	friend ostream & operator << (ostream & os, TreeNodeUnrooted<T2> & m);
};

// output a node into a string stream
template<class T>
ostream & operator << (ostream & os, TreeNodeUnrooted<T> & m) {
	os << "addr:" << &m
	   << ", parent:" << m.parent()
	   << ", children:{" << m.child(0) << ", " << m.child(1) << '}'
	   << ", relatives:{" << m.relative[0]  << ", " << m.relative[1] << ", " << m.relative[2]<< "}";
	return os;
}

// ------------------------------------------------------------------------------------------------------------------
// contains the ID (name) of a node
// nodes with the same name are automatically grouped together
#define TWO_NAMES_PER_NODE 

#ifdef TWO_NAMES_PER_NODE

/* MJS Version for two names possibly at each named node.
	Each node is allowed a 'w_name' (whole name) and a 'name' (species name).
	The legacy code of DupTree just uses 'name' everywhere in both gene trees and species trees
	to permit mapping between them. Here we allow some nuance mainly so we can have names 
	within alignments that are specific to genes, but also encode what species they were sampled from.
	That encoding of a gene name in an alignment looks like this:

		spname__identifier	acgtacacttt....

	where there is a magic double underscore delimiter between spname and identifier and the order is 
	respected. In this instance, when this two-token name is parsed, the variable 'name' is set to 'spname'
	and the variable 'w_name' is set to 'spname__identifier'.

	If there is no '__' delimiter in the string, both variables are set to the entire string.

	With this in mind, we require the following format for input of gene alignments, gene trees and
	species trees:

		Labels in gene alignments and gene trees must follow the two-token format, with the first token
		corresponding to species names in a species tree. The w_name must be unique within the alignment
		and gene tree to permit a one-to-one mapping within the gene alignment/tree. We use a hash here
		so  in theory there could be duplicates across the whole data set, but when parsing each Gene class
		an exception will be thrown if there are duplicate names within that Gene.

		[This last means that if there is only one gene per species in an alignment, you can drop the "__" delim and
		use the species name in the alignment file and tree--but NOT if there are multiple copies of any gene.. ]

		Labels in the species tree must either
			(i) not use the delimiter, in which case the whole string is the species name and name=w_name, or
			(ii) use the delimiter, and its ordering convention, in which case 'name' has the relevant species name 
				and w_name includes everything (might be useful for voucher data or some such).
*/
class NodeID {
public:
	static map<string,int> namelist;
	///NB! Don't change this name because DupTree accesses this public variable in its tree mapping (naughty).
	map<string,int>::iterator name;	// representation of 'name' is as an iterator into hash
	static map<string,int> w_namelist;
	map<string,int>::iterator w_name;


	// create a node (with a given parent)
	NodeID(const string &id) 
		{
		string whole_name(id);
		string species_name = parseGetSpName(id);
//msgout << "NodeID initializer entered with following names:" << endl;
//msgout << "species_name=" << species_name << " and whole_name= " << whole_name << endl;

		// Add the species name to namelist and to this instance as variable 'name'
		name = namelist.find(species_name);	// if we find the name in the static array, we store it as an iterator
		if (name == namelist.end())	// name doesn't exist 
			{ 
//msgout << "species_name not found...adding it"<<endl;
			namelist[species_name] = 0;
			name = namelist.find(species_name); // else insert it and use THAT iterator value
			}
//msgout << "name: first and second was = " <<name->first << ":" << name->second << endl;
		name->second++;
//msgout << "name: first and second now = " <<name->first << ":" << name->second << endl;

		// Add the whole name to wnamelist and to this instance as variable 'w_name'
		w_name = w_namelist.find(whole_name);	// if we find the name in the static array, we store it as an iterator
		if (w_name == w_namelist.end())	// name doesn't exist 
			{ 
//msgout << "whole_name not found...adding it"<<endl;
			w_namelist[whole_name] = 0;
			w_name = w_namelist.find(whole_name); // else insert it and use THAT iterator value
			}
//msgout << "second was = " << w_name->second << endl;
		w_name->second++;
//msgout << "second now = " << w_name->second << endl;

//msgout << "At end of NodeID constructor, print out namelist and w_namelist!" << endl;
//for (map<string,int>::iterator itr=NodeID::namelist.begin();itr!=NodeID::namelist.end();itr++)
//	cout << "Species namelist entry:" << itr->first << ' ' << itr->second << endl;
//msgout << "\nAll done!" << endl;
//for (map<string,int>::iterator witr=NodeID::w_namelist.begin();witr!=NodeID::w_namelist.end();witr++)
//	cout << "Species w_namelist entry:" << witr->first << ' ' << witr->second << endl;
//msgout << "\nAll done!" << endl;


		}

	// destroy a node
	virtual ~NodeID() {
		name->second--;
		if (name->second == 0) namelist.erase(name);
	//MJS:
		w_name->second--;
		if (w_name->second == 0) w_namelist.erase(w_name);
	}

	// return the name
	inline const string &getName() {	// and to retrieve it, just get the key value of the stored iterator
		return name->first;
	}
	// return the whole_name
	inline const string &getWholeName() {	// and to retrieve it, just get the key value of the stored iterator
		return w_name->first;
	}

	private: string parseGetSpName(const string &id) // parse the node name and return the part of it corresponding to species name
		{
		string delim = "__"; // double underscore!
		std::size_t found = id.find(delim);
		if (found!=std::string::npos)
			return id.substr(0,found);
		else
			return string(id);		// no parsing was done due to lack of delimiter; just return the whole string unparsed
		}


	friend ostream & operator << (ostream & os, NodeID & m);
};
#else
// else just one name per node (legacy code)
class NodeID {
public:
	static map<string,int> namelist;
	map<string,int>::iterator name;	// representation of 'name' is as an iterator into hash

	// create a node (with a given parent)
	//MJS: 
	NodeID(const string &id) {
		name = namelist.find(id);	// if we find the name in the static array, we store it as an iterator
		if (name == namelist.end()) { // name doesn't exist
			namelist[id] = 0;
			name = namelist.find(id); // else insert it and use THAT iterator value
		}
		name->second++;
	}

	// destroy a node
	virtual ~NodeID() {
		name->second--;
		if (name->second == 0) namelist.erase(name);
	}

	// return the name
	inline const string &getName() {	// and to retrieve it, just get the key value of the stored iterator
		return name->first;
	}

	friend ostream & operator << (ostream & os, NodeID & m);
};
#endif
// Following are 'definitions' required by linker when there is static variable 
// See e.g. https://isocpp.org/wiki/faq/ctors#explicit-define-static-data-mems
// These must occur once in a single compilation unit, and below these imply default initializations. 
// And they must occur AFTER their declarations above...
map<string,int> NodeID::namelist; 
map<string,int> NodeID::w_namelist; 

// output NodeID into a string stream
ostream & operator << (ostream & os, NodeID & m) {
	os << "name:" << m.name->first;
	return os;
}


#endif
