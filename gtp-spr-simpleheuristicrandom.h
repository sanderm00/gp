/*
 * Copyright (C) 2007 Andre Wehe, Mukul Bansal, Oliver Eulenstein
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef FASTGENEDUP_SIMPLEHEURISTICRANDOM_H
#define FASTGENEDUP_SIMPLEHEURISTICRANDOM_H


// MJS Modified below in various ways to do just a single round of species tree SPR search and put all resulting species
// trees in a priority queue based on duplication score.
// At the moment it still moves species tree to the best one found in this neighborhood. Get fancier shortly.

class SimpleHeuristicRandom :  public Heuristic {

// started down this path in this file, but pursued it in the..PQ.h file
protected:struct sprMoveTree // MJS: called 'data' in legacy code
		{
		SpeciesNode *BestSubtreeRoot;
		SpeciesNode *BestNewLocation;
		double	gene_dup_score; //MJS 
		} temp, old,temp_pqueue;

// Comparator for priority queue for duplication scores, so lowest score has highest priority
public: class compareTreeScore
	{
	public:
	bool operator()(struct sprMoveTree& t1, struct sprMoveTree& t2) 
		{
		if (t1.gene_dup_score > t2.gene_dup_score) 
			return true;
		else
			return false;
		}
	};

protected:
        double Best_score;
	bool update;
	bool score_flag;
	vector<sprMoveTree> queue;

	std::priority_queue<sprMoveTree, vector<sprMoveTree>, compareTreeScore> pqueue;

	int j, index;
	Format format;
	unsigned int countTotal;

public:
        inline void scoreComputed(SpeciesNode &node)
	{
                const double &genedup = node.score;
			//MJS added for priority queue
			temp_pqueue.BestSubtreeRoot = speciestree->nodes[j];
                        temp_pqueue.BestNewLocation = &node;
                        temp_pqueue.gene_dup_score = genedup;
			pqueue.push(temp_pqueue);
		//	...push it...
                if(genedup < Best_score)
                {
			update = true; // mark this discovery of a better tree by setting update flag
			queue.clear();
			temp.BestSubtreeRoot = speciestree->nodes[j];
                        temp.BestNewLocation = &node;
			queue.push_back(temp);
                        Best_score = genedup;

                        //msgout<< "\rCurrent best score: " <<Best_score<<"      ";
			flush(cout);
                }
		else if ((update== true) && (genedup == Best_score)) // queue up trees that are equally good
		{
			temp.BestSubtreeRoot = speciestree->nodes[j];
                        temp.BestNewLocation = &node;
                        queue.push_back(temp);
		}

        }

        SimpleHeuristicRandom(Format format, bool score_flag) : format(format), score_flag(score_flag)
        {
		index=0;
		//srand(time(0));
		Best_score = UINT_MAX;
		update = true;
		temp.BestSubtreeRoot = NULL;
		temp.BestNewLocation = NULL;
		countTotal = 0;
		queue.push_back(temp); // queue is initialized to have one tree in it, though with NULL parameters and UINT_MAX score,
					// so this will be discarded always during scoring of the first real tree
					// Note. Legacy code will throw exception when trying to 'moveSubtree' with null pointers, unlike
					// my spr.rearrange() code for genetrees...
	}


	// Do rSPR search on species tree but stop after scanning the neighborhood. 
	// Do not continue to try to improve after first round
#if 1
	void run(ostream &os, const ReRoot reroot)
	{
		bool rerooting = (reroot == ALL); // 'rerooting' means whether gene trees will be rerooted at every step of iteration below...
                createLeafMapping();

                int num_nodes, Left_Right;
		int num_spr_searches=0;
                num_nodes = speciestree->nodes.size();
//msgout << *this << endl; ...to print all the gene trees do this
                for ( j=0; j< num_nodes;j++) // notice j is a class var
                        {

                                SpeciesNode *prnt, *sblng;

                                if (speciestree->nodes[j] == speciestree->root)
                                        continue;

                                prnt = speciestree->nodes[j]->parent();
                                if (prnt->child(0) == speciestree->nodes[j])
                                        Left_Right = 0;
                                else Left_Right =1;

                                sblng = prnt->child(1-Left_Right);

                                speciestree->moveSubtree(speciestree->nodes[j], speciestree->root);

                                computeGeneDuplications(speciestree->nodes[j], rerooting);	
					//MJS: the score for this subtree when it's made sib to root
					// rerooting controls whether gene trees will be rerooted at every iteration...
                                speciestree->moveSubtree(speciestree->root->child(Left_Right), sblng); //MJS: Move back to original location
                        }

			// Legacy code: will change this in relation to priority queue
			// MJS: Pick one sp tree at random from queue and move to there prior to next round
			//msgout << *this << endl; ..to print the gene trees, do this
		if (!queue.empty()) {
				countTotal++;
				index = rand()%queue.size();
				old.BestSubtreeRoot = queue[index].BestSubtreeRoot;
				old.BestNewLocation = queue[index].BestNewLocation;
				speciestree->moveSubtree(old.BestSubtreeRoot, old.BestNewLocation);
			}

//		msgout << endl;
		//msgout << "# of tree edit operations: " << countTotal << "rSPR" << endl;
// 		msgout << endl;

		// output score
		//msgout << "Gene duplications: " << getCurrentScore() << endl;

		speciestree->tree2newick(os); os << endl; // don't delete, MJS


msgout << "WARNING: Running single round sp tree spr search only" << endl;
msgout << "queue size = " << queue.size() << endl;
msgout << "pqueue size = " << pqueue.size() << endl;
msgout << "sp tree: " << speciestree<< endl;
#if 0
while (!pqueue.empty())
	{
	sprMoveTree tt = pqueue.top();
	msgout << tt.gene_dup_score << " ";
	pqueue.pop();
	}
msgout << endl;
#endif

	} // end runOneRound

#else
	void run(ostream &os, const ReRoot reroot)
	{
		bool rerooting = (reroot == ALL); // 'rerooting' means whether gene trees will be rerooted at every step of iteration below...
                createLeafMapping();

                int num_nodes, Left_Right;
		int num_spr_searches=0;
                num_nodes = speciestree->nodes.size();

//		list<data>:: iterator iter;

		//msgout << "Computing...\n";
		do {

/*	Looping over the global rSPR neighborhood seems to be spread across multiple files:

for ( j=0; j< num_nodes;j++) { ... the outer "global" loop is here below in this file
...
computeGeneDuplications(SpeciesNode *subtree, bool reroot = true) { ... this and the next is in gtp-spr-heuristic.h
	....
	forEachCallScoreComputed(subtree, sibling); // MJS. Looks like this is where we loop over INNER loop of rSPR neighborhood
	}
}
*/
                        for ( j=0; j< num_nodes;j++)
                        {

                                SpeciesNode *prnt, *sblng;

                                if (speciestree->nodes[j] == speciestree->root)
                                        continue;

                                prnt = speciestree->nodes[j]->parent();
                                if (prnt->child(0) == speciestree->nodes[j])
                                        Left_Right = 0;
                                else Left_Right =1;

                                sblng = prnt->child(1-Left_Right);

// checkConstraintsStructure(speciestree->root,0);
                                speciestree->moveSubtree(speciestree->nodes[j], speciestree->root);

                                computeGeneDuplications(speciestree->nodes[j], rerooting);	
					//MJS: the score for this subtree when it's made sib to root
					// rerooting controls whether gene trees will be rerooted at every iteration...
                                speciestree->moveSubtree(speciestree->root->child(Left_Right), sblng); //MJS: Move back to original location
// checkConstraintsStructure(speciestree->root,0);

                        }
			++num_spr_searches;
msgout << "Num spr searches completed while doing species tree search = " << num_spr_searches << endl;

		// MJS: The following seems to implement an increasingly desperate search for improvement when no improvement has been found
			if (update == false) { //MJS: this is 'true' after first round even if no improvement, see right below...
				computeBestRooting(); // reroot after rearrangements done
				if (rerooting) break; // will do final termination now, since we've exhausted possibilities of rerooting
				rerooting = true; // so now let's force reroot searching on all rearrangement and see if this improves things
				queue.clear(); // This seems to ensure that on this second or later round, when no improvement was found,
						// we just don't bother to move to any of the possibly longer list of enqueued equally good
						// trees. Instead, we just leave species tree at its location from the previous round,
						// which is fine, since there has been no improvement in the current round.
			} else {
				rerooting = (reroot == ALL);
			}

			update = false; // MJS: This only gets set AFTER we have gone through one round above,
					// so in previous test of 'update', 'update' will be true even if no improvement...

			// MJS: Pick one sp tree at random from queue and move to there prior to next round
			if (!queue.empty()) {
				countTotal++;
				index = rand()%queue.size();
				old.BestSubtreeRoot = queue[index].BestSubtreeRoot;
				old.BestNewLocation = queue[index].BestNewLocation;
// cout << "----------------------" << endl;
// speciestree->tree2newick(cout); cout << endl;
				speciestree->moveSubtree(old.BestSubtreeRoot, old.BestNewLocation);
// speciestree->tree2newick(cout); cout << endl;
// cout << endl;
// checkConstraintsStructure(speciestree->root,0);
// cout << "======================" << endl;
			}
		} while(!interruptFlag);

		msgout << endl;
		//msgout << "# of tree edit operations: " << countTotal << "rSPR" << endl;
// 		msgout << endl;

		// output score
		//msgout << "Gene duplications: " << getCurrentScore() << endl;

		speciestree->tree2newick(os); os << endl; // don't delete, MJS

// 		if (format == NEWICK) {
// 			speciestree->tree2newick(os); os << endl;
// 			if (score_flag) os << "Gene duplications: " << getCurrentScore() << endl;
// 		}
// 		if (format == NEXUS) {
// 			os << "#nexus" << endl;
// 			os << "begin trees;" << endl;
// 			os << "[Gene duplications: " << getCurrentScore() << "]" << endl;
// 			os << "tree speciestree = "; speciestree->tree2newick(os); os << endl;
// 			os << "end;" << endl;
// 		}

	} // end run
#endif

// 	int currentScore() {
// 		return Best_score;
// 	}

	double getCurrentScore() {
		return Best_score;
	}
	//unsigned int getCountTotal() { return countTotal;}  // this is the number of SPR moves done...hard to extract it through class heirarchy
};

#endif
