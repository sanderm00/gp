/*
 * Copyright (C) 2007 Andre Wehe, Mukul Bansal, Oliver Eulenstein
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef FASTGENEDUP_BIOHEURISTIC_H
#define FASTGENEDUP_BIOHEURISTIC_H

class BioHeuristic :  public Heuristic {
protected:
        double Best_score;
	bool update, secondrun;
	bool score_flag;
        SpeciesNode *oldsibling;
	struct data
	{
		SpeciesNode *BestSubtreeRoot;
		SpeciesNode *BestNewLocation;
	} temp, old;
	list<data> queue;
	int j, queue3index, queue3position, queue3size, max_os;

        map<ConstructionKey, ConstructionPlan*> queue2;
	ConstructionPlan** queue3;
	Format format;

public:
        inline void scoreComputed(SpeciesNode &node)
	{
                const double &genedup = node.score;
                if(genedup < Best_score)
                {
			update = true;
			secondrun = false;
			queue.clear();
			temp.BestSubtreeRoot = speciestree->nodes[j];
                        temp.BestNewLocation = &node;
			queue.push_back(temp);
                        Best_score = genedup;

                        msgout<< "\rCurrent best score: " <<Best_score<<"      ";
			flush(cout);
                }
		else if (genedup == Best_score)
		{
			if(!secondrun)
			{
				temp.BestSubtreeRoot = speciestree->nodes[j];
                        	temp.BestNewLocation = &node;
                        	queue.push_back(temp);
			}
		}

        }

        BioHeuristic(int queuesize, int max_output, Format format, bool score_flag) : format(format), score_flag(score_flag)
        {
		Best_score = UINT_MAX;
		update = true;
		secondrun = false;
		temp.BestSubtreeRoot = NULL;
		temp.BestNewLocation = NULL;
		queue.push_back(temp);
		queue3index=0;
		queue3position = 0;
		queue3size = queuesize;
		max_os = max_output;
		queue3 = new ConstructionPlan*[queuesize];
	}

	~BioHeuristic()
	{
		delete [] queue3;
	}

	void run(ostream &os, const ReRoot reroot)
	{

		bool rerooting = (reroot == ALL);
//		if (reroot != ALL) WARNING("this heuristic does only work with --reroot=all");
                createLeafMapping();

                int num_nodes, Left_Right;
                num_nodes = speciestree->nodes.size();

		list<data>:: iterator iter;
		map<ConstructionKey, ConstructionPlan*>:: iterator iter2;

		msgout << "Computing...\n";
                while(!interruptFlag)
                {
                        for ( j=0; j< num_nodes;j++)
                        {

                                SpeciesNode *prnt, *sblng;

                                if (speciestree->nodes[j] == speciestree->root)
                                        continue;
                                prnt = speciestree->nodes[j]->parent();
                                if (prnt->child(0) == speciestree->nodes[j])
                                        Left_Right = 0;
                                else Left_Right =1;
                                sblng = prnt->child(1-Left_Right);
                                speciestree->moveSubtree(speciestree->nodes[j], speciestree->root);

                                computeGeneDuplications(speciestree->nodes[j], rerooting);
                                speciestree->moveSubtree(speciestree->root->child(Left_Right), sblng);

                        }

				if(update == true)
				{
					for (map<ConstructionKey, ConstructionPlan*>::iterator itr=queue2.begin(); itr!=queue2.end(); itr++) {
						delete itr->second;
					}
					queue2.clear();
					queue3index=0;
					queue3position = 0;
					iter = queue.begin();
					while(iter!=queue.end())
					{


						oldsibling = iter->BestSubtreeRoot->getSibling();
						speciestree->moveSubtree(iter->BestSubtreeRoot, iter->BestNewLocation);


						if(queue3index < queue3size)
						{

							int oldsize1 = queue2.size();
							ConstructionPlan *cplan = new ConstructionPlan(*speciestree);

							queue2[ConstructionKey(cplan)] = cplan;
							if(oldsize1 < queue2.size())
							{

								queue3[queue3index] = cplan;
								queue3index++;
							}
						}
						speciestree->moveSubtree(iter->BestSubtreeRoot, oldsibling);
						iter++;
					}

					queue.clear();
				}


				if(update== false)
				{
					if ((queue2.size() == 0) || (Best_score == 0))
						break;

					iter = queue.begin();
					while(iter!=queue.end())
					{
						oldsibling = iter->BestSubtreeRoot->getSibling();
                                               	speciestree->moveSubtree(iter->BestSubtreeRoot, iter->BestNewLocation);
						if(queue3index < queue3size )
						{
							int oldsize1 = queue2.size();
							ConstructionPlan *cplan = new ConstructionPlan(*speciestree);

					       		queue2[ConstructionKey(cplan)] = cplan;
							if(oldsize1< queue2.size())
							{

								queue3[queue3index] = cplan;
								queue3index++;
							}
						}
                                               	speciestree->moveSubtree(iter->BestSubtreeRoot, oldsibling);
					       	iter++;
					}
					queue.clear();
				}

//cout<<"queue2 size at end" << queue2.size()<< " " << queue3position << endl;

				if(queue3position == queue3index)
				{
					computeBestRooting();
					if(rerooting)
						break;
					if(secondrun)
						break;
					rerooting = true;
					secondrun = true;
					queue3position = 0;
				}
				if(secondrun)
					rerooting = true;
				else rerooting = (reroot == ALL);

				queue3[queue3position]->construct_tree(*speciestree);
				queue3position++;

				update = false;
		}

//              speciestree->tree2newick(cout);
/*		for (map<ConstructionKey, ConstructionPlan*>::iterator itr=queue2.begin(); itr!=queue2.end(); itr++) {
			delete itr->second;
		}*/
		msgout << endl;

		// output score
		msgout << "Gene duplications: " << getCurrentScore() << endl;

		if(max_os == -1)
			max_os = queue3size;
		if (max_os > queue3index)
			max_os = queue3index;
		for(int k= 0; k< max_os; k++)
		{
			queue3[k]->construct_tree(*speciestree);
			speciestree->tree2newick(os); os << endl;
		}
// 		for(int k= 0; k< max_os; k++) {
// 			queue3[k]->construct_tree(*speciestree);
// 			speciestree->tree2newick(os); os << endl;
// 		}

/*		if (format == NEWICK) {
			if(max_os == -1)
				max_os = queue3size;
			if (max_os > queue3index)
				max_os = queue3index;
			for(int k= 0; k< max_os; k++)
			{
				queue3[k]->construct_tree(*speciestree);
				speciestree->tree2newick(os); os << endl;
			}
			if (score_flag) os << "Gene duplications: " << getCurrentScore() << endl;
		}
		if (format == NEXUS) {
			os << "#nexus" << endl;
			os << "begin trees;" << endl;
			os << "[Gene duplications: " << getCurrentScore() << "]" << endl;
			if(max_os == -1)
				max_os = queue3size;
			if (max_os > queue3index)
				max_os = queue3index;
			for(int k= 0; k< max_os; k++)
			{
				queue3[k]->construct_tree(*speciestree);
				os << "tree speciestree" << k << " = ";
				speciestree->tree2newick(os); os << endl;
			}
			os << "end;" << endl;
		}*/

	}

	double getCurrentScore() {
		return Best_score;
	}
};

#endif
