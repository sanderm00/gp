CPP      =g++
#FLAGS    =-O2 -g
#FLAGS    = -g --std=c++14
#FLAGS    = -g --std=c++17
#FLAGS    = -g
#FLAGS    =  -O3  -mavx 
# previous is for avx 
#FLAGS    =   -pthread -O3 
#FLAGS    =   -g -fpermissive -pthread  
#FLAGS    =   -g  -pthread  -O3 -DPTHREADS
FLAGS    =  -pthread  -O3 -DPTHREADS 
#FLAGS    =   -g  -pthread  -DPTHREADS
#FLAGS    =   -g  -pthread  
# this optimization includes vectorization apparently; seems to give me a 4x speedup
all :
	$(CPP) $(FLAGS) mpduptree.cc -o gp
boot :
	$(CPP) $(FLAGS) -DBOOT=1 mpduptree.cc -o gp_boot
mac :
	$(CPP) $(FLAGS) --std=c++14 mpduptree.cc -o gp
