// Checksum based on Stack Overflow comment 
#include <iostream>
#include <inttypes.h>

uint64_t fletcher64(uint32_t * data, uint32_t count)
{
    uint64_t sum1 = 0;
    uint64_t sum2 = 0;
    for (uint32_t index = 0; index < count; ++index) 
    {
        sum1 = (sum1 + data[index]) % UINT32_MAX;
        sum2 = (sum2 + sum1) % UINT32_MAX;
    }
    // build final checksum
    return (sum2 << 32) | sum1;
}
