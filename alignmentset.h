#ifndef ALIGNMENTSET_H
#define ALIGNMENTSET_H

//  Class to encapsulate all the gene alignments in a data set.
//  Each alignment is stored in dynamic memory and alignments[] contains pointers to these, in order of input.

class AlignmentSet {	

private:

public:


	vector<string> filenames;
	vector<Alignment*> alignments;	

	// Constructor reads alignments from files, stores the raw and encoded sequences as vectors, reads starting trees if present
	// and stores them as newick strings. 
	// Does not do any further processing that involves actual tree structures.
	AlignmentSet(Input &input, Filters filters) // set max_input_files=0 to read all files; else just the first num_files of them
		{
		readFileFilenames(input);
		//readAlignmentSet(max_input_files);
		readAlignmentSet(filters);
		initializeBestEverScores();
		};

	// Initialize "best-ever" scores across gene set, stored for each gene, prior to doing any computing
	// Because these are alignment-specific, this should be called prior to ALL searches, not within 
	void initializeBestEverScores()
		{
		for (auto alignment : alignments) // for each gene, do a separate gene tree heuristic search
			{
			alignment->bestever_mp_score = UINT_MAX; 
			alignment->bestever_gtp_score = UINT_MAX;
			}
		return ;
		}
	void initializeAllBootWeights(std::mt19937 generator)
		{
		for (auto alignment : alignments) // for each gene, do a separate gene tree heuristic search
			{
			alignment->initializeBootWeights(generator);
			}
		return ;
		}

	inline unsigned int nAlignments(){return alignments.size();}

	// Read the initial list of all filenames of alignments, but these will possibly be filtered once alignments are read
	inline void readFileFilenames(Input &input)
		{
		char c;
		string fn;
		while (1)
			{
			fn = input.getStringMJS();
			if (fn == "") break;
			filenames.push_back(fn);
			}
		msgout << filenames.size() << " alignment filenames read" << endl;
		}


	void readAlignmentSet(Filters filters)
		{
		string fn;
		msgout << "********************************************************************************" << endl;
		msgout << "Reading alignment set" << endl;
		unsigned int num_filenames;
		unsigned int max_input_files = filters.max_input_files;

		std::vector<string> filenames_filtered;
		for (int i=0;i<filenames.size();i++)
			{
			if (alignments.size() >= max_input_files)
				break;
			fn = filenames[i];
			istream *alin;
			ifstream alinfile;
			alinfile.open(fn.c_str());
			if (!alinfile) EXCEPTION("Alignment file " << fn << " not found");
			alin = &alinfile;
			Input alinput(alin);

			//*** Do the work of constructing each alignment
			auto *alignment = new Alignment(alinput);
			//***

// Good place to filter out e.g. large or small alignments; 
// e.g. check alignment sizes before push_back
			if (alignment->satisfies(filters))
				{
				alignments.push_back(alignment);
				filenames_filtered.push_back(fn);
				//msgout << "............................................" << endl;
				//msgout << "Keeping alignment " << i << " [file: '" << fn << "']" << endl;
				}
			else
				delete alignment;
			}
		//cout << "Alignments passing all filters: " << alignments.size() << endl;
		if (filenames_filtered.size()<filenames.size())
			filenames = filenames_filtered;		// prune this vector down to size based on filters to keep filenames in register with alignments
		msgout << alignments.size()<<" alignments stored"<<endl;
		if (alignments.size() == 0)
			EXCEPTION("No alignments were found that satisfied filters. Exiting...");
		}


	// Reports details about data set initial conditions, now called from genetree-heuristic...
	void report()
		{
		msgout << "********************************************************************************" << endl;
		msgout << "Alignments input summary" << endl;
		msgout << "#\tFile\t\tnGenes\tnSpp\tStarting gene tree" << endl;
		for (int i=0;i<alignments.size();i++)
			{
			string fn = filenames[i];
			fn.resize(15,' '); // pad names out to 10 spaces
			string newick = alignments[i]->starting_tree.newick();
			if (newick.length() > 40) newick = newick.substr(0,40) + "...";
			msgout << i << "\t" << fn << "\t" << alignments[i]->ntax << "\t" << alignments[i]->nspp << "\t" << newick << endl;	
			}
		}

};



#endif
