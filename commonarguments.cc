/*
 * Copyright (C) 2007 Andre Wehe, Mukul Bansal, Oliver Eulenstein
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

	// MJS: 
	// Mix of some GP parameters and no longer used DupTree parameters kept here just in case.
	// 

	// random seed given
	unsigned int randomseed = unsigned(time(NULL)) % 0xFFFF;
	{
		const Argument *arg = Argument::find("--seed");
		if (arg != NULL) arg->convert(randomseed);
		srand(randomseed);
	}

	// choose heuristic
	int heuristic_type;
	{
		const Argument *arg1 = Argument::find("-e");
		const Argument *arg2 = Argument::find("--heuristic");
		const Argument *arg = arg1 != NULL ? arg1 : arg2;
		if (arg == NULL) heuristic_type = 1;
		else arg->convert(heuristic_type);
	}

	// queue size
	int queue;
	{
		const Argument *arg = Argument::find("--queue");
		if (arg == NULL) queue = 500;
		else {
			if (heuristic_type != 5) WARNING("argument --queue is ignored for heuristic " << heuristic_type);
			arg->convert(queue);
		}
		if (queue <= 0) EXCEPTION("--queue value has to be greater than 0");
	}

	// score
	bool score_flag = true;
// 	{
// 		const Argument *arg = Argument::find("--score");
// 		if (arg != NULL) {
// 			score_flag = true;
// 			if (arg->hasValue()) EXCEPTION("--score doesn't need a value");
// 		}
// 	}

	// report
	bool report_flag = true;
// 	{
// 		const Argument *arg = Argument::find("--report");
// 		if (arg != NULL) {
// 			score_flag = true;
// 			if (arg->hasValue()) EXCEPTION("--report doesn't need a value");
// 		}
// 	}

	// max output trees
	int max_trees;
	{
		const Argument *arg1 = Argument::find("-t");
		const Argument *arg2 = Argument::find("--trees");
		const Argument *arg = arg1 != NULL ? arg1 : arg2;
		if (arg == NULL) max_trees = 1;
		else {
			if (heuristic_type != 5) WARNING("argument --trees is ignored for heuristic " << heuristic_type);
			string str;
			arg->convert(str);
			if (str == "all") max_trees = -1;
			else {
				arg->convert(max_trees);
				if (max_trees <= 0) EXCEPTION("-t, --trees value has to be greater than 0");
			}
		}
	}

	// max output trees
	ReRoot reroot;
	{
		const Argument *arg1 = Argument::find("-r");
		const Argument *arg2 = Argument::find("--reroot");
		const Argument *arg = arg1 != NULL ? arg1 : arg2;
		if (arg == NULL) reroot = OPT;
		else {
			string str;
			arg->convert(str);
			if (str == "all") reroot = ALL;
			else
			if (str == "opt") reroot = OPT;
			else EXCEPTION("--reroot has a wrong argument");
		}
	}

	// output format
//	SeqDataType seqDataType; NB. I will use this as a global declared in common.h for now. Ugh but easy
	{
		const Argument *arg = Argument::find("--seqtype");
		if (arg == NULL) seqDataType = NT;
		else {
			string str;
			arg->convert(str);
			if (str == "nt") seqDataType = NT;
			else
			if (str == "aa") seqDataType = AA;
			else EXCEPTION("--seqtype has a wrong argument");
		}
	}

	enum TreeSetType treeset_type;
	{
		const Argument *arg = Argument::find("--ts_type");
		if (arg == NULL) treeset_type = NEITHER;
		else {
			string str;
			arg->convert(str);
			if (str == "both") treeset_type = BOTH;
			else
			if (str == "neither") treeset_type = NEITHER;
			else
			if (str == "speciestree") treeset_type = SPECIESTREE;
			else
			if (str == "genetrees") treeset_type = GENETREES;
			else EXCEPTION("--ts_type has a wrong argument");
		}
	}
	enum HSearchType hsearch_type;
	{
		const Argument *arg = Argument::find("--hsearch_type");
		if (arg == NULL) hsearch_type = WITH_TIES;
		else {
			string str;
			arg->convert(str);
			if (str == "with_ties") hsearch_type = WITH_TIES;
			else
			if (str == "without_ties") hsearch_type = WITHOUT_TIES;
			else EXCEPTION("--hsearch_type has a wrong argument");
		}
	}




	// output format
	Format oformat;
	{
		const Argument *arg = Argument::find("--oformat");
		if (arg == NULL) oformat = NEWICK;
		else {
			string str;
			arg->convert(str);
			if (str == "newick") oformat = NEWICK;
			else
			if (str == "nexus") oformat = NEXUS;
			else EXCEPTION("--oformat has a wrong argument");
		}
	}

	// output input gene trees
	bool genetreesFlag = true;
	{
		const Argument *arg = Argument::find("--nogenetree");
		if (arg != NULL) {
			genetreesFlag = false;
			if (arg->hasValue()) EXCEPTION("--nogenetree doesn't need a value");
		}
	}

	// choose tree generator
	int generator;
	{
		const Argument *arg1 = Argument::find("-g");
		const Argument *arg2 = Argument::find("--generator");
		const Argument *arg = arg1 != NULL ? arg1 : arg2;
		// MJS: if (arg == NULL) generator = 1;
		if (arg == NULL) generator = 8; // MJS change default to my option for now
		else arg->convert(generator);
	}

	// puts the leaf add into fast mode
	bool fastFlag = false;
	{
		const Argument *arg1 = Argument::find("-f");
		const Argument *arg2 = Argument::find("--fast");
		const Argument *arg = arg1 != NULL ? arg1 : arg2;
		if (arg != NULL) {
			fastFlag = true;
			if (arg->hasValue()) EXCEPTION("--fast doesn't need a value");
		}
	}
	if (fastFlag) {
		if (generator != 1) WARNING("--fast does not apply to generator " << generator);
	}

	// constraints input
	istream *constraints_in;
	ifstream constraints_infile;
	{
		const Argument *arg = Argument::find("--constraints");
		if (arg == NULL) constraints_in = NULL;
		else {
			string filename;
			arg->convert(filename);
			msgout << "Constraints file: " << filename << endl;
			constraints_infile.open(filename.c_str());
			if (!constraints_infile) EXCEPTION("constraints file " << filename << " not found");
			constraints_in = &constraints_infile;
			if ((generator != 1) && (generator != 2)) WARNING("--constraints does not apply to generator " << generator);
		}
	}


	
void printHelp(string prog); 
void printPQInfo(string title ,std::priority_queue<TSASDataPersistent> &pq);

bool helpFlag = false;
{
	const Argument *arg1 = Argument::find("-h");
	const Argument *arg2 = Argument::find("--help");
	const Argument *arg = arg1 != NULL ? arg1 : arg2;
	if (arg != NULL) {
		helpFlag = true;
		if (arg->hasValue()) EXCEPTION("--help doesn't need a value");
	}
}
// if requesting help, print help but do nothing else...
if (helpFlag) {
	printHelp(argsv[0]);
	return 0;
	}

quiet = false;
{
	const Argument *arg1 = Argument::find("-q");
	const Argument *arg2 = Argument::find("--quiet");
	const Argument *arg = arg1 != NULL ? arg1 : arg2;
	if (arg != NULL) {
		quiet = true;
		if (arg->hasValue()) EXCEPTION("--quiet doesn't need a value");
	}
}

bool versionFlag = false;
{
	const Argument *arg1 = Argument::find("-v");
	const Argument *arg2 = Argument::find("--version");
	const Argument *arg = arg1 != NULL ? arg1 : arg2;
	if (arg != NULL) {
		versionFlag = true;
		if (arg->hasValue()) EXCEPTION("--version doesn't need a value");
	}
}

// bool helpFlag = (Argument::find("-h") != NULL) || (Argument::find("--help") != NULL);
// quiet = (Argument::find("-q") != NULL) || (Argument::find("--quiet") != NULL);
// bool versionFlag = (Argument::find("-v") != NULL) || (Argument::find("--version") != NULL);

// choose input MJS: This serves as any "first file" needed for input; then we will need special second files too
// careful to not encapsulate these blocks; have to care for the streams etc and their scope. blech
istream *in;
ifstream infile;
{
	const Argument *arg1 = Argument::find("-i");
	const Argument *arg2 = Argument::find("--input");
	const Argument *arg = arg1 != NULL ? arg1 : arg2;
	if (arg == NULL) in = &cin;
	else {
		string filename;
		arg->convert(filename);
		msgout << "Input file: " << filename << endl;
		infile.open(filename.c_str());
		if (!infile) EXCEPTION("input file " << filename << " not found");
		in = &infile;
	}
}
Input input(in);


// choose output
ostream *out;
//ofstream *out;
ofstream outfile;
{
	const Argument *arg1 = Argument::find("-o");
	const Argument *arg2 = Argument::find("--output");
	const Argument *arg = arg1 != NULL ? arg1 : arg2;
	if (arg == NULL) out = &cout; // replacing this with the folllowing exception to I can replace declartion of ostream out with ofstream out
//	if (arg == NULL)
//		{WARNING("arg was null in out command--consult commonarguments.h code");}
	else {
		string filename;
		arg->convert(filename);
		msgout << "Output file: " << filename << endl;
		outfile.open(filename.c_str());
		if (!outfile) EXCEPTION("Output file " << filename << " not created");
		out = &outfile;
	}
}
// output version number
if (versionFlag) {
	cout << version << endl;
	return 0;
}

//**************************************************************************************************************************
//**************************************************************************************************************************
//**************************************************************************************************************************

//  		GP OPTIONS


// Alignment filtering
	unsigned int filter_min_spp;  // exclude any alignments with fewer than this many species
	{
		const Argument *arg = Argument::find("--filter_min_spp");
		if (arg == NULL) filter_min_spp=0; // this default is interpreted to mean use all input files
		else 
			arg->convert(filter_min_spp);
	}
	unsigned int filter_max_leaves;  // exclude any alignments with more than this number of leaves
	{
		const Argument *arg = Argument::find("--filter_max_leaves");
		if (arg == NULL) filter_max_leaves=UINT_MAX; // this default is interpreted to mean use all input files
		else 
			arg->convert(filter_max_leaves);
	}
	unsigned int filter_max_input_files;  // use only the first this many input files
	{
		const Argument *arg = Argument::find("--filter_max_input_files");
		if (arg == NULL) filter_max_input_files=UINT_MAX; // this default is interpreted to mean use all input files
		else 
			arg->convert(filter_max_input_files);
	}


	unsigned int n_threads;  // use only the first this many input files
	{
		const Argument *arg = Argument::find("--n_threads");
		if (arg == NULL) n_threads=1; // this default is interpreted to mean use all input files
		else 
			arg->convert(n_threads);
	}
	unsigned int nni_radius;  // use only the first this many input files
	{
		const Argument *arg = Argument::find("--nni_radius");
		if (arg == NULL) nni_radius=UINT_MAX; // this default is interpreted to mean use all input files
		else 
			arg->convert(nni_radius);
	}
	unsigned int sp_max_iter;  // use only the first this many input files
	{
		const Argument *arg = Argument::find("--sp_max_iter");
		if (arg == NULL) sp_max_iter=5; // this default is interpreted to mean use all input files
		else 
			arg->convert(sp_max_iter);
	}
	unsigned int multrees;  // 
	{
		const Argument *arg = Argument::find("--multrees");
		if (arg == NULL) multrees=1; // this default is interpreted to mean use all input files
		else 
			arg->convert(multrees);
	}
	unsigned int h_multrees;  // 
	{
		const Argument *arg = Argument::find("--h_multrees");
		if (arg == NULL) h_multrees=5; // this default is interpreted to mean use all input files
		else 
			arg->convert(h_multrees);
	}
	//unsigned int gtp_weight;
	double gtp_weight;
	{
		const Argument *arg = Argument::find("--gtp_weight");
		if (arg == NULL) gtp_weight=1;
		else 
			arg->convert(gtp_weight);
	}
	//unsigned int mp_weight;
	double mp_weight;
	{
		const Argument *arg = Argument::find("--mp_weight");
		if (arg == NULL) mp_weight=1;
		else 
			arg->convert(mp_weight);
	}
	unsigned int gtpmp_max_iter;
	{
		const Argument *arg = Argument::find("--gtpmp_max_iter");
		if (arg == NULL) gtpmp_max_iter=5;
		else 
			arg->convert(gtpmp_max_iter);
	}
	unsigned int mp_max_iter;
	{
		const Argument *arg = Argument::find("--mp_max_iter");
		if (arg == NULL) mp_max_iter=5;
		else 
			arg->convert(mp_max_iter);
	}

// search heuristics specification file
	//std::ifstream file("modeltest");

ifstream algorithm_file;
bool algorithm_file_read = false;
	const Argument *arg1 = Argument::find("-a");
	const Argument *arg2 = Argument::find("--algorithm");
	const Argument *arg = arg1 != NULL ? arg1 : arg2;
{
	string filename,algo_fname;
	if (arg == NULL)
		{
		// Added 3/24. Check for direct input of algorithm file if all else fails (i.e., anywhere on file system, not just ALGORITHMS dir)
		const Argument *arg3 = Argument::find("--algorithm_infile");
		if (arg3 != NULL)
			arg3->convert(filename);
		else
			filename = prog_dir + "/ALGORITHMS/default"; // None of three ways to input were provided, so look for this default algo file

		}
	else
		{
		arg->convert(algo_fname);
		filename = prog_dir + "/ALGORITHMS/" + algo_fname;
		}
	algorithm_file.open(filename.c_str());
	if (!algorithm_file) EXCEPTION("input algorithm file " << filename << " not found");
	algorithm_file_read=true;
}

// TreeSet input for gp
// ** Fixed bug in next three blocks, where the default condition arg==NULL was assigning a file handle to "in", which was wrong variable
// Note this default behavior downstream is a pretty opaque anyway, but has to be handled right to avoid improper initializations with
// e.g., compiler option O3 on OS X. The whole command line interface needs a face lift.
istream *in2;
ifstream treeset_infile;
bool treeset_infile_open = false;
{
	const Argument *arg1 = Argument::find("-s");
	const Argument *arg2 = Argument::find("--treeset_file");
	const Argument *arg = arg1 != NULL ? arg1 : arg2;
	if (arg == NULL) in2 = &cin;
	else {
		string filename;
		arg->convert(filename);
		msgout << "Input treeset file: " << filename << endl;
		treeset_infile.open(filename.c_str());
		if (!treeset_infile) EXCEPTION("input treeset file " << filename << " not found");
		treeset_infile_open=true;
		in2 = &treeset_infile;
	}
}
Input input_st(in2);

// filefile input
istream *ffin;
ifstream ffinfile;

{
	const Argument *arg1 = Argument::find("-ff");
	const Argument *arg2 = Argument::find("--filefile");
	const Argument *arg = arg1 != NULL ? arg1 : arg2;
	if (arg == NULL) ffin = &cin;
	else {
		string filefilename;
		arg->convert(filefilename);
		msgout << "Input filefile: " << filefilename << endl;
		ffinfile.open(filefilename.c_str());
		if (!ffinfile) EXCEPTION("input file " << filefilename << " not found");
		ffin = &ffinfile;
	}
}

Input ffinput(ffin);

// DEPRECATED?  gene input
istream *alin;
ifstream alinfile;

{
	const Argument *arg1 = Argument::find("-gf");
	const Argument *arg2 = Argument::find("--gene_file");
	const Argument *arg = arg1 != NULL ? arg1 : arg2;
	if (arg == NULL) alin = &cin;
	else {
		string alfilename;
		arg->convert(alfilename);
		msgout << "gene file: " << alfilename << endl;
		alinfile.open(alfilename.c_str());
		if (!alinfile) EXCEPTION("gene file " << alfilename << " not found");
		alin = &alinfile;
	}
}
Input alinput(alin);

// Handle existing log files; overwrite if requested, or check if they exist and bail if so, with some advice
// This little block should be placed here before the outstreams are initialized below it. Else, they will be present 
// with 0 size, but fool the test.
bool overwrite = false; 
{
	const Argument *arg = Argument::find("--overwrite");
	if (arg != NULL) {
		overwrite = true;
		if (arg->hasValue()) EXCEPTION("--overwrite doesn't need a value");
	}
}

bool outlog_bypass=false; 

// out streams for logs of various kinds
ofstream *outgi,*outgo,*outscores,*outsp,*outgs,*outc,*outch,*outpq,*outff,*outbest,*outbest2;
ofstream out_genes_in,out_genes_out,out_scores,out_sp,outgscores,outcom,outcheck,outfinalpq,out_files_used,out_best,out_best2;
string filename_prefix,filename_genes_in,filename_genes_out,filename_scores,filename_sptree,filename_genescores,filename_command,filename_check,filename_pq,filename_outff,filename_outbest,filename_outbest2;

//Added 3/24 to permit output of a single sp tree solution bypassing logs
{
	const Argument *arg = Argument::find("--o_sptree");
	if (arg != NULL)
		{
		arg->convert(filename_outbest2);
		out_best2.open(filename_outbest2.c_str());
		if (!out_best2) EXCEPTION("Bypass output file " << filename_outbest2 << " not created");
		outbest2 = &out_best2;
		outlog_bypass=true;
		}
}

// Get one with the default log files setup
{
	const Argument *arg = Argument::find("--prefix");
	if (arg == NULL)
		filename_prefix = "GP";
	else 
		arg->convert(filename_prefix);

	if (!overwrite)
		{
		string existing_fn = filename_prefix + ".log";
		ifstream xfile(existing_fn);
		if (xfile)
			fatal("Files with prefix '" + filename_prefix + "' already exist! Delete them, or use --prefix or --overwrite options to continue.");
		}



	filename_outbest = filename_prefix + ".best";
	out_best.open(filename_outbest.c_str());
	if (!out_best) EXCEPTION("Output file " << filename_outbest << " not created");
	outbest = &out_best;

	filename_outff = filename_prefix + ".files_used";
	out_files_used.open(filename_outff.c_str());
	if (!out_files_used) EXCEPTION("Output file " << filename_outff << " not created");
	outff = &out_files_used;

	filename_pq = filename_prefix + ".final_pq";
	outfinalpq.open(filename_pq.c_str());
	if (!outfinalpq) EXCEPTION("Output file " << filename_pq << " not created");
	outpq = &outfinalpq;

	//filename_check = filename_prefix + ".checksum";
	//outcheck.open(filename_check.c_str());
	//if (!outcheck) EXCEPTION("Output file " << filename_check << " not created");
	//outch = &outcheck;


	filename_command = filename_prefix + ".command";
	outcom.open(filename_command.c_str());
	if (!outcom) EXCEPTION("Output file " << filename_command << " not created");
	outc = &outcom;

	//filename_genescores = filename_prefix + ".genescores";
	//outgscores.open(filename_genescores.c_str());
//	msgout << "Writing summary table on all alignments "<<filename_genes_in<<endl;
	//if (!outgscores) EXCEPTION("Output file " << filename_genescores << " not created");
	//outgs = &outgscores;

	filename_genes_in = filename_prefix + ".treeset_input";
	out_genes_in.open(filename_genes_in.c_str());
//	msgout << "Writing input genes to file "<<filename_genes_in<<endl;
	if (!out_genes_in) EXCEPTION("Output file " << filename_genes_in << " not created");
	outgi = &out_genes_in;

	filename_genes_out = filename_prefix + ".treeset_output";
	out_genes_out.open(filename_genes_out.c_str());
//	msgout << "Writing output genes to file "<<filename_genes_out<<endl;
	if (!out_genes_out) EXCEPTION("Output file " << filename_genes_out << " not created");
	outgo = &out_genes_out;
	
	//filename_sptree = filename_prefix + ".species_trees";
	//out_sp.open(filename_sptree.c_str());
//	msgout << "Writing output species trees tp file "<<filename_sptree<<endl;
	//if (!out_sp) EXCEPTION("Output file " << filename_sptree << " not created");
	//outsp = &out_sp;
	
	// Using variable names for 'scores' but now a 'log' file
	filename_scores = filename_prefix + ".log";
	out_scores.open(filename_scores.c_str());
//	msgout << "Writing scores to file "<<filename_scores<<endl;
	if (!out_scores) EXCEPTION("Output file " << filename_scores << " not created");
	outscores = &out_scores;
	
}

bool disable_logs = false; 
{
	const Argument *arg = Argument::find("--disable_logs");
	if (arg != NULL) {
		disable_logs = true;
		if (arg->hasValue()) EXCEPTION("--disable_logs doesn't need a value");
	}
}
bool scoreTreeSetAndExit = false; 
{
	const Argument *arg = Argument::find("--score");
	if (arg != NULL) {
		scoreTreeSetAndExit = true;
		if (arg->hasValue()) EXCEPTION("--score doesn't need a value");
	}
}

bool  no_sptree_search = false;
{
	const Argument *arg = Argument::find("--no_sptree_search");
	if (arg != NULL) {
		no_sptree_search = true;
		if (arg->hasValue()) EXCEPTION("--no_sptree_search doesn't need a value");
	}
}
bool pre_mp_flag = false;
{
	const Argument *arg = Argument::find("--pre_mp");
	if (arg != NULL) {
		pre_mp_flag = true;
		if (arg->hasValue()) EXCEPTION("--pre_mp doesn't need a value");
	}
}
bool root_flag = false;
{
	const Argument *arg = Argument::find("--root");
	if (arg != NULL) {
		root_flag = true;
		if (arg->hasValue()) EXCEPTION("--root doesn't need a value");
	}
}


{
	const Argument *arg = Argument::find("--no_warn");
	if (arg != NULL) {
		no_warn = true;  //default set in common.h
		if (arg->hasValue()) EXCEPTION("--no_warn doesn't need a value");
	}
}
bool queue_flag = false;
{
	const Argument *arg = Argument::find("--queues");
	if (arg != NULL) {
		queue_flag = true;
		if (arg->hasValue()) EXCEPTION("--queue doesn't need a value");
	}
}
bool build_flag = false;
{
	const Argument *arg = Argument::find("--build");
	if (arg != NULL) {
		build_flag = true;
		if (arg->hasValue()) EXCEPTION("--build doesn't need a value");
	}
}
bool boot_flag = false;
{
	const Argument *arg = Argument::find("--boot");
	if (arg != NULL) {
		boot_flag = true;
		if (arg->hasValue()) EXCEPTION("--boot option doesn't need a value");
		if (!BOOT) EXCEPTION("Default compilation of program does not allow bootstrapping. Recompile with 'make boot' and run as 'gp_boot ...'");
	}
}
//..........................................................

//..........................................................
// complain about unknown arguments
vector<Argument*> unused = Argument::unusedArgs();
for (vector<Argument*>::iterator itr = unused.begin(); itr != unused.end(); itr++) {
	EXCEPTION("Unknown argument " << (*itr)->key << endl << "Try " << argsv[0] << " --help for a list of arguments");
}
