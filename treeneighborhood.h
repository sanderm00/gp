#ifndef TREENEIGHBORHOOD
#define TREENEIGHBORHOOD
#include <assert.h>
#include "moves.h"
#include "savedtrees.h"

// GENERAL INTERFACE PHILOSOPHY:

// Neighborhoods are generated based on a "reference tree".  
// The neighborhood DOES NOT include the reference tree.

//To avoid copying trees, iterators and generators of the
// neighborhood may rewrite the reference tree in place, but they should always return it to its original state when done!

// Template stuff is not working at the moment...
// template<class NODE, class LEAFNODE>
// NB! Consequently I've set this thing up just for rooted gene trees for now; TO DO

class TreeNeighborhoodIterator; // forward decl for iterator needed
class rSPR_TreeNeighborhoodIterator; // forward decl for iterator needed
class rNNI_TreeNeighborhoodIterator; // forward decl for iterator needed

#if ROOTED
typedef GeneTreeRooted TREE;
typedef GeneNodeRooted NODE;
#else
typedef GeneTreeUnrooted TREE;
typedef GeneNodeUnrooted NODE;
#endif



//*********************************************************************************************
// General tree neighborhood base class 
//*********************************************************************************************

class TreeNeighborhood
	{
	public:
		int n_trees=0;
		int n_trees_examined=0;
		TREE *start_tree;
		vector<NODE*> source_nodes; // valid nodes for use in generating neighborhood

		TreeNeighborhood(TREE *s):start_tree{s} { }
	};


//*********************************************************************************************
// Tree neighborhood base class that uses a sequence or set of NNIs to describe it.
// That set of NNIs is stored as a vector in NNI_Sequence. 
//*********************************************************************************************

class NNISeqTreeNeighborhood:public NNI_Sequence
	{
	protected:
	TREE *start_tree;

	public:
	NNISeqTreeNeighborhood(TREE* st): start_tree{st} {};

	virtual SPR toSPR(NNI nni) = 0;  // This only does something sensible in the rSPR as NNI neighborhood -- see below
	};



//*********************************************************************************************
// rooted global SPR neighborhood, comprising a set of local SPR neighborhoods, each of which
// correspond to detaching a subtree from the tree and reattaching in every possible place.
// Stores a set of all nodes of the tree EXCEPT the root. This means we don't have to check each
// of these to see if they are the root.
//*********************************************************************************************

class rSPR_GlobalTreeNeighborhood:public TreeNeighborhood
	{
	public:
	rSPR_GlobalTreeNeighborhood(TREE *s):TreeNeighborhood{s}
		{
		setupSourceNodes();
		n_trees = source_nodes.size(); // NB! This is the number of subtrees to move...not the real size of neighborhood
		}
	void setupSourceNodes()  // initializes this array needed for NNI neighborhoods at least 
		{
		for (int i=0;i<start_tree->nodes.size();i++)
			{
			NODE *node=start_tree->nodes[i];		
			if (node->isRoot()) continue;	// skip root
			source_nodes.push_back(node);
			}
		}
	rSPR_TreeNeighborhoodIterator begin(NODE*); 
	rSPR_TreeNeighborhoodIterator end();
	};



//*********************************************************************************************
// Rooted local SPR neighborhood using NNI_Sequence as its data structure
//*********************************************************************************************
/*

	Local rooted SPR neighborhood found by
	removing a particular 'source' subtree and reattaching
	in all other places. Distinguished from global neighborhood that is found
	by considering all source subtrees.
	
	Done here using an NNI_Sequence paradigm.
		
	Only NNI objects with a 'forward' direction can produce a new SPR tree in the neighborhood.
	'Backward' moves revisit an earlier tree in the sequence of trees visited during the
	recursive setup of the NNI_Sequence. 

	An "iterator" is implemented by using the intrinsic vector's iterator in NNI_Sequence.

	TO DO: It is possible to do a direct iterator that sidesteps the setup of the vector, but tricky.

	TO DO: It is also possible to do the computing of scores, etc., in real time recursively.


                    *
                   / \
                  /   \
                 /     \
                /       \
               * STEM    \
              / \  	  \ 						. 
             /   \   	   \						.
         x  /     \   	    						.
           /       \   							.
          /         \   						.
         *  CROWN    * aunt(A)
        / \          							. 
    y  /   \								.
      /     \								.
     /       \ 								.
    * sib(A)  * A ['subtr']]
   / \
  /   \
 B     C=niece2(A)


Let leaf A be the subtree that will be moved to each edge of the tree in an SPR neighborhood.
It can be involved in NNI moves that involve rotations around either edge x or y.
NNIs involving rotating A around edge y concern the crown clade (formally smallest clade having A
and its sib).
NNIs involving rotating A around edge x concern the stem clade (formally, smallest clade having A 
but NOT A's aunt).
For each of these edges, we can engineer two NNI moves that place A either elsewhere within the crown
or stem groups. Then we can recursively move elsewere in the tree.
There are separate recursion routines for each, and the iteration begins by calling each in turn.
The crown method uses A's nieces as the source node for the NNI move; the stem method uses either
A or its sib as source.
*/





class rSPR_LocalTreeNeighborhoodUsingNNI: public NNISeqTreeNeighborhood
	{
	private:
		// Limit the size of the NNI neighborhood to at most this many nnis from ref tree
		// Note that this is "approximate" relative to SPR moves, given the structure of the NNI sequence built below.
		unsigned int distance_cutoff = UINT_MAX;
	protected: 
		NODE *subtree; // this subtree will be moved and re-attached to every internal edge
	public:
		rSPR_LocalTreeNeighborhoodUsingNNI(TREE *gt, NODE *s,unsigned int radius=UINT_MAX):NNISeqTreeNeighborhood{gt}, subtree{s},distance_cutoff{radius}
			{
			setupNNIs();
			}
		NODE* getSubroot() {return subtree;}
		
		// This neighborhood generates local SPR moves using a sequence of NNIs, so this function
		// provides a CAREFUL mapping between the tree generated by the NNI sequence based on using 
		// subtree to start with, and moving via NNIs (up to the current nni), 
		// and the SINGLE SPR move from subtree to target_node.
		// Basic question is for a given tree and subtree, which NNI moves are equivalent to the SPR
		// moves moveSubtree(subtree, target_node). The answer is that there are four such moves, and
		// so we have to consider four cases below.
		// Because we use the same moveSubtree function to implement both NNIs and SPRs, we are assured
		// that the internal node structure is stable and only the subtree and its parent node move around.
		// This lets us use a hash with the nodes of the trees as keys, which might otherwise be sensitive 
		// to willy nilly modifications of the tree.	

		SPR  toSPR(NNI nni)
			{
			NODE* nni_source = nni.getSource();
			if (nni_source == nullptr) // i.e., this means the NNI did not move from ref tree, stays put ; 
				return SPR(start_tree,subtree,subtree->getSibling()); // moving subtree to be sister of its sib is same as no move

			NODE* target_node;
			if (nni_source == subtree) 
				target_node = subtree->grandparent();
			else if (nni_source == subtree->getSibling()) 
				target_node = subtree->getAunt();
			else 
				{
				auto niece0 = subtree->getSibling()->child(0);
				auto niece1 = subtree->getSibling()->child(1);
				if (nni_source == niece0)
					target_node = niece1;
				else if (nni_source == niece1)
					target_node = niece0;
				else
					target_node = nullptr;
				}
			SPR spr(start_tree,subtree,target_node);
			return spr;	
			}

#if 0
	SPRTree toSPRTree(NNI nni)
		{
			auto spr = toSPR(nni);
			SPRTree spr_tree = SPRTree(spr.tree, spr.subroot, spr.target_node);
			return spr_tree;
		}
#endif
			
	private:

		// Setup the sequence of NNIs that generates all local rSPR moves, recursively.

		void setupNNIs()
			{
			// Assumes source is NOT the root on entry
			nnis.clear();		// Need to do this between multiple calls
			processCrownClade(0); // Works on the smallest crown clade containing subtr (using neighboring edge that is sister to this edge)
			processStemClade(0);  // then on the smallest stem clade containing subtr (using neighboring edge in direction of root)
			}
		void processCrownClade(int dist)
			{
			NODE* niece;
			if (subtree->getSibling()->isLeaf()) // termination criterion
				return;
if (dist > distance_cutoff) return;
			for (int i=0;i<2;i++) // both children
				{
				niece = subtree->getSibling()->child(i);
				NNI nni_forward(start_tree,niece);
				NNI nni_backward= nni_forward.reversed();
				nnis.push_back(nni_forward);
				nni_forward.rearrange();
				processCrownClade(dist+1);
				nnis.push_back(nni_backward);
				nni_backward.rearrange();
				}
//msgout << "NNI distance = "<< dist << endl;
			return;
			}	

		void processStemClade(int dist)
			{
			if (subtree->isRootOrRootChild()) 
				return;
if (dist > distance_cutoff) return;
			NNI nni_forward(start_tree,subtree);
			NNI nni_backward= nni_forward.reversed();
			nnis.push_back(nni_forward);
			nni_forward.rearrange();
			processStemClade(dist+1);
			nnis.push_back(nni_backward);
			nni_backward.rearrange();

			NODE* sib  = subtree->getSibling();
			NNI nni2_forward(start_tree,sib);
			NNI nni2_backward= nni2_forward.reversed();
			nnis.push_back(nni2_forward);
			nni2_forward.rearrange();
			processCrownClade(dist+1);
			nnis.push_back(nni2_backward);
			nni2_backward.rearrange();
//msgout << "NNI distance = "<< dist << endl;
			return;
			}
	};

//*********************************************************************************************
// Rooted local SPR neighborhood described directly from SPR moves from the original tree (not a 'sequence' of moves).

// ** Given: a start_tree and a node corresponding to the root of a subtree,
// ** Finds: all the valid target_nodes within start_tree to which an SPR move can be made with that subtree.
// ** DOES NOT INCLUDE THE ORIGINAL STARTING TREE IN THE NEIGHBORHOOD.

// A target_node is the node that will become the sibling of the subtree after the move.

//  Neighborhood is represented by a vector of SPR moves.

//*********************************************************************************************
class rSPR_LocalTreeNeighborhoodUsingSPR: public SPR_Sequence // a vector of forward SPR moves any of which can be done and then undone
	{
	protected: 
		TREE *start_tree;
		NODE *subtree; // this subtree will be moved and re-attached to every internal edge
	public:
		rSPR_LocalTreeNeighborhoodUsingSPR(TREE *gt, NODE *s):start_tree{gt}, subtree{s}
			{
			//setupSPRsWrong();
			if (subtree->isRoot()) 
				EXCEPTION("Subtree cannot be root in setting up rSPR neighborhood");
			sprs.clear();
			setupSPRs(start_tree->root);
			}
		inline NODE* getSubroot() {return subtree;}
	private:
		void setupSPRs(NODE* t_node)
			{
			if (t_node == subtree) return; // do not include nodes in the subtree or the subtree's edge either, 
			if (t_node != subtree->parent() && t_node != subtree->getSibling()) 
				// skip using the parent node of the subtree as well, or the sib; both cases are homomorphic to starting tree
				// Note that in TreeCut(), this parent node will be deleted until uncut()
				{
				SPR spr(start_tree,subtree,t_node);
				sprs.push_back(spr);
				}
			if (!t_node->isLeaf())
				{
				setupSPRs(t_node->child(0));	
				setupSPRs(t_node->child(1));	
				}
			}
		void setupSPRsWrong()
			{
			sprs.clear();
			for (auto target_node : start_tree->nodes)
				{
				//if (target_node == subtree) continue; // start tree has subroot already the sib of target_node, so don't do it
				SPR spr(start_tree,subtree,target_node);
				sprs.push_back(spr);
				}
			}
	};

//*********************************************************************************************
// Neighborhood of all rerootings of a tree, implemented using an NNI_sequence. 
// Note that there is no simple SPR version of this, unlike the rSPR_LocalNeighborhoodUsingNNI()
// class. The rerootings are a sequence of NNIs but not a SPR neighborhood of some start_tree.
// This means converting to 'equivalent SPR' is meaningless here despite having the base class 
// function for this.

// This neighborhood does not include the start tree.
// It also does not include the redundant rerooting that occurs on the two edges descended from the
// root. That is, only one of these is included.

//*********************************************************************************************

class RerootNeighborhoodUsingNNI: public NNISeqTreeNeighborhood
	{
	public:
		RerootNeighborhoodUsingNNI(TREE *gt):NNISeqTreeNeighborhood{gt}
			{ setupNNIs(); }

		SPR   toSPR(NNI nni)
			{
			SPR spr;
			return spr;	
			}
	private:
		void setupNNIs()
			{
			nnis.clear();		// Need to do this between multiple calls
			processNode(start_tree->root); 
			}
		void processNode(NODE* node)
			{
			if (node->grandParentIsRoot()) // only do NNI in this case, so rotate around root node
				{
				NNI nni_forward(start_tree,node);
				NNI nni_backward= nni_forward.reversed();
				nnis.push_back(nni_forward);
				nni_forward.rearrange();
//msgout << "REROOT TREE: "<< start_tree->tree2newick()<<endl;
				processNode(node); // follow the recursion now after we've moved node into its new position in case, 
							// because its children will now be candidates to rearrange...
				nnis.push_back(nni_backward);
				nni_backward.rearrange();
				return;
				} 
			else
				if (node->isLeaf()) return;
			else
				for (int i=0;i<2;i++) // both children
					processNode(node->child(i));
			return;
			}	

};
#endif
