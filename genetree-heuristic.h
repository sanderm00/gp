#ifndef GENETREEHEURISTIC
#define GENETREEHEURISTIC

#include "tsas.h"
#include "treeneighborhood.h"
#include "savedtrees.h"
#include "treecut.h"
#include <limits>


// Algorithms to find optimal gene tree for a given species tree and gene alignment, or for a set of genetrees/alignments.
// Currently ties are resolved arbitrarily, not randomly (nor is a queue kept of equally optimal solns).

// Input: 'gene' object (alignment and gene tree) and species tree, or entire geneset of gene objects.
// Output: 'optimal' gene tree(s), score, etc., using some kind of rearrangement, search, over gene trees...

// This is a base class and keeps a reference to the gene_set and gene we are working on.
// When it is destroyed, nothing happens to those objects or the nice things they contain, like the species tree and gene trees...
// (in other words, I tried inheriting from GeneSet/TreeSet--bad idea w.r.t. destructors).

// TO DO:
// Implement the "site cutoff" idea which stops optimizing site scores once the running total exceeds the score of current best tree.
// But note this is only useful for the pure MP search.

struct gtp_data
	{
		NODE* spr_target_node;
		unsigned int gtp_score;
	};


class GeneTreeHeuristic
	{

#if 0   // now forcing this to always be unrooted gene trees
typedef GeneTreeRooted TREE;
typedef GeneNodeRooted NODE;
#else
typedef GeneTreeUnrooted TREE;
typedef GeneNodeUnrooted NODE;
#endif


	private:
	NNISeqTree bestTree;
	//GeneSet &gene_set;  

	unsigned int nni_radius; //currently only used for gtpmp searches
	unsigned int best_gtp_score=UINT_MAX;
	public:
	TSASData  &gene_set;  // Genes are now 'alignments', but lets keep names like 'gene' and 'gene_set', which is what was used before. Also make this public to help with global pthreads worker function

	// The concept here is that this object will represent one complete heuristic search only.
	GeneTreeHeuristic(TSASData &gs,unsigned int nr): gene_set{gs},nni_radius{nr}
		{
		gene_set.createLeafMapping();	// Always do this upon construction, but note that it must be redone if sp tree is replaced!
						// Need a property observer on the gs.speciestree!
		gene_set.speciestree->preprocessLCA();
		};
	~GeneTreeHeuristic()
		{
		//gene_set.speciestree->postprocessLCA();  ...now I take care of this in the species tree destructor in gtp-spr-treeset.h
		}

	TreeScore preoptimizeMP(unsigned int mp_iter,double gtp_weight, double mp_weight)
		{
		msgout << "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%" << endl;
		msgout << "PRE-OPTIMIZE MP SCORES..." << endl;
		msgout << "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%" << endl;
		TreeScore initialScore, mpTotalScore,postMPScore,rerootTotalScore,gtpmpTotalScore, finalScore;

		gene_set.createLeafMapping(); // Do this in case sp tree has been deleted and replaced	
		initializeScores(gtp_weight, mp_weight);
		initialScore 	= scoreTotal(gtp_weight,mp_weight);
		mpTotalScore 	= optimizeGeneTreesBy(MP,mp_iter,gtp_weight,mp_weight);
		finalScore	= reScoreTotal(gtp_weight,mp_weight); // this ensures both MP and GTP scores done (clunky)
		msgout << "********************************************************************************" << endl;
		msgout << "Results:" << endl;
		initialScore.printScores	("Initial scores:         ");
		mpTotalScore.printScores	("MP scores:              ");
		finalScore.printScores 	        ("Final scores:           ");
		return finalScore;
		}
	TreeScore optimizeRooting(double gtp_weight, double mp_weight)
		{
		msgout << "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%" << endl;
		msgout << "OPTIMIZE GENE TREE ROOTING W.R.T. GTP SCORES..." << endl;
		msgout << "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%" << endl;
		TreeScore initialScore, mpTotalScore,postMPScore,rerootTotalScore,gtpmpTotalScore, finalScore;

		gene_set.createLeafMapping();	
		initialScore 	= reScoreTotal(gtp_weight, mp_weight);
		rerootTotalScore = optimizeGeneTreesBy(GTP_ROOTING,1,gtp_weight,mp_weight); // second arg is not used
		finalScore	= reScoreTotal(gtp_weight,mp_weight);
		msgout << "********************************************************************************" << endl;
		msgout << "Results:" << endl;
		initialScore.printScores	("Initial scores:         ");
		finalScore.printScores 	        ("Final scores:           ");
		return finalScore;
		}

	//*********************************************************************************************
	// Optimize all gene trees separately using sequence of various algorithms.
	// On exit, this will have set all gene trees in the TSAS object to their individual local optima
	// according to various criteria for optimization. Also will report some results.
	//*********************************************************************************************

	TreeScore geneTreeHeuristicAll(unsigned int gtpmp_max_iter, double gtp_weight, double mp_weight)
		{
		TreeScore initialScore, mpTotalScore,postMPScore,rerootTotalScore,gtpmpTotalScore, finalScore;

		gene_set.createLeafMapping();	// has to happen any time the species tree has been deleted and replaced
		initializeScores(gtp_weight, mp_weight);
		//gene_set.report();
		initialScore 	= scoreTotal(gtp_weight,mp_weight);
		//mpTotalScore 	= optimizeGeneTreesBy(MP,1,gtp_weight,mp_weight);
		//postMPScore	= reScoreTotal(gtp_weight,mp_weight);
		//rerootTotalScore= optimizeGeneTreesBy(GTP_ROOTING);
		gtpmpTotalScore	= optimizeGeneTreesBy(GTP_MP,gtpmp_max_iter,gtp_weight,mp_weight);
		finalScore 	= gtpmpTotalScore;

		msgout << "********************************************************************************" << endl;
		msgout << "Results:" << endl;
		initialScore.printScores	("Initial scores:         ");
		//mpTotalScore.printScores	("MP scores:              ");
		//postMPScore.printScores 	("PostMP all scores:      ");
		//rerootTotalScore.printScores 	("Reroot scores:          ");
		//gtpmpTotalScore.printScores 	("GTP_MP scores:          ");
		finalScore.printScores  	("Final scores:           ");
		return finalScore;
		}

	// Initialize scores across gene set, stored for each gene
	void initializeScores(double gtp_weight, double mp_weight)
		{
		for (auto gene = gene_set.genes().begin();gene<gene_set.genes().end();gene++) 
		//for (auto gene : gene_set.genes()) // for each gene, do a separate gene tree heuristic search
			{
			gene->tree_score = getReferenceTreeScore(*gene,gtp_weight,mp_weight);
			//gene.tree_score = getReferenceTreeScore(gene,gtp_weight,mp_weight);
			}
		return ;
		}

	// Recompute scores of all genes for reference tree at each gene
	TreeScore reScoreTotal(double gtp_weight,double mp_weight)
		{
		TreeScore score,totalScore;
		totalScore.setScores(0,0,0,gtp_weight,mp_weight);
		for (auto gene : gene_set.genes()) // for each gene, do a separate gene tree heuristic search
			{
			totalScore = totalScore + getReferenceTreeScore(gene,gtp_weight,mp_weight);
			}
		return totalScore;	
		}

	// Summarize scores across gene set after searches, assuming those scores have been saved as gene.treeScore
	TreeScore scoreTotal(double gtp_weight, double mp_weight)
		{
		TreeScore score,totalScore;
		totalScore.setScores(0,0,0,gtp_weight,mp_weight); // weights are set initially and TreeScore + operator does not change them.
		for (auto gene : gene_set.genes()) // for each gene, do a separate gene tree heuristic search
			{
			totalScore = totalScore + gene.tree_score;
			}
		return totalScore;	
		}

	// Manage calling different searches and reporting results
	TreeScore optimizeGeneTreesBy(enum CriteriaType opt_criteria, unsigned int max_iter,double gtp_weight, double mp_weight)
		{
		// Optionally check the final tree in any GTP_MP search to confirm that its scores match those determined by "fast" algorithms
		bool recheck_score = true; // costs a smallish fraction of compute time

		msgout << "********************************************************************************" << endl;
		if (opt_criteria == GTP_ROOTING)
			{msgout << "Exhaustive rerooting to optimize gene trees"<< endl;}
		else 
			{
			msgout << "Heuristic search using rSPR rearrangements with criteria = ";
			switch (opt_criteria)
				{
				case MP: msgout << "MP" << endl; break;
				case GTP_MP: msgout << "'weighted GTP + MP'" << endl; break;
				default: msgout << endl;
				}
			msgout << "GTP weight = " << gtp_weight << ";  MP weight = " << mp_weight << endl;
			msgout << "Maximum iterations = " << max_iter << endl;
			}
		TreeScore score,totalScore;
		//totalScore.setScores(0,0,0,gtp_weight,mp_weight);
		int gene_number = 0;
		unsigned int min_seen_gtp, sum_min_gtp = 0;
		for (auto gene = gene_set.genes().begin();gene<gene_set.genes().end();gene++) // for each gene, do a separate gene tree heuristic search
		//for (auto gene : gene_set.genes()) // for each gene, do a separate gene tree heuristic search
			{
			++gene_number;
			msgout << "..........................." << endl;
			msgout << "Gene " << gene_number << endl ;
#if 0
			if (opt_criteria == GTP_ROOTING)
				score = runRerootSearch(gene,gtp_weight,mp_weight);
			else
				score = searchGlobalSPRNeighborhood(gene,max_iter,opt_criteria,gtp_weight,mp_weight);
#else
			switch(opt_criteria)
				{
				case (GTP): ;	// unused
				case (GTP_ROOTING):
					score = runRerootSearch(*gene,gtp_weight,mp_weight); break;
				case (MP):
					score = searchGlobalSPRNeighborhood(*gene,max_iter,opt_criteria,gtp_weight,mp_weight);break;
				case (GTP_MP):
					// Commenting out old version in favor of new specialized version
					//score = searchGlobalSPRNeighborhood(gene,max_iter,opt_criteria,gtp_weight,mp_weight);break;
					score = gtpmpSearchGlobal(*gene,max_iter,gtp_weight,mp_weight);break;
				}

#endif
			gene->tree_score = score; // save scores for each alignment here

			if (opt_criteria == GTP_MP && recheck_score) // temporary for validation of scoring both mp and gtp funcs
				{
				TreeScore checkScore = getReferenceTreeScore(*gene,gtp_weight,mp_weight);
				checkScore.printScores("[Rechecked scores:]      ");
				if (!(score == checkScore)) 
					{
					msgout << "WARNING: Score recheck FAILED!" << endl;
					//msgout << gene.tree->tree2newick()<<endl;
					EXCEPTION("Exiting...");
					}
				}
			//totalScore = totalScore + score;
			}
		return scoreTotal(gtp_weight,mp_weight);	// summarize across genes
		}

	//*********************************************************************************************
	// Next come higher level routines to do the searches, followed by lower level functions
	//*********************************************************************************************

	//*********************************************************************************************
	// Search over all rerootings of the gene's starting tree, incl starting tree and return one that 
	// improves GTP score the most.
	// Does not need to recompute MP  score for this. Uses efficient NNI structure to compute GTP scores.
	//*********************************************************************************************

	TreeScore runRerootSearch(TSASData::Gene &gene,unsigned int gtp_weight, unsigned int mp_weight) 
		{
		RerootNeighborhoodUsingNNI rrn(gene.tree); 
		//map <NODE*,unsigned int> score_h; // this will collect garbage but needed for the following func
		vector<struct gtp_data> score_v; // part of changeover to vector data structure in searches (see below)  
// set flag to 0!
#if 0
 		TreeScore refTreeScore = getReferenceTreeScore(gene,gtp_weight,mp_weight);
		NNISeqTree gtpRefTree(gene.tree,refTreeScore); //***** This will be used in local search

#else
		NNISeqTree gtpRefTree(gene.tree,gene.tree_score); // Assumes the scores have already been stored in alignments
#endif


		//NNISeqTree bestRerootedTree = gtpSearchLocalNNISeqNeighborhood(gene,rrn,gtpRefTree,score_h,gtp_weight,mp_weight);
		NNISeqTree bestRerootedTree = gtpSearchLocalNNISeqNeighborhood2(gene,rrn,gtpRefTree,score_v,gtp_weight,mp_weight);
		//NNISeqTree bestRerootedTree = gtpSearchLocalNNISeqNeighborhood(gene,rrn,score_h,gtp_weight,mp_weight);
		bestRerootedTree.printScores("Exact reroot search score: ");
		rrn.moveReferenceTreeToHere(bestRerootedTree.getTreePosition()); 
		return bestRerootedTree.getTreeScore();
		}

	//*********************************************************************************************
	// Do max_iter iterations of ...
	// ...search over global rSPR neighborhood of starting tree of ...
	// ......searches over local rSPR neighborhoods to find best MP  tree, or
	// ......searches over local rSPR neighborhoods to find best weighted GTP+MP score trees
	// If there is any improvement in score during iteration, move reference tree to the better tree
	// and start another iteration. Else terminate if there was no improvement found in the global neighborhood.
	// Returns: TreeScore object with optimal scores for this gene
	//*********************************************************************************************

	TreeScore searchGlobalSPRNeighborhood(	TSASData::Gene &gene, \
						int max_iter, \
						enum CriteriaType opt_criteria, \
						double gtp_weight, double mp_weight)
		{


		TreeScore refTreeScore = getReferenceTreeScore(gene,gtp_weight,mp_weight);

		SPRSeqTree gtpmpBestGlobalTree(gene.tree,refTreeScore); // These accumulate the best trees WITHIN an iteration
		SPRSeqTree mpBestGlobalTree=gtpmpBestGlobalTree; 	// across the local SPR searches within the global SPR search
//		unsigned int mps=mpBestGlobalTree.getMPScore();
//		mpBestGlobalTree.set(0,mps,0,mps);
//		mpBestGlobalTree.setScores(0,mps,0,gtp_weight,mp_weight);

		SPRSeqTree gtpmpOptimalTree = gtpmpBestGlobalTree; // These accumulate the best global SPR trees BETWEEN iterations
		SPRSeqTree mpOptimalTree = mpBestGlobalTree;	   // ...

		//unsigned int gtpmpScore0 = gtpmpBestGlobalTree.getTotalScore();
		//unsigned int mpScore0 = mpBestGlobalTree.getMPScore();

		map<NODE*,unsigned int> mpScore_h; // hash score for each spr using target_node as key
		map<NODE*,unsigned int> gtpScore_h; // hash score for each spr using target_node as key

		//msgout << "gene.tree dump start runFastGlobalSPRSearch:" << endl << *(gene.tree) << endl;

		for (int i=0;i<max_iter;i++)
			{
// But notice that the only thing that changes between iterations is the reference tree topology, so should be able to just fetch all
// scores from previous iteration and put them on new ref tree...this needs to look at the local/global variables set below...
// Next...change the following so they only compute either gtp or mp scores respectively.
// Actually, since we need both those scores in each iteration, it's ok to leave this as is???


// It's obscure but we use these respective optimal trees to reseed the reference trees needed in the local searches. This avoids recomputing anything

			NNISeqTree gtpRefTree(gene.tree,gtpmpOptimalTree.getTreeScore()); // this is the only tree that keeps track of gtp scores below
			SPRSeqTree mpRefTree(gene.tree);
			// The following is necessary, because the proper resetting of the best or "reftree" needed in calling the mpLocalSearch 
			// can differ depending on which path the program flows through the spaghetti below. Simplicity might suggest separating these criteria-paths...
			if (opt_criteria == MP)
				mpRefTree.set(mpOptimalTree.getTreeScore());
			if (opt_criteria == GTP_MP)
				mpRefTree.set(gtpmpOptimalTree.getTreeScore());


			msgout << "Search iteration " << i ;
			rSPR_GlobalTreeNeighborhood tnspr(gene.tree); 

			for (auto subtree : tnspr.source_nodes)
				{
				//msgout << ".........\n...doing local search(s) using subtree:" << gene.tree->getNodeWName(subtree) << endl ;
				if (opt_criteria == MP || opt_criteria == GTP_MP)
					{
					//fast local mp search
					mpScore_h.clear();

					// For just an MP search we care about the returned SPRSeqTree below, but for GTP+MP, we are just going
					// to use the mashup based on the hashes and don't really care about this return value...
					SPRSeqTree mpBestLocalTree = mpSearchLocalSPRNeighborhood(gene,mpRefTree,subtree,mpScore_h,gtp_weight,mp_weight);

					if (mpBestLocalTree.isBetterThan(mpBestGlobalTree,MP))
						{
						mpBestGlobalTree = mpBestLocalTree; // improvement occured in this local neighborhood
						}
					}

				if (opt_criteria == GTP_MP)
					{	
					//fast local gtp search (keep instantiating neighborhood outside of func! we have two versions of NNISeq neighb 
					rSPR_LocalTreeNeighborhoodUsingNNI rs(gene.tree,subtree);
					NNISeqTree temp = gtpSearchLocalNNISeqNeighborhood(gene,rs,gtpRefTree,gtpScore_h,gtp_weight,mp_weight); 
					// Again, don't really care about this return value except to keep track of best ever scores below...
					// Using the hash in the mashup of the two scores. The mashup is over all neighbors of the ref tree
					SPRSeqTree gtpmpBestLocalTree = mashup(mpScore_h,gtpScore_h, gene.tree,subtree, gtp_weight,mp_weight); 

					if (gtpmpBestLocalTree.isBetterThan(gtpmpBestGlobalTree,GTP_MP))
						gtpmpBestGlobalTree = gtpmpBestLocalTree; // improvement occured in this local neighborhood

					// "Digression": this is to save best-ever gtp and (below) mp tree scores...
						//if (temp.isBetterThan(gene.alignment->bestever_gtp_score,GTP))
						//	gene.alignment->bestever_gtp_score = temp.getTreeScore();

					}
				} // end loop over all local neighborhoods for this iteration ...BestGlobalTree is current best

			// Digression...
			// At end of current iteration; now save best-ever and test if we've improved over previous iteration
					//if (mpBestGlobalTree.isBetterThan(gene.alignment->bestever_mp_score,MP))
					//	gene.alignment->bestever_mp_score = mpBestGlobalTree.getTreeScore();
			//
			if (opt_criteria == MP)
				{
				if (mpBestGlobalTree.isBetterThan(mpOptimalTree,MP))
					{
					mpBestGlobalTree.printScores(" scores: ");  
					mpBestGlobalTree.getSPR().rearrange(); // move to that tree and continue to next iter
					mpOptimalTree = mpBestGlobalTree;
					}
				else
					{
					mpOptimalTree.printScores(" (Best): "); // no improvement; terminate search with this
					return mpOptimalTree.getTreeScore();
					}
				}
			if (opt_criteria == GTP_MP)
				{
				if (gtpmpBestGlobalTree.isBetterThan(gtpmpOptimalTree,GTP_MP))
					{
					gtpmpBestGlobalTree.printScores(" scores: ");  
					gtpmpBestGlobalTree.getSPR().rearrange(); 
					gtpmpOptimalTree = gtpmpBestGlobalTree;
					}
				else
					{
					gtpmpOptimalTree.printScores(" (Best): "); // no improvement; terminate search with this
					return gtpmpOptimalTree.getTreeScore();
					}
				}

			} // Continue iterations

		// got to max iterations while still making progress: terminate and return appropriate tree 
		if (opt_criteria == MP)
			return mpOptimalTree.getTreeScore(); 
		else
			return gtpmpOptimalTree.getTreeScore(); 
		}

	//*********************************************************************************************
	// Combine the two score hashes from mp and gtp searches to find best local tree that minimizes weighted
	// linear combination of scores. Returns a saved tree of this solution.
	// The hash keys are based on the SPR moves of the trees, which lets the correct scores be 
	// combined. See code for .toSPR() in treeneighborhood.h

	// NB. The hash INCLUDES THE REFERENCE TREE.

	//*********************************************************************************************

	typedef std::map<NODE*,unsigned int> NodeH;
	SPRSeqTree mashup(NodeH &mpScore_h,NodeH &gtpScore_h, TREE* tree, NODE* subtree, double gtp_weight, double mp_weight)
		{

		//printScoreH(gtpScore_h,"gtpScore_h",tree,subtree); 
		//printScoreH(mpScore_h,"mpScore_h",tree,subtree); 
		if (mpScore_h.size() != gtpScore_h.size())
			EXCEPTION("Size mismatch in score hashes");
		NodeH::iterator it;
		unsigned int min_score = std::numeric_limits<unsigned int>::max();
		SPRSeqTree best_tree;
		SPR spr;
		for (it=mpScore_h.begin();it != mpScore_h.end(); it++)
			{
			NODE* target_node = it->first;
			unsigned int mpScore = it->second;
			NodeH::iterator jt;
			jt = gtpScore_h.find(target_node);
			if (jt == gtpScore_h.end())
				EXCEPTION("A node in one hash was not found in the other");
			unsigned int gtpScore = jt->second;
			unsigned int weighted_score = mpScore*mp_weight + gtpScore*gtp_weight;
			if (weighted_score < min_score)  // should always happen at least once
				{
				min_score = weighted_score;
				best_tree.setScores(gtpScore,mpScore,0,gtp_weight,mp_weight);		
				spr.tree = tree;
				spr.subroot = subtree;
				spr.target_node = target_node;
				best_tree.setSPR(spr);
				}
			}
		return best_tree;
		}

	void printScoreH(std::map<NODE*,unsigned int> &h,string label,TREE* t, NODE* subtree)
		{
		std::map<NODE*,unsigned int>::iterator it;
		msgout << endl;
		for (it=h.begin();it != h.end(); it++)
			{
			msgout << label <<  ":" << it->first << "=> " << it->second << ":" << endl;
#if 0
			SPR spr(t,subtree,it->first);
			NODE* save_sib = subtree->getSibling();
			SPR spr_back(t,subtree,save_sib);
			spr.rearrange();
			msgout << "tree saved as SPR move: " << t->tree2newick() << endl;
			spr_back.rearrange();
#endif
			}
		}



	// Compute the gtp and mp score of the current gene.tree
	// Note that this is done without any of the "fast" architecture of NNI moves for gtp
	// or view 3-vectors for mp, so this represents a quasi-independent check on those faster computations

	// CAREFUL: MUST HAVE RUN createLeafMapping() first...

	TreeScore getReferenceTreeScore(TSASData::Gene &gene,double gtp_weight, double mp_weight)
		{
		TreeScore score;
		TREE *t0=gene.tree;
		gene_set.createPrimaryMapping(*t0); 
		unsigned int gtpScore = gene_set.getScore(*t0);
		unsigned int mpScore = gene.alignment->computeMPScore(t0);
		score.setScores(gtpScore,mpScore,0,gtp_weight,mp_weight);
		return score;
		}
	//*********************************************************************************************

	// Search across a local SPR neighborhood, using the 'UsingSPR' neighborhood data structure, which
	// is just a vector of all valid SPR moves for the given gene.tree and subtree. 
	// Meant to handle all optimality criteria, but at the moment focuses on efficient computing of
	// mp score using 'views'. 

	// ** Given: a gene and gene.tree, and a subtree root node (and thus an rSPR local neighborhood).
	// ** Finds: the best mp tree in this local rSPR neighborhood, including the starting tree

	// This requires O(n) to set up neighborhood, O(n) to set up state vectors
	// and then O(n) to compute mp scores for ALL trees in neighborhood.

	//*********************************************************************************************

	SPRSeqTree mpSearchLocalSPRNeighborhood(TSASData::Gene &gene, SPRSeqTree refTree, NODE* subtree,  \
		map<NODE*,unsigned int> &score_h, \
		unsigned int gtp_weight, unsigned int mp_weight)
		{
		SPRSeqTree bestTree = refTree;

		//bestTree.setGTPScore(0); 

		unsigned int mpScore;

		SITEINT* ss0 = new SITEINT[gene.alignment->nchar];
		SITEINT* ss1 = new SITEINT[gene.alignment->nchar];

		rSPR_LocalTreeNeighborhoodUsingSPR spr_neighborhood(gene.tree,subtree);
// Here we add the provided reference tree score to the hash before doing real work.
		NODE* spr_target_node = subtree->getSibling(); // this is tantamount to "no move", i.e., stay at reference tree (obscure!)
		score_h[spr_target_node]=refTree.getMPScore(); // include the score of ref tree
//
		TreeCut tc(gene.tree,subtree);
		tc.cut();
		gene.alignment->setupStateSetViews(tc.pruned_tree);
		gene.alignment->setupStateSetViews(tc.subtree);

		for (auto rsitr=spr_neighborhood.begin();rsitr != spr_neighborhood.end();rsitr++) 
				{
				SPR spr = *rsitr;
				NODE* t = spr.target_node;
				NODE* s = spr.subroot;
				View vs(s,nullptr);
				unsigned int mp;	
				if (t->isRoot()) 
					{
					View vt(t,nullptr);
					mp = vt.getMPScore()+vs.getMPScore();
					mp += gene.alignment->computeStateSet(vt.getStateSet(),vs.getStateSet(),ss1);
					}
				else
					{
					NODE* p = t->parent();
					View vt(t,p);
					View vp(p,t);
					mp = vt.getMPScore()+vp.getMPScore()+vs.getMPScore();
					mp += gene.alignment->computeStateSet(vt.getStateSet(),vs.getStateSet(),ss1);
					mp += gene.alignment->computeStateSet(vp.getStateSet(),ss1,ss0);
					}
				mpScore = mp;
				score_h[spr.target_node]=mpScore;


				if (mpScore < bestTree.getMPScore())
					{
					bestTree.setSPR(spr);
					//bestTree.set(0,mpScore,0,mpScore);
					bestTree.setMPScore(mpScore);
					}
				}
		tc.uncut();
		delete[] ss0;
		delete[] ss1;
		return bestTree;
		}

//***********************************************************************************************************************
//***********************************************************************************************************************
// Following are modified searches using a vector to combine gtp and mp scores of trees in a neighborhood, instead of
// original hash structure. The latter caused unpredictable ranking of tied trees, because std:map does not guarantee
// order of key/values will be preserved between runs at all. So this often led to different optimization paths to different
// solutions. Not terribly bad, but better to explicitly code this randomness at some point than just have it happen!
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

//***********************************************************************************************************************

	// New attempt to simplify...Working
	TreeScore gtpmpSearchGlobal(	TSASData::Gene &gene, int max_iter, double gtp_weight, double mp_weight)
		{

		TreeScore refTreeScore = getReferenceTreeScore(gene,gtp_weight,mp_weight);
		SPRSeqTree gtpmpBestGlobalTree(gene.tree,refTreeScore); // These accumulate the best trees WITHIN an iteration
		SPRSeqTree gtpmpOptimalTree = gtpmpBestGlobalTree; // These accumulate the best global SPR trees BETWEEN iterations

		map<NODE*,unsigned int> gtpScore_h; // hash score for each spr using target_node as key
		vector<struct gtp_data>gtp_data_v; // new vector version of same

		unsigned int min_seen_gtp=UINT_MAX; // smallest value seen anywhere during the following search

		for (int i=0;i<max_iter;i++)
			{
			NNISeqTree gtpmpRefTree(gene.tree,gtpmpOptimalTree.getTreeScore()); // this is the only tree that keeps track of gtp scores below
			msgout << "Search iteration " << i ;
			rSPR_GlobalTreeNeighborhood tnspr(gene.tree); 

			for (auto subtree : tnspr.source_nodes)
				{
				gtpScore_h.clear();
				rSPR_LocalTreeNeighborhoodUsingNNI rs(gene.tree,subtree,nni_radius);

#ifdef HASH_NEIGHBORS  		// don't define this...	
				// Original code used a hash structure, gtpScore_h. This was ulimately (though not originally) unnecessary, 
				// inefficient, and caused indeterminacy in the following two calls
				gtpSearchLocalNNISeqNeighborhood(gene,rs,gtpmpRefTree,gtpScore_h,gtp_weight,mp_weight); 
				SPRSeqTree gtpmpBestLocalTree = mpSearchLocalAdd(gene, subtree, gtpScore_h, gtp_weight, mp_weight);
#else
				// Get a vector having all the tree(moves) and their gtp scores in this local neighborhood.
				// New code uses a vector instead of a hash, which guarantees processing of trees 
				// in the neighborhood in the same order in both function calls (hash was not deterministic).
				gtpSearchLocalNNISeqNeighborhood2(gene,rs,gtpmpRefTree,gtp_data_v,gtp_weight,mp_weight); 
				SPRSeqTree gtpmpBestLocalTree = mpSearchLocalAdd2(gene, subtree, gtp_data_v, gtp_weight, mp_weight);
#endif
				if (gtpmpBestLocalTree.isBetterThan(gtpmpBestGlobalTree,GTP_MP))
						gtpmpBestGlobalTree = gtpmpBestLocalTree; // improvement occured in this local neighborhood
				//for (auto gtp_data:gtp_data_v)
				//	if (gtp_data.gtp_score < min_seen_gtp) min_seen_gtp = gtp_data.gtp_score;
				} // end loop over all local neighborhoods for this iteration ...BestGlobalTree is current best

			if (gtpmpBestGlobalTree.isBetterThan(gtpmpOptimalTree,GTP_MP))
					{
					gtpmpBestGlobalTree.printScores(" scores: ");  
					gtpmpBestGlobalTree.getSPR().rearrange(); 
					gtpmpOptimalTree = gtpmpBestGlobalTree;
					}
			else
					{
					gtpmpOptimalTree.printScores(" (Best): "); // no improvement; terminate search with this
					return gtpmpOptimalTree.getTreeScore();
					}

			} // Continue iterations

		// got to max iterations while still making progress: terminate and return appropriate tree 
		return gtpmpOptimalTree.getTreeScore(); 
		}

// Computes mp and "adds" the mp results and gtp results to its TreeScore return value, optimizing by the total GTP_MP score.

// This version uses the hash of SPR target nodes (effectively SPR moves) created by the gtp local search as input
// and therefore restricts computing only to those moves. This will let us compute on a restricted SPR neighborhood
// eventually. This version simplifies the global search because it takes care of the mashup of gtp and mp moves right
// here rather than having to call separate mashup function. Seems a little faster, but definitely simpler.

	SPRSeqTree mpSearchLocalAdd(TSASData::Gene &gene, NODE* subtree,  map<NODE*,unsigned int> &gtp_score_h, \
		unsigned int gtp_weight, unsigned int mp_weight)

		{
		SPRSeqTree bestTree(gene.tree);

		unsigned int mpScore;

		SITEINT* ss0 = new SITEINT[gene.alignment->nchar];
		SITEINT* ss1 = new SITEINT[gene.alignment->nchar];

		rSPR_LocalTreeNeighborhoodUsingSPR spr_neighborhood(gene.tree,subtree);

		TreeCut tc(gene.tree,subtree);
		tc.cut();
		gene.alignment->setupStateSetViews(tc.pruned_tree);
		gene.alignment->setupStateSetViews(tc.subtree);

		SPR spr;
		bool first=true;
		NodeH::iterator it; // see typedef above
		for (it=gtp_score_h.begin();it != gtp_score_h.end(); it++)
			{
			NODE* s = subtree;
			NODE* t = it->first;
			unsigned int gtpScore = it->second;
			SPR spr(gene.tree,s,t);
			View vs(s,nullptr);
			unsigned int mp;	
			if (t->isRoot()) 
					{
					View vt(t,nullptr);
					mp = vt.getMPScore()+vs.getMPScore();
					mp += gene.alignment->computeStateSet(vt.getStateSet(),vs.getStateSet(),ss1);
					}
			else
					{
					NODE* p = t->parent();
					View vt(t,p);
					View vp(p,t);
					mp = vt.getMPScore()+vp.getMPScore()+vs.getMPScore();
					mp += gene.alignment->computeStateSet(vt.getStateSet(),vs.getStateSet(),ss1);
					mp += gene.alignment->computeStateSet(vp.getStateSet(),ss1,ss0);
					}
			mpScore = mp;
			TreeScore currentScore(gtpScore,mpScore,0,gtp_weight,mp_weight);
			if (first)
					{
					first=false;
					bestTree.setSPR(spr);
					bestTree.set(currentScore);
					}
			else
				if (currentScore.isBetterThan(bestTree,GTP_MP))
					{

					bestTree.setSPR(spr);
					bestTree.set(currentScore);
					}

			// Possibly the following should be optional.
			// Keep track of the best ever scores seen, irrespective of the best GTP_MP score found here
//			if (gtpScore < gene.alignment->bestever_gtp_score) gene.alignment->bestever_gtp_score = gtpScore;
//			if (mpScore < gene.alignment->bestever_mp_score) gene.alignment->bestever_mp_score = mpScore;

			}

		tc.uncut();
		delete[] ss0;
		delete[] ss1;
		return bestTree;
		}


//***********************************************************************************************************************
//***********************************************************************************************************************
//***********************************************************************************************************************

	//*********************************************************************************************
	// Combine the two score hashes from mp and gtp searches to find best local tree that minimizes weighted
	// linear combination of scores. Returns a saved tree of this solution.
	//*********************************************************************************************

	// GTP score search across any neighborhood implemented as an NNI_Seq data structure, which includes
	// local SPR neighborhoods and Reroot neighborhoods.

	// Note, we pass the neighborhood as a parameter because we have at least two use cases with different neighborhoods

	// ** Given: a gene and gene.tree, and local neighborhood.
	// ** Finds: the best gtp tree in this neighborhood, including the reference tree

	// This requires O(n) to set up neighborhood, 
	// and then O(n) to compute gtp scores for ALL trees in neighborhood.

	//*********************************************************************************************
	NNISeqTree gtpSearchLocalNNISeqNeighborhood(TSASData::Gene &gene, NNISeqTreeNeighborhood &nniseq_neighborhood, NNISeqTree refTree, 
	//NNISeqTree gtpSearchLocalNNISeqNeighborhood(TSASData::Gene &gene, NNISeqTreeNeighborhood &nniseq_neighborhood, 
			map<NODE*,unsigned int> &score_h, 
			unsigned int gtp_weight,unsigned int mp_weight)
		{
		// parameter bestTree should be initialized to the reference tree by the caller.

		// Initialization insures reference tree will be included in search
//		NNISeqTree bestTree = initializeReferenceTree(gene,gtp_weight,mp_weight);//set local best tree to ref tree
// Moving the redundant init out of here...
		NNISeqTree bestTree = refTree;

		score_h.clear();

		TREE *t0=gene.tree;
		gene_set.createPrimaryMapping(*t0); // Make sure to set this here

		unsigned int gtpScore0 = bestTree.getGTPScore();
		unsigned int gtpScore=gtpScore0;

		NODE* spr_target_node = nniseq_neighborhood.toSPR(NNI(gene.tree)).target_node; // target node will end up being sib of neighborhood's subtree variable
		// NB! when the neighborhood is a reroot neighborhood, this 'SPR' gets set to something silly, and the hash won't mean much
		score_h[spr_target_node]=gtpScore0; // include the score of ref tree

		SpeciesNode *mapping, *mappingABC;
		int cc=0;
		for (auto rsitr=nniseq_neighborhood.begin();rsitr != nniseq_neighborhood.end();rsitr++) 
				{
				auto nni = *rsitr;
				NODE* spr_target_node = nniseq_neighborhood.toSPR(nni).target_node; // do this before rearrangment
				// start handling gtp calculations
				auto A = nni.getSource();
				auto C = A->getAunt();	// This is *currently* taxon C in ((A,B),C), but this => ((C,B),A) after NNI
				int delta = deltaGTPScore(A,mapping,mappingABC); // fetches deltaGTP score AND the one needed mapping needed below
				gtpScore += delta;
				nni.rearrange(); 	// Having done the elegant compute for gtp score BEFORE moving to the new tree, 
							// we now actually move to tree and can do other stuff	
				(C->parent())->setMapping(mapping); // set these internal nodes to their new mappings
				(A->parent())->setMapping(mappingABC); 

				if (nni.isPostTreeNovel()) // i.e., these are trees not already seen in the nni traversal
					{
					int tree_pos = nniseq_neighborhood.position(rsitr)+1; // convert from the nni pos to tree pos

					// find equivalent spr for current tree and hash it
					score_h[spr_target_node]=gtpScore; // only hash this if the move leads to a novel tree

//msgout << gtpScore << " ";
					if (gtpScore < bestTree.getGTPScore())
						{
						//bestTree.set(gtpScore,0,0,tree_pos); 
						bestTree.setGTPScore(gtpScore);
						bestTree.setTreePosition(tree_pos); //ok, now this version doesn't touch MP score...

						}
//Careful not to set MP score to 0 in this code, because the treescores get passed out to caller and then copied to each alignment
// and they need to keep their mp scores computed elsewhere...
					}
				}
//msgout << endl;
		return bestTree;
		}

//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

	NNISeqTree gtpSearchLocalNNISeqNeighborhood2(TSASData::Gene &gene, NNISeqTreeNeighborhood &nniseq_neighborhood, NNISeqTree refTree, vector<struct gtp_data> &gtp_data_v, unsigned int gtp_weight,unsigned int mp_weight)
		{
		// parameter bestTree should be initialized to the reference tree by the caller.

		// Initialization insures reference tree will be included in search
//		NNISeqTree bestTree = initializeReferenceTree(gene,gtp_weight,mp_weight);//set local best tree to ref tree
// Moving the redundant init out of here...
		NNISeqTree bestTree = refTree;

		gtp_data_v.clear();

		TREE *t0=gene.tree;
		gene_set.createPrimaryMapping(*t0); // Make sure to set this here

		unsigned int gtpScore0 = bestTree.getGTPScore();
		unsigned int gtpScore=gtpScore0;

		NODE* spr_target_node = nniseq_neighborhood.toSPR(NNI(gene.tree)).target_node; // target node will end up being sib of neighborhood's subtree variable
		// NB! when the neighborhood is a reroot neighborhood, this 'SPR' gets set to something silly, and the hash won't mean much

		struct gtp_data gtpd={spr_target_node,gtpScore0};
		gtp_data_v.push_back(gtpd); // include the score of ref tree

//msgout << gtpScore0 << " ";

		SpeciesNode *mapping, *mappingABC;
		int cc=0;
		for (auto rsitr=nniseq_neighborhood.begin();rsitr != nniseq_neighborhood.end();rsitr++) 
				{
				auto nni = *rsitr;
				NODE* spr_target_node = nniseq_neighborhood.toSPR(nni).target_node; // do this before rearrangment
				// start handling gtp calculations
				auto A = nni.getSource();
				auto C = A->getAunt();	// This is *currently* taxon C in ((A,B),C), but this => ((C,B),A) after NNI
				int delta = deltaGTPScore(A,mapping,mappingABC); // fetches deltaGTP score AND the one needed mapping needed below
				gtpScore += delta;
				nni.rearrange(); 	// Having done the elegant compute for gtp score BEFORE moving to the new tree, 
							// we now actually move to tree and can do other stuff	
				(C->parent())->setMapping(mapping); // set these internal nodes to their new mappings
				(A->parent())->setMapping(mappingABC); 

				if (nni.isPostTreeNovel()) // i.e., these are trees not already seen in the nni traversal
					{
					int tree_pos = nniseq_neighborhood.position(rsitr)+1; // convert from the nni pos to tree pos

					// find equivalent spr for current tree and hash it
					//score_h[spr_target_node]=gtpScore; // only hash this if the move leads to a novel tree
					struct gtp_data gtpd={spr_target_node,gtpScore};
					gtp_data_v.push_back(gtpd); // include the score of ref tree
					if (gtpScore < bestTree.getGTPScore())
						{
						bestTree.setGTPScore(gtpScore);
						bestTree.setTreePosition(tree_pos); //ok, now this version doesn't touch MP score...

						}
//Careful not to set MP score to 0 in this code, because the treescores get passed out to caller and then copied to each alignment
// and they need to keep their mp scores computed elsewhere...

					if (gtpScore < gene.alignment->bestever_gtp_score) 
						gene.alignment->bestever_gtp_score = gtpScore;
					}
				}
		return bestTree;
		}

//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
	SPRSeqTree mpSearchLocalAdd2(TSASData::Gene &gene, NODE* subtree,  vector<struct gtp_data> gtp_data_v, \
		unsigned int gtp_weight, unsigned int mp_weight)

		{
		SPRSeqTree bestTree(gene.tree);

		unsigned int mpScore;

		SITEINT* ss0 = new SITEINT[gene.alignment->nchar];
		SITEINT* ss1 = new SITEINT[gene.alignment->nchar];

		rSPR_LocalTreeNeighborhoodUsingSPR spr_neighborhood(gene.tree,subtree);

		TreeCut tc(gene.tree,subtree);
		tc.cut();
		gene.alignment->setupStateSetViews(tc.pruned_tree);
		gene.alignment->setupStateSetViews(tc.subtree);

		SPR spr;
		bool first=true;
		for (auto gtp_data:gtp_data_v)
			{
			NODE* s = subtree;
			NODE* t = gtp_data.spr_target_node;
			unsigned int gtpScore = gtp_data.gtp_score;

			SPR spr(gene.tree,s,t);
			View vs(s,nullptr);
			unsigned int mp;	
			if (t->isRoot()) 
					{
					View vt(t,nullptr);
					mp = vt.getMPScore()+vs.getMPScore();
					mp += gene.alignment->computeStateSet(vt.getStateSet(),vs.getStateSet(),ss1);
					}
			else
					{
					NODE* p = t->parent();
					View vt(t,p);
					View vp(p,t);
					mp = vt.getMPScore()+vp.getMPScore()+vs.getMPScore();
					mp += gene.alignment->computeStateSet(vt.getStateSet(),vs.getStateSet(),ss1);
					mp += gene.alignment->computeStateSet(vp.getStateSet(),ss1,ss0);
					}
			mpScore = mp;
			TreeScore currentScore(gtpScore,mpScore,0,gtp_weight,mp_weight);
			if (first)
					{
					first=false;
					bestTree.setSPR(spr);
					bestTree.set(currentScore);
					}
			else
				if (currentScore.isBetterThan(bestTree,GTP_MP))
					{

					bestTree.setSPR(spr);
					bestTree.set(currentScore);
					}

			// Possibly the following should be optional.
			// Keep track of the best ever scores seen, irrespective of the best GTP_MP score found here
			if (mpScore < gene.alignment->bestever_mp_score) gene.alignment->bestever_mp_score = mpScore;

			}

		tc.uncut();
		delete[] ss0;
		delete[] ss1;
		return bestTree;
		}

//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&



#if 0 // DOES NOT WORK: Can't cross the two algorithms in this way; their trees are not compatible at same points in the sequence

	//**************************************************************************************************************************
	// Computes the CHANGE in the GTP score for an NNI move between ((A,B),C) => ((C,B),A), which, using our data structures,
	//*********************************************************************************************

	// GTP score search across any neighborhood implemented as an NNI_Seq data structure, which includes
	// local SPR neighborhoods and Reroot neighborhoods.

	// Note, we pass the neighborhood as a parameter because we have at least two use cases with different neighborhoods

	// ** Given: a gene and gene.tree, and local neighborhood.
	// ** Finds: the best gtp tree in this neighborhood, including the reference tree

	// This requires O(n) to set up neighborhood, 
	// and then O(n) to compute gtp scores for ALL trees in neighborhood.

	//*********************************************************************************************
	SPRSeqTree gtpmpSearchLocal(TSASData::Gene &gene, SPRSeqTree refTree, NODE* subtree, unsigned int gtp_weight,unsigned int mp_weight)
		{
		SPRSeqTree bestTree = refTree;

		//inits for gtp part
		unsigned int gtpScore,gtpScore0;
		SpeciesNode *mapping, *mappingABC;
		TREE *t0=gene.tree;
		gene_set.createPrimaryMapping(*t0); 
		gtpScore0 = bestTree.getGTPScore();
		gtpScore  = gtpScore0;

		//inits for mp part
		unsigned int mpScore;
		unsigned int *ss0, *ss1;
		ss0 = new unsigned int[gene.alignment->nchar];
		ss1 = new unsigned int[gene.alignment->nchar];
		TreeCut tc(gene.tree,subtree);
		tc.cut();
		gene.alignment->setupStateSetViews(tc.pruned_tree);
		gene.alignment->setupStateSetViews(tc.subtree);
			
		rSPR_LocalTreeNeighborhoodUsingNNI nniseq_neighborhood(gene.tree,subtree);
			
		for (auto rsitr=nniseq_neighborhood.begin();rsitr != nniseq_neighborhood.end();rsitr++) 
				{
				auto nni = *rsitr;
				SPR spr = nniseq_neighborhood.toSPR(nni);


				// Do efficient gtp calculation and rearrange
				auto A = nni.getSource();
				auto C = A->getAunt();	// This is *currently* taxon C in ((A,B),C), but this => ((C,B),A) after NNI
				//int delta = deltaGTPScore(A,mapping); // old version worked with previous nni.rearrange() implementation
				int delta = deltaGTPScore(A,mapping,mappingABC); // fetches deltaGTP score AND the one needed mapping needed below
				gtpScore += delta;
				nni.rearrange(); 	// Having done the elegant compute for gtp score BEFORE moving to the new tree, 
							// we now actually move to tree and can do other stuff	
				(C->parent())->setMapping(mapping); // set these internal nodes to their new mappings
				(A->parent())->setMapping(mappingABC); 
				// end gtp calculation

				if (nni.isPostTreeNovel()) // i.e., these are trees not already seen in the nni traversal
					{
					// do efficient mp calculation but only on new trees. Use the spr equivalent move
					NODE* t = spr.target_node;
					NODE* s = spr.subroot;
					View vs(s,nullptr);
					unsigned int mp;	
					if (t->isRoot()) 
						{
						View vt(t,nullptr);
						mp = vt.getMPScore()+vs.getMPScore();
						mp += gene.alignment->computeStateSet(vt.getStateSet(),vs.getStateSet(),ss1);
						}
					else
						{
						NODE* p = t->parent();
						View vt(t,p);
						View vp(p,t);
						mp = vt.getMPScore()+vp.getMPScore()+vs.getMPScore();
						mp += gene.alignment->computeStateSet(vt.getStateSet(),vs.getStateSet(),ss1);
						mp += gene.alignment->computeStateSet(vp.getStateSet(),ss1,ss0);
						}
					mpScore = mp;

					// Finally, test new trees for optimality relative to ref tree
					TreeScore currentScore(gtpScore,mpScore,0,gtp_weight,mp_weight);
					if (currentScore.isBetterThan(bestTree,GTP_MP))
						{
						bestTree.setSPR(spr);
						bestTree.set(currentScore);
						}

					}
				}
		tc.uncut();
		delete ss0;
		delete ss1;
		return bestTree;
		}
#endif

	//**************************************************************************************************************************
	// Computes the CHANGE in the GTP score for an NNI move between ((A,B),C) => ((C,B),A), which, using our data structures,
	// would be implemented by doing an nni.rearrange() operation when the nni.source_node =  A. However, this function
	// does not actually do the rearrangement, which is really the point. This allows computing a rearranged tree in O(1)
	// running time. To engineer a tour of a bunch of trees connected by NNI moves, however, it is then necessary to recompute
	// mappings for the internal nodes of the triplet tree that will be changed. This is tricky and depends on how NNI moves
	// are actually implemented. For both  NNIs and SPRs we use the same tree.moveSubtree() function, which affects the internal
	// nodes in repeatable well-defined ways. The two species node mappings in the parameter list are returned to the caller
	// and then these must be set after the nni.rearrange() is called. We use the same underlying moveSubtree() code because 
	// this will let us keep NNI and SPR changes to the tree in 'sync' with each other--see the mashup() function for more on that.

	// The PrimaryMapping must be correctly set before calling this. Input is the source node, node 'A'.

	//*********************************************************************************************

	//public: long deltaGTPScore(NODE* source_node, SpeciesNode *& m2bc)
	public: long deltaGTPScore(NODE* source_node, SpeciesNode *& m2bc, SpeciesNode *& MABC)
		{
		int delta=0;
		NODE *A = source_node;
		NODE *B = A->getSibling();
		NODE *C = A->parent()->getSibling();
		SpeciesNode *&MA = A->getMapping();
		SpeciesNode *&MB = B->getMapping();
		SpeciesNode *&MC = C->getMapping();
//		SpeciesNode *&M2BC = gene_set.speciestree->getLCA(MB,MC); // careful to call this method on sptree object
		m2bc = gene_set.speciestree->getLCA(MB,MC); // careful to call this method on sptree object
		SpeciesNode *&M2BC = m2bc;
		SpeciesNode *&MAB = A->parent()->getMapping();
		//SpeciesNode *&MABC = A->parent()->parent()->getMapping();
		MABC = A->parent()->parent()->getMapping();
		// These are references to the pointer's location, so we don't make needless copies of these mappings
		// Equality checks are just on the mappings as pointers to species nodes: i.e.,  check if pointers are the same
		if (MAB==MA   || MAB==MB ) 
			--delta;
		if (M2BC==MB  || M2BC==MC) 
			++delta;
		if (MABC==MAB || MABC==MC)
			--delta;
		if (MABC==M2BC|| MABC==MA)
			++delta;

		return delta;
		}
#if 0
	//*********************************************************************************************
	// Brute force slow version of this search, which uses and O(n) gtp calculation for EACH tree.
	// Used to validate code as needed.
	//*********************************************************************************************
	void bruteGTPSearchLocalNNISeqNeighborhood(TSASData::Gene &gene, NNISeqTreeNeighborhood &nniseq_neighborhood, map<NODE*,unsigned int> &score_h)
		{
		// currently missing the code to hash the results for future debugging, 
		// also defaults to unweighted scoring
		for (auto rsitr=nniseq_neighborhood.begin();rsitr != nniseq_neighborhood.end();rsitr++) 
				{
				auto nni = *rsitr;
				nni.rearrange(); 	
				if (nni.isPostTreeNovel())
					TreeScore checkScore = getReferenceTreeScore(gene,1,1);
				}
		}
#endif


	};

#endif
