/*
 * Copyright (C) 2007 Andre Wehe, Mukul Bansal, Oliver Eulenstein
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef FASTGENEDUP_HEURISTIC_H
#define FASTGENEDUP_HEURISTIC_H

// ------------------------------------------------------------------------------------------------------------------
class Heuristic : public TreeSet {  //MJS: Subclass of TreeSet
//class Heuristic : public TSASData {  //MJS: Subclass of TreeSet ...maybe do this...
public:
	using TreeSet::getScore;

	Heuristic() {
		#ifdef DEBUG
		cout << "Heuristic created" << endl;
		#endif
	}
	//Heuristic(TSASDataPersistent dp): TSASData(dp) {};

	virtual ~Heuristic() {
		#ifdef DEBUG
		cout << "Heuristic destroyed" << endl;
		#endif
	}

	// ------------------------------------------------------------------------------------------------------
	// compute the gene duplications
	// subtree is the subtree pruned (P)  //MJS: ...and now the sister to root?
	// MJS: I think this computes across all the local rSPR moves for input where subtree has been made sister to root of sp tree.


	inline void computeGeneDuplications(SpeciesNode *subtree, bool reroot = true) {
if (reroot) 
	{
	WARNING("Legacy rerooting attempted in gtp-spr-heuristic search:computeGeneDuplications. Disabling...");
	reroot = false;
	}
		SpeciesNode *sibling = subtree->getSibling();
		speciestree->establishOrder(); //MJS:gtp-spr-treeset.h  Orders the nodes, initialize some node numbering for LCA?
		speciestree->preprocessLCA();  //MJS: prep the LCA stuff
		resetGeneDuplications(sibling); //MJS; set GDP score to 0 throughout sibling subtree
		// process all rooted trees
		for(int i=0; i<genetree_rooted.size(); i++) {
			GeneTreeRooted &tree = *genetree_rooted[i];
			createPrimaryMapping(tree); // leaf label map from gene tree labels to sp tree; 
			const double score = getScore(tree) * tree.weight; // MJS: getScore does the usual LCA based calc of score over both trees
			createSecondaryMapping(tree, subtree); // MJS: don't get this yet...
			computeGeneDuplicationsTriple(); //  in gtp-spr-treeset.h, complex algorithm...
			removeSecondaryMapping(tree);
			computeGeneDuplicationsAdd(sibling, score, tree.weight);
		}
		// process all unrooted trees
		for(int i=0; i<genetree_unrooted.size(); i++) {
			GeneTreeUnrooted &tree = *genetree_unrooted[i];
			if (reroot) { // find the best geneduplication score of all rootings (rerooting of the genetrees)
				createPrimaryMappingUnrooted(tree);
				double &score = best_score;
				score = getScore(tree) * tree.weight;
				createSecondaryMapping(tree, subtree);
				computeGeneDuplicationsTriple();
				removeSecondaryMapping(tree);
				computeGeneDuplicationsTempReplace(sibling, score, tree.weight);
				GeneNodeUnrooted *u = tree.root->child(0);
				GeneNodeUnrooted *v = tree.root->child(1);
				best_node[0] = u;
				best_node[1] = v;
				#ifdef DEBUG
				position_counter_debug = 2;
				if ((u == NULL) || (v == NULL)) EXCEPTION("child = NULL in computeGeneDuplications" << endl);
				#endif
				moveRoot(tree, u, subtree, sibling);
				moveRoot(tree, v, subtree, sibling);
				#ifdef DEBUG
				if (position_counter_debug != tree.nodes.size()-1)
					WARNING("tree traversal failed in computeGeneDuplications" << position_counter_debug << " != " << tree.nodes.size()-1 << endl);
				#endif
				tree.reroot(u, v);
				addTempGeneDuplications(sibling);
			} else { // find the best genedupication of the current rooting
				createPrimaryMapping(tree);
				const double score = getScore(tree) * tree.weight;
				createSecondaryMapping(tree, subtree);
				computeGeneDuplicationsTriple();
				removeSecondaryMapping(tree);
				computeGeneDuplicationsAdd(sibling, score, tree.weight);
			}
		}
		speciestree->postprocessLCA();
		forEachCallScoreComputed(subtree, sibling); // MJS. Looks like this is where we loop over INNER loop of rSPR neighborhood
	}

	// find the best rooting of the unrooted gene trees
	// MJS: Seems to me this is O(n^2) for each tree because the moveRoot2() recurses through the tree and at each node
	// calls getScore()
	void computeBestRooting() {
WARNING("Legacy rerooting attempted in computeBestRooting() in gtp-spr-heuristic search. Disabling");
return;
		speciestree->establishOrder();
		speciestree->preprocessLCA();
		// process all unrooted trees
		for(int i=0; i<genetree_unrooted.size(); i++) {
			GeneTreeUnrooted &tree = *genetree_unrooted[i];
			createPrimaryMappingUnrooted(tree);
			double &score = best_score;
			score = getScore(tree) * tree.weight;
			GeneNodeUnrooted *u = tree.root->child(0);
			GeneNodeUnrooted *v = tree.root->child(1);
			best_node[0] = u;
			best_node[1] = v;
			#ifdef DEBUG
			position_counter_debug = 2;
			if ((u == NULL) || (v == NULL)) EXCEPTION("child = NULL in computeBestRooting" << endl);
			#endif
			moveRoot2(tree, u, score);
			moveRoot2(tree, v, score);
			#ifdef DEBUG
			if (position_counter_debug != tree.nodes.size()-1)
				WARNING("tree traversal failed in computeBestRooting" << position_counter_debug << " != " << tree.nodes.size()-1 << endl);
			#endif
			tree.reroot(best_node[0], best_node[1]);
		}
		speciestree->postprocessLCA();
	}

	// calculate the gene duplication for all rootings and rSPR operation
	#ifdef DEBUG
	int position_counter_debug;
	#endif
	double best_score;
	GeneNodeUnrooted *best_node[2];

	// recurse into the clade on gene tree with root *p
	inline void moveRoot(GeneTreeUnrooted &tree, GeneNodeUnrooted *p, SpeciesNode *subtree, SpeciesNode *sibling) 
		{
		GeneNodeUnrooted *c[2];
		for (int i=0; i<2; i++) c[i] = p->child(i);
		for (int i=0; i<2; i++) {
			if (c[i] != NULL) {
				moveRoot(tree, c[i], subtree, sibling);
				tree.reroot(c[i], p); // reroot on edge between p and one of its children
				#ifdef DEBUG
				tree.checkStructure();
				position_counter_debug++;
				#endif
				const double score = getScore(tree) * tree.weight;
				if (score < best_score) {
					best_score = score;
					best_node[1] = c[i];
					best_node[0] = p;
				}
				createSecondaryMapping(tree, subtree);
				computeGeneDuplicationsTriple();
				removeSecondaryMapping(tree);
				computeGeneDuplicationsTempMin(sibling, score, tree.weight);
			}
		}
	}

	// calculate the gene duplication for all rootings
	inline void moveRoot2(GeneTreeUnrooted &tree, GeneNodeUnrooted *p, double &best_score) {
		GeneNodeUnrooted *c[2];
		for (int i=0; i<2; i++) c[i] = p->child(i);
		for (int i=0; i<2; i++) {
			if (c[i] != NULL) {
				moveRoot2(tree, c[i], best_score);
				tree.reroot(c[i], p);
				#ifdef DEBUG
				tree.checkStructure();
				position_counter_debug++;
				#endif
				const double score = getScore(tree) * tree.weight;
				if (score < best_score) {
					best_score = score;
					best_node[1] = c[i];
					best_node[0] = p;
				}
			}
		}
	}

	// reset the gene duplication score to 0
	inline void resetGeneDuplications(SpeciesNode *&node) {
		if (node == NULL) return;
		node->score = 0;
		for (int i=0; i<2; i++)
			resetGeneDuplications(node->child(i));
	}

	// compute the gene duplication score and add it to the current score
	inline void computeGeneDuplicationsAdd(SpeciesNode *&node, const double &score, const double &weight) {
		if (node == NULL) return;
		node->score += score;
		for (int i=0; i<2; i++) {
			computeGeneDuplicationsAdd(node->child(i), score + (double(node->gain) - double(node->lost[i])) * weight, weight);
		}
	}

	// compute the gene duplication score into a temporary variable
	inline void computeGeneDuplicationsTempReplace(SpeciesNode *&node, const double &score, const double &weight) {
		if (node == NULL) return;
		node->tempscore = score;
		for (int i=0; i<2; i++)
			computeGeneDuplicationsTempReplace(node->child(i), score + (double(node->gain) - double(node->lost[i])) * weight, weight);
	}
	// compute the gene duplication score into a temporary variable (replace is smaller than current score)
	inline void computeGeneDuplicationsTempMin(SpeciesNode *&node, const double &score, const double &weight) {
		if (node == NULL) return;
		if (score < node->tempscore) node->tempscore = score;
		for (int i=0; i<2; i++)
			computeGeneDuplicationsTempMin(node->child(i), score + (double(node->gain) - double(node->lost[i])) * weight, weight);
	}
	// add the temporary gene duplication score to the final
	inline void addTempGeneDuplications(SpeciesNode *&node) {
		if (node == NULL) return;
		node->score += node->tempscore;
		for (int i=0; i<2; i++)
			addTempGeneDuplications(node->child(i));
	}

	// travers the tree and call the virtual function scoreComputed for valid rSPR operations
	// MJS: *node here is the target node of an rSPR move, and this gets stored as such in the
	// temp variable in the heuristic search instance variable. So looks like this is where we loop over inner loop of
	// rSPR neighborhood.
	inline void forEachCallScoreComputed(SpeciesNode *&subtree, SpeciesNode *&node) {
		if (node == NULL) return;
		// call scoreComputed when constraints are not violated
		// MJS: Don't get this logic here, which must maintain respect for constraints even during an SPR move away from original tree
		const unsigned int color = subtree->parent()->constraint;
		if (color == node->constraint) {
			scoreComputed(*node);
		} else
		if ((subtree->parent() != node->parent()) && (color == node->parent()->constraint)) {
			scoreComputed(*node);
		}
		for (int i=0; i<2; i++)
			forEachCallScoreComputed(subtree, node->child(i));
	}

	virtual void scoreComputed(SpeciesNode &node) = 0;
	virtual void run(ostream &os, const ReRoot reroot) = 0;
	virtual double getCurrentScore() = 0;
	//virtual unsigned int getCountTotal() = 0;

	// ------------------------------------------------------------------------------------------------------
	// outputs the trees into a string stream
	friend ostream & operator << (ostream &os, Heuristic &t);
};

// outputs the trees into a string stream
ostream & operator << (ostream &os, Heuristic &t) {
	os << (TreeSet&) t;
	return os;
}

#endif
