#!/usr/bin/perl

# Usage: runboot.pl seed reps prefix

$run = 1;
die "Need args (seed reps prefix)\n" if (scalar @ARGV != 3);
$seed = $ARGV[0];
$reps = $ARGV[1];
$prfx_base = $ARGV[2];
$progdir = "/home/sanderm/MPDUPTREE/MPDUPTREE.2.16.23";
#$opts = " -ff ff1000 --seqtype aa --overwrite --no_warn --root --n_threads 4  --algorithm MODELS/modeltest --constraints constraint ";
$opts = " -ff ff1000 --seqtype aa --overwrite --no_warn --root --n_threads 4  --algorithm $progdir/MODELS/modeltest --constraints $progdir/constraint ";

$prog = "$progdir/gp_boot";
srand($seed);


for $rep (1..$reps)
	{
	$bseed = int(rand(10000));
	$prfx = "$prfx_base\_$rep";
	$ropts = "$opts  --seed $bseed --prefix $prfx";
	$com = "$prog $ropts";
	print "$com\n";
	system $com if ($run);
	}
