#!/usr/bin/perl

# converts fasta to phylip AND DOES SOME EDITING OF THE TAXON NAME FOR USE IN MPDUPTREE
# namely, inserts a '__' after the first part of the label (which is the species name in these fasta files).

fasta2Nex($ARGV[0]); # ignore the naming


sub fasta2Nex
{
my ($infile)=@_;

# script to translate aligned fasta format (e.g., dialign's fasta output) to nexus format
#
# input format:
#       similar to fasta but each sequence is written on multiple lines:
#               >sequence_identifier
#               sequence...
#               sequence...
#               >sequence_identifier
#               ...
#
#       blank lines can be anywhere
#
# output format:
#       standard nexus file format, not interleaved
#
# M. McMahon
#
# command line input ------------------------------------

die ("Missing input file $infile\n") if (! -e $infile);

my ($firstLength,$fasta,@sis,@lines,$si,$seq,%siSeqH,$length,$ntax);

# read input --------------------------------------------
open FHi, "<$infile";
$/ = ">";
$firstLength =  0;
$fasta = 0;
@sis = ();
while (<FHi>)
  {
  if (! $fasta) {if (s/^\s*>//) {$fasta = 1; next;}}
  die ("File format does not appear to be fasta\n") if (! $fasta);
  s/\>//;
  next if (/^\s*$/);
  @lines = split /\n/;
  $si = shift @lines;
  push @sis, $si;
  $seq = join "", @lines;
  $seq =~ s/\n//g;
  $seq =~ s/\s//g;
  $siSeqH{$si} = $seq;
  }
close FHi;

# check data --------------------------------------------
for $si (keys %siSeqH)
  {
  $length = length ($siSeqH{$si});
  if (! $firstLength) {$firstLength = $length;}
  elsif ( $length != $firstLength) {die ("Problem:  lengths of input sequences not equal");}
  }

# write output ------------------------------------------
$ntax = scalar (@sis);
print "$ntax $firstLength\n";
for $si (@sis)
  {
	my $tax =$si;
	$tax =~ s/\./__/;	# replaces first . with a __
	print "$tax\t$siSeqH{$si}\n";

  }
}

