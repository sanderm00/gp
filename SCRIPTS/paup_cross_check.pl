#!/usr/bin/perl

# Checks the output from GP to see if each final gene tree's MP score agrees with what PAUP would say

# USAGE: ./paup_cross_check.pl ff_file out_treeset_file nucleotide|protein GPlogfile

# Note that the data type switch (nucleotide|protein) is what paup recognizes and is not the same as in GP (nt|aa) but hopefully you can figure this out
# Make sure to run this script from the directory where the ff_file is located


$run = 1;  # turn on execution of this script rather than debugging it

$ff = $ARGV[0];
$tset = $ARGV[1];
$data = $ARGV[2];
$gplog= $ARGV[3];

$nexfile = "infile.nex";
$paupfile= "paupfile";
$paupfile2 = "paupfile2";
$logfile = "logfile";

die "invalid args\n" if (scalar @ARGV != 4);

open FH, "<$ff";
while (<FH>)
	{
	chomp;
	push @infiles,$_;
	}
close FH;

open FH, "<$tset";
$count =0;
while (<FH>)
	{
	chomp;
	if ($count == 0) 
		{$stree = $_;}
	else 
		{ push @gtrees,$_; }
	++$count;
	}
close FH;

die "Num input files does not match num gene trees reported\n" if (scalar @gtrees != scalar @infiles);



open FHi,"<$gplog";
$parsing = 0;
while (<FHi>)
	{
	if (/^Align\s+File\s+nGenes/) {$parsing=1;next;} # start reading the table of MP scores from GP log file at this point
	if ($parsing)
		{
		if (/^\-\-\-\-\-/) {$parsing=0;break;}
		@fields = split;
		$mpscore = $fields[5];
		push @loggedMPs,$mpscore;
		}
	}
$ix=0;
for $infile (@infiles)
	{
	# convert the GP input relaxed phylip data to a nexus file
	open FHo, ">$paupfile";
	print FHo "#nexus\nbegin paup;\ntonexus from=$infile to=$nexfile format=relphy datatype=$data replace=yes;\nquit;\nend;\n";
	close FHo;
	$command = "paup $paupfile";
	system $command if ($run);

	# append the GP gene tree to the nexus file
	open FHo, ">>$nexfile";
	print FHo "begin trees;\ntree gtree=$gtrees[$ix++];\nend;\n";
	close FHo;

	# write another paup command file to read the nexus file and tree and save its parsimony score to a logfile
	open FHo, ">$paupfile2";
	print FHo "#nexus\nbegin paup;\nexecute $nexfile;log file=$logfile start=yes replace=yes;pscore;log stop=yes; quit;end;\n";
	close FHo;
	$command = "paup $paupfile2";
	system $command if ($run);

	# read the logfile to fetch its MP score as "Length"
	open FHi,"<$logfile";
	while (<FHi>)
		{
		if (/^Length\s+(\d+)/)
			{
			$MP=$1; break;
			}
		}
	close FHi;
	print "MP score = $MP\n";
	push @mpscores,$MP;
	}
$ix=0;

print "GP\tPAUP\tFile\n";
$Error=0;
for $infile (@infiles)
	{
	if ($loggedMPs[$ix] != $mpscores[$ix]) {$ERR="\t** ERROR **";$Error=1;} else {$ERR="\tok";}
	print "$loggedMPs[$ix]\t$mpscores[$ix]\t$infile\t$ERR\n";
	++$ix;
	}
if ($Error)
	{ print " ** WARNING: Errors were found **\n" }
else
	{ print " ** NO errors were found **\n" }
