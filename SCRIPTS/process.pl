#!/usr/bin/perl

# Takes aligned amino acid fasta files, converts them to relaxed phylip and runs them through RAXML to get a parsimony tree.
# Then writes a new ".processed" phylip file having the original alignment and the parsimony tree appended at the end.
# Does some munging of the taxon names, which is specific to the data set I'm working on, and is aimed at inserting the "__"
# delimiter recognized by mpduptree.

# Reads a filefile containing a list of all input fasta files.

$filefile = $ARGV[0];
open FH, "<$filefile";
while (<FH>)
	{
	chomp;
	push @files,$_;
	}
close FH;

$fasta2phy = "../fasta2phyTransl.pl";
$raxml = "raxmlHPC";



for $file (@files)
	{
	$phyFN = "$file\.phy";
	$com = "$fasta2phy $file > $phyFN";
	system $com;
	$seed = int(rand(10000));
	$raxOpts = "-s $phyFN -y -m PROTCATGTR -n $file -p $seed";
	$com = "$raxml $raxOpts";
	print "$com\n";
	system $com;
	$raxMPtree="RAxML_parsimonyTree.$file";
	$raxMPtreeSpNamesOnly = "$raxMPtree\.spnames";
	open FHi, "<$raxMPtree";
	open FHo, ">$raxMPtreeSpNamesOnly";
	while (<FHi>)
		{
		chomp;
		s/__.*?([\:\,\)])/$1/g;
		print FHo;
		}
	$processedFN = "$file.processed";
	system "cat $phyFN $raxMPtree > $processedFN";
	close FHo;
	close FHi;
	}
