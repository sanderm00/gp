/*
 * Copyright (C) 2007 Andre Wehe, Mukul Bansal, Oliver Eulenstein
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * ---- This code modified and extended 2021-2023 by Michael Sanderson ----
 * ---- Distributed under the same license as described above          ----

 */

// This file is the main program entry point for GP.


//*******************************************************************************************************************
// The following is the size integer required to hold the encoding of a single site in a sequence.
// At the moment, we know that using the 8 bit size with nucleotide alignments saves about 3X RAM requirements
// but also takes 2-3 times longer to run with current optimizations. For AA alignments we must use unsigned int type.

#define SITEINT unsigned int
//#define SITEINT uint8_t


// Following should be set in makefile and controls whether the code is optimized for normal or bootstrap workflows
// If not set in makefile it defaults to normal setting

#ifndef BOOT
#define BOOT 0
#endif

//*******************************************************************************************************************
#define ROOTED 0
#include <iostream>
#include <random>
#include <algorithm>
#include <stdint.h>

#include <list>
#include <fstream>
#include <signal.h>
#include "rmq.h"
#include "rmq.c"
#include "common.h"
#include "argument.h"
#include "input.h"
#include "node.h"
#include "tree.h"

#include "alignment.h"
#include "alignmentset.h"
//#include "gene.h"
//#include "geneset.h"
#include "genetree-heuristic.h"

//#include "tsas.h"

#include "pthopt.h"
#include <inttypes.h>
#include "pqvector.h"

// Now the following is only needed in tsas-heuristic.h, but leave this as is
namespace buildtree {
#include "buildtree-treeset-random.h"
#include "buildtree-treeset-leafadd.h"
#include "heuristic-leafadd.h"
#include "buildtree-heuristicsecondfast.h"
#include "buildtree-heuristicsecond.h"
}

namespace gtpspr {

#include "tsas.h"

#include "gtp-spr-treeset.h"
#include "gtp-spr-heuristic.h"

#include "gtp-spr-simpleheuristicrandom_DUPTREE.h"  // 

#include "gtp-spr-heuristic-PQ.h"
#include "gtp-spr-advancedheuristic.h"
#include "gtp-spr-bioheuristic-cplan.h"
#include "gtp-spr-bioheuristic.h"
}

#include "tsas-heuristic.h" // has to come after the namespaces are set up

//...........Detritus.......
inline Input filenameArg2Input(string argument);

template <class T>
void mergePQs(std::priority_queue<T> &target, std::priority_queue<T> &source);

std::string dirnameOf(const std::string& fname)
{
     size_t pos = fname.find_last_of("\\/");
     return (std::string::npos == pos)
         ? ""
         : fname.substr(0, pos);
}


//*******************************************************************************************************************
//*******************************************************************************************************************
//*******************************************************************************************************************

int main(int argc, char *argsv[]) {

	string prog_path = argsv[0];
	string prog_dir = dirnameOf(prog_path);

	initialization();
	signal(SIGINT, interruptFunction);

	Argument::add(argc, argsv);

	#include "commonarguments.cc"  	//MJS: Get the file args inside this file

	
	// ***********************
	// **** PRELIMINARIES ****
	// ***********************

			struct SearchParameters initial_parms  // notice that multrees can be modified below before execution...
				{
				"",
				outscores,
				randomseed,
				root_flag,
				queue_flag,
				n_threads,
				gtpmp_max_iter,
				sp_max_iter,
				gtp_weight,
				mp_weight,
				nni_radius,
				hsearch_type,
				multrees,
				h_multrees,
				constraints_in
				};
			// Some clunky user interface rules and regulations
			if ( treeset_type==NEITHER && treeset_infile_open)
				fatal("Treeset file was read, but '--ts_type' was not set.");
	//		if ( (treeset_type==NEITHER || treeset_type==GENETREES) && build_flag==false )
	//			fatal("No species tree was read with selected options, so you must also choose '--build'.");
			if ( treeset_type != NEITHER && treeset_infile_open==false)
				fatal("Specified ts_type but did not actually attempt to open a treeset file. ");
			if (no_sptree_search)
				sp_max_iter = 1;		
			unsigned int id=0;

			// Save command line invocation in the PREFIX.command logfile 
			if (!disable_logs) {
			string command_line;
			for (int i=0;i<argc;i++)
				{
				string ag(argsv[i]);
				*outc << ag << " ";
				command_line = command_line + ag + " ";
				}
			*outc << endl;
			outc->close();
			}

			// It would be pointless for multrees to be less than h_multrees
			if (multrees < h_multrees)
				{
				multrees=h_multrees;
				WARNING("Option multrees must be >= h_multrees! Setting multrees=h_multrees");
				}

#ifdef PTHREADS
			msgout << "Threads used: " << n_threads << endl;
#else
			msgout << "Threading not used " << endl;
#endif
			// Some setup for data input filters
			Filters alignment_filters;
			alignment_filters.max_input_files = filter_max_input_files;
			alignment_filters.max_leaves = filter_max_leaves;
			alignment_filters.min_spp = filter_min_spp;

			// Read the alignment files (with optional gene trees) and parse with filters
			AlignmentSet als(ffinput,alignment_filters); 
			unsigned int n_alignments = als.nAlignments();	
			als.report();
			// write log file of filenames that passed filters (disable for bootstrapping)
			if (!disable_logs && !boot_flag)
				{
				for (string fn : als.filenames)
					*outff << fn << endl;
				}
			outff->close();

			if (boot_flag)
				{
// NB! **** LOOK INTO MULTIPLE USE OF SEED FOR BOOTSTRAP AND FOR MULTITHREADING
				std::mt19937 rnd_generator(randomseed);  
				als.initializeAllBootWeights(rnd_generator);
				}

			// Read the input--minimally the alignments; possibly also gene trees and a species tree. See treeset_type.
			TSASData data00(als,&input_st,treeset_type);

			// Get to work.
			// Process the algorithm module to run a user designed sequence of heuristic searches,
			// or one of the ones provided by us.
			//
			if (algorithm_file)
				{
				TSASDataPersistent datap00(data00);
				if (!disable_logs && !boot_flag) //write this logfile except when bootstrapping
					*outgi << datap00;
				TSASHeuristic th(datap00, initial_parms, algorithm_file);

				// ***************
				th.execute_queue(); // runs a queue of searches specified in the algorithm module file
				// ***************

				TSASDataPersistent final_best = th.best();

				// Log final results
				if (!disable_logs) {
				*outgo << final_best;	// Log the best treeset
				uint64_t check = final_best.checksum();
				*outbest << final_best.printScores()<< "\t" << check << "\t" << final_best.getSpeciesTree() << ";" << endl;

				*outpq << th.pqPrint();
				outbest->close();
				outgi->close();
				outgo->close();
				outpq->close();
				*outscores << final_best.finalReport().str()<<endl;			
				outscores->close();
				}

				msgout << final_best.finalReport().str()<<endl;			
				if (outlog_bypass) // On request, output file with just this sp tree bypass usual 'log' naming conventions
					{
					*outbest2 << final_best.getSpeciesTree() << ";" << endl;
					outbest2->close();
					}
				}

	return 0;
}
//********************************************************************************************************
//********************************************************************************************************
//********************************************************************************************************


// BEGIN GLOBAL FUNCTIONS; CLEAN UP THIS EVENTUALLY
//********************************************************************************************************
//********************************************************************************************************
//********************************************************************************************************

// Merge two priority queues.
template<class T>
void mergePQs(std::priority_queue<T> &target, std::priority_queue<T> &source)
	{
	while (!source.empty())
		{
		target.push(source.top());
		source.pop();
		}
	}

void printPQInfo(string title,std::priority_queue<TSASDataPersistent> &pq)
	{
	cout << title << endl;
	std::vector<TSASDataPersistent> v = pqToVector(pq);
	int i=0;
	for (auto dv : v)
		{
		i++;
		cout << i << " : " << dv.peekCheck() << endl; // best case this prints tree scores, else error about internal
								// inconsistency of scores. See peekCheck()
		}
	}
// Compare vector data to vector minimized element-wise, and keep the smaller value in vector minimized
// Be sure to initialize minimized vector first.
void minimize_vector(std::vector<unsigned int> &minimized, std::vector<unsigned int> &data)
	{
	for (unsigned int i=0;i<data.size();i++)
		if (data[i] < minimized[i]) minimized[i]=data[i];
	return;
	}

void printHelp(string program_name)
{
		cout << "Usage: " << program_name << " [ARGUMENT]" << endl;
		cout << endl;
#if 0
		cout << "  -i, --input                   Input file" << endl;
		cout << "  -o, --output                  Output file" << endl;
		cout << "      --oformat newick | nexus  Output format [default=newick]" << endl;
		cout << "      --nogenetree              Don't output gene trees." << endl;
// 		cout << "      --score                   Report the final gene duplications." << endl;
// 		cout << "      --noreport                Don't report gene duplications." << endl;
// 		cout << "      --genetrees               Output the input gene trees after the speciestree." << endl;
		cout << "  -g, --generator 0|1|2         Starting species tree options" << endl;
		cout << "                                0 - use first tree in input" << endl;
		cout << "                                1 - build with leaf adding heuristic " << endl;
		cout << "                                2 - build random tree" << endl;
		cout << "  -f, --fast                    fast mode; only for generator 1" << endl;
		cout << "      --constraints <file>      A file containing groupings of species for generator 1." << endl;
		cout << "  -e, --heuristic 1|2|3         Species tree rearrangement options" << endl;
		cout << "                                1 - randomized hill climbing [default]" << endl;
		cout << "                                2 - partial queue based" << endl;
		cout << "                                3 - queue based [default: --queue=500 --trees=1]" << endl;
		cout << "      --queue                   The queue size for heuristic 3. [default=500]" << endl;
		cout << "  -t, --trees <number> | all    Maximum number of species trees to be output." << endl;
		cout << "                                <number> - max number of species trees [default=1]" << endl;
		cout << "                                all - all species trees found" << endl;
		cout << "  -r**, --reroot** opt | all    Rerooting of unrooted gene trees [default=opt]" << endl;
		cout << "                                opt - reroot the optimal tree only" << endl;
		cout << "                                all - reroot after every sp tree rearrangement" << endl;
		cout << "  -q, --quiet                   No processing output." << endl;
		cout << "      --seed <integer number>   Set a user defined random number generator seed." << endl;
		cout << "  -v, --version                 Output the version number." << endl;
		cout << "  -h, --help                    Give a brief help message about the arguments and example." << endl;
		cout << endl;
		cout << "  ** = DISABLED IN MPDUPTREE" << endl;
		cout << endl;
		cout << "Example:" << endl;
		cout << "  " << program_name << " -i trees.newick -o speciestree.newick" << endl;
		cout << "  cat speciestree.newick genetrees.newick | " << program_name << " -g 0 --oformat nexus" << endl;
#endif
		//cout << "  ----------------------------------------------------------------------------------------" << endl;
		cout << "  GP OPTIONS                                                              " << endl;

		cout << endl;
		cout << "  Input/output" << endl;
		cout << "  -ff,--filefile <file>            REQUIRED. File of alignment filenames, one per line." << endl;
		cout << "                                     Each alignment file must be in relaxed Phylip format," << endl;
		cout << "                                     with optional starting gene tree on last line, in newick format." << endl;
		cout << "                                     If some or all gene trees are absent, these must be supplied in '-s' command." << endl;
		cout << "  -s, --treeset_file <file>        OPTIONAL. Input treeset file in newick format. "<<endl;
		cout << "                                     File contents must conform to types specified in --ts_type option." << endl;
		cout << "                                     If present, gene trees override any gene trees in alignment files."<<endl;
		cout << "      --ts_type                    REQUIRED IF -s IS INVOKED. Treeset contents type:" << endl;
		cout << "                                       'speciestree'   -- a single species tree" << endl;
		cout << "                                       'genetrees'     -- one or more gene trees" << endl;
		cout << "                                       'both'          -- a single species tree followed by gene trees" << endl;
		cout << "      --seqtype <'nt'|'aa'>        Sequence data type [default='nt']" << endl;
		cout << "      --prefix <string>            Prefix to be used for log files [default='GP']" <<  endl;
		cout << "      --overwrite                  Overwrite existing log files with same prefix [default=false]" <<  endl;
		cout << "      --o_sptree <file>            Write an extra file with just the one best species tree (bypasses 'logfile' naming convention)" <<  endl;

		cout << endl;
		cout << "  Input filters" << endl;
		cout << "      --filter_max_input_files" << endl; 
		cout << "                       ...<int k>  Use only the first k alignments in filefile [default=use all]" << endl;
		cout << "      --filter_max_leaves <int k>  Exclude alignments with more than k leaves" << endl;
		cout << "      --filter_min_spp <int k>     Exclude alignments with fewer than k leaves" << endl;

		cout << endl;
		cout << "  Search options" << endl;
		cout << "   -a --algorithm                  Filename (in the ALGORITHMS subdirectory!) containing specification of algorithm(s) for search." << endl;
		cout << "                                   (if absent, GP will use the filename called 'default' in ALGORITHMS directory)" << endl; 
		cout << "      --algorithm_infile           Or,use this file for search algorithm (ignoring the ALGORITHMS directory)." << endl;
//		cout << "      --score                      Find scores for input tree set and exit [default=false]" << endl;
		cout << "      --root                       Use GTP score to reroot gene trees during each GTP-MP iteration [default=false]" << endl;
		cout << "                                   (does not affect any initial leaf addition search, which ALWAYS reroots gene trees)" << endl;
//		cout << "      --pre_mp                     Use MP score to optimize gene trees before further searches [default=false]" << endl;
//		cout << "      --no_sptree_search        Just optimize gene trees for input species tree; do not improve sp tree [default=false]" << endl;

//		cout << "      --build                      Build starting species tree by Duptree leaf addition heuristic 1," << endl;
//		cout << "                                     with no Duptree rearrangement heuristics. Then proceed to GP." << endl;
//		cout << "                                     (if using -s command, the treeset file's first tree --a species tree-- " << endl;
//		cout << "                                      will be ignored)." << endl;
		cout << "      --constraints <file>         Optional monophyly constraint imposed on all species trees." << endl;
		cout << "                                     Each line contains one constraint: a comma-separated taxa list ending in ';'" << endl;
		cout << "      --hsearch_type <'with_ties'|'without_ties'>" << endl;
		cout << "                                   Decide whether to keep topologically distinct but 'tied' sp trees " << endl;
		cout << "                                     in the PQ while rearranging sp trees, keeping gene trees fixed " << endl;
		cout << "                                     (default:'with_ties'), or require tree scores be different ['without_ties']" << endl;

		cout << endl;
		cout << "  Search parameters (\"defaults\": these can be overridden by an algorithm file)" << endl;
		cout << "      --sp_max_iter <int>          Max num outer iterations in species tree search [default=5]" << endl;
		cout << "      --gtpmp_max_iter <int>       Max num iterations in any GTP_MP search [default=5]" << endl;
		cout << "      --mp_max_iter <int>          Max num iterations in optional mp pre-search [default=5]" << endl;
		cout << "      --multrees <int>             Max num species trees to search on each iteration [default=1]" << endl;
		cout << "      --h_multrees <int>           Max num species trees to keep from sp tree heuristic [default=5]" << endl;
		cout << "      --nni_radius <int>           Limit NNI search space for MP search [default=no limit]" << endl;
		cout << "      --gtp_weight <int>           GTP weight for weighted score [default=1]" << endl;
		cout << "      --mp_weight <int>            MP weight for weighted score [default=1]" << endl;
		cout << endl;

		cout << "  Other options" << endl;
		cout << "  -h, --help                       Give a brief help message about the arguments and example." << endl;
		cout << "      --n_threads <int k>          Try to use k threads if available [default=1]." << endl;
		cout << "                                     Threads compute on a random partition of the set of genes. " << endl;
		cout << "  -q, --quiet                      Do not print most progress messages." << endl;
		cout << "      --no_warn                    Do not print warning messages." << endl;
//		cout << "      --queues                     Print debugging information about trees in priority queues." << endl;
		cout << "      --disable_logs               Turn off writing all logs except main log file." << endl;
		cout << endl;
		cout << "  ----------------------------------------------------------------------------------------" << endl;

		cout << "  GP-SPECIFIC TAXON LABEL CONVENTIONS (PROVIDING A MAP BETWEEN GENES AND SPECIES)" << endl;
		cout << "       In alignments and gene trees:  "<< endl;
		cout << "                            XXXX__YYYY   where XXXX is the species name; YYYY is a gene identifier" << endl;
		cout << endl;
		cout << "       In species trees:    XXXX         where XXXX is the species name" << endl;
		cout << "       [See futher discussion in source code file node.h]" << endl;

		cout << endl;
		cout << "  ----------------------------------------------------------------------------------------" << endl;
		cout << "  LIST OF LOG FILES CREATED EACH RUN (Default PREFIX='GP'; reset with --prefix)" << endl;
		cout << "       PREFIX.command          --      The command line arguments used in this run  "<< endl;
		cout << "       PREFIX.log              --      Log of priority queues found in different search stages"<< endl;
//		cout << "       PREFIX.species_trees    --      Best species tree logged at each iteration of search (last is best)  "<< endl;
		cout << "       PREFIX.final_pq         --      All species trees in final priority queue ranked with best first"<< endl;
		cout << "       PREFIX.best             --      Scores of optimal treeset found, and its species tree"<< endl;
		cout << "       PREFIX.treeset_input    --      Species and gene trees used as input  "<< endl;
		cout << "       PREFIX.treeset_output   --      Best treeset found (may be used to restart search from there, using -s option)  "<< endl;
		cout << "       PREFIX.files_used       --      List of alignment files that passed filters and were used."<< endl;
//		cout << "       PREFIX.genescores       --      Table reporting scores and other statistics on each alignment for best treeset.  "<< endl;
//		cout << "       PREFIX.checksum         --      Command line used, plus a checksum on each treeset in the final PQ."<< endl;
		cout << endl;
		cout << "  ----------------------------------------------------------------------------------------" << endl;
		cout << "Example:" << endl;
		cout << "  " << program_name << " -s sptree.nwk -ff fileNamefile.txt --seqtype aa --prefix testrun --root --no_warn --quiet" << endl;
		cout << endl;
		cout << "    Runs GP, expecting a species tree in sptree.nwk, a list of amino acid alignment/gene tree files" << endl; 
		cout << "    contained in fileNamefile.txt. No search algorithm file is specified, so the 'default' file in" << endl;
		cout << "    ALGORITHMS directory is used. Gene trees will be rerooted during searches, and output is kept to a minimum." << endl; 
		cout << "    Log file names will be prefixed with 'testrun'. If these already exist the program will quit, because" << endl; 
		cout << "    the 'overwrite' option was not used." << endl;

return;
}
