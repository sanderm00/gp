/*****************************************************************************************

	Class View is just a directed edge (u,v), pointing from u to v, on an unrooted tree,
	which provides a "view" of the clade with MRCA of x,y, from the viewpoint of node u,
	if the tree were to be rooted at u.

                         x
                        /
     u              v  /
     +--------------->
    tail         head  \
                        \
                         y


	This can also be useful in traversals of unrooted trees. Passing the view recursively
	into subtrees lets the code know which nodes have already been visited (namely u,
	but not x or y).

*/

#ifndef VIEW_H
#define VIEW_H

#if ROOTED
typedef GeneTreeRooted TREE;
typedef GeneNodeRooted NODE;
#else
typedef GeneTreeUnrooted TREE;
typedef GeneNodeUnrooted NODE;
#endif

class View //
	{
	public:
	NODE* view_node; 
	NODE* view_from;
	unsigned int size;

	View(NODE* h, NODE* t, unsigned int sz=0): view_node{h},view_from{t},size{sz} {}; // i.e., View(v,u) in the above diagram
	View() {};

	inline NODE* viewNode() {return view_node;}
	inline NODE* viewFrom() {return view_from;}

	inline void swap()
		{
		NODE* tmp = view_node;
		view_node = view_from;
		view_from = tmp;
		}

	void print()
		{
		msgout << "View:" << endl;
		msgout << "  -view_node: " << *view_node << endl;
		msgout << "  -view_from: " << *view_from << endl;
		msgout << "  -size:      " << size << endl;
		msgout << endl;
		}
	inline SITEINT* getStateSet()
		{
		return view_node->getStateSet(view_from);
		}
	inline unsigned int getMPScore()
		{
		return view_node->getMPScore(view_from);
		}
	inline void setStateSet(SITEINT *ptr)
		{
		view_node->setStateSet(view_from, ptr); 
		}
	};

// Put this here for now.
class DirectedEdge
	{
	private:
	NODE* head;
	NODE* tail;
	int weight;

	public:
	DirectedEdge(NODE* h, NODE* t, int w=0): head{h},tail{t},weight{w} {};
	DirectedEdge() {};

	inline NODE* getHead() {return head;}
	inline NODE* getTail() {return tail;}
	inline void setWeight(int w) {weight = w;}
	inline int getWeight() {return weight;}

	};



#endif
